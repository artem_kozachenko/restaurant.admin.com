<?php

namespace Tests\Unit;

use App\Models\Dish;
use App\Services\DTO\Builders\DishObjectBuilder;
use App\Services\DTO\Objects\DishObject;
use App\Services\Files\PhotoUploader;
use App\Services\IntermediateManagers\DishIntermediateManager;
use App\Services\Managers\DescriptionManager;
use App\Services\Managers\DishManager;
use App\Services\Managers\PhotoManager;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class DishIntermediateManagerTest
 * @package Tests\Unit
 */
class DishIntermediateManagerTest extends TestCase
{
    /**
     * @var Dish
     */
    protected $dish;

    /**
     * @var DishManager
     */
    protected $dishManager;

    /**
     * @var PhotoUploader
     */
    protected $photoUploader;

    /**
     * @var DishObjectBuilder
     */
    protected $dishObjectBuilder;

    /**
     * @var DescriptionManager
     */
    protected $descriptionManager;

    /**
     * @var DishIntermediateManager
     */
    protected $dishIntermediateManager;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->dish = $this
            ->getMockBuilder(Dish::class)
            ->setMethods(['ingredients'])
            ->getMock();
        $this->dish->setAttribute('ingredients', collect([['id' => 1], ['id' => 2]]));
        $this->dishManager = $this
            ->getMockBuilder(DishManager::class)
            ->setMethods([
                'attachIngredients',
                'detachIngredients'
            ])
            ->getMock();
        $this->photoUploader = $this
            ->getMockBuilder(PhotoUploader::class)
            ->setConstructorArgs([
                $this->getMockBuilder(PhotoManager::class)->getMock()
            ])
            ->setMethods([
                'manageFile',
            ])
            ->getMock();
        $this->dishObjectBuilder = $this
            ->getMockBuilder(DishObjectBuilder::class)
            ->getMock();
        $this->descriptionManager = $this
            ->getMockBuilder(DescriptionManager::class)
            ->setMethods([
                'create',
            ])
            ->getMock();
        $this->dishIntermediateManager = $this
            ->getMockBuilder(DishIntermediateManager::class)
            ->setConstructorArgs([
                $this->dishManager,
                $this->photoUploader,
                $this->dishObjectBuilder,
                $this->descriptionManager
            ])
            ->setMethods([
                'buildObject',
                'createStrategy',
            ])
            ->getMock();
    }

    /**
     *
     */
    public function testCreateCreateStrategyCalled()
    {
        $this->dishIntermediateManager
            ->expects($this->once())
            ->method('buildObject')
            ->withAnyParameters()
            ->willReturn($this->getMockBuilder(DishObject::class)->getMock());
        $this->dishIntermediateManager
            ->expects($this->once())
            ->method('createStrategy')
            ->withAnyParameters();

        $this->dishIntermediateManager->create([]);
    }

    /**
     *
     */
    public function testCreateCreateStrategyNotCalled()
    {
        $this->dishIntermediateManager
            ->expects($this->once())
            ->method('buildObject')
            ->withAnyParameters()
            ->willReturn(null);
        $this->dishIntermediateManager
            ->expects($this->never())
            ->method('createStrategy')
            ->withAnyParameters();

        $this->dishIntermediateManager->create([]);
    }

    /**
     * @throws \ReflectionException
     */
    public function testManagePhotoWhenPhotoExists()
    {
        $this->photoUploader
            ->expects($this->once())
            ->method('manageFile');

        $this->invokeMethod(
            $this->dishIntermediateManager,
            'managePhoto',
            [
                $this->dish,
                UploadedFile::fake()->image('avatar.jpg')
            ]
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testManagePhotoWhenPhotoNotExists()
    {
        $this->photoUploader
            ->expects($this->never())
            ->method('manageFile');

        $this->invokeMethod($this->dishIntermediateManager, 'managePhoto', [$this->dish]);
    }

    /**
     * @throws \ReflectionException
     */
    public function testManageDescriptionWhenDescriptionExists()
    {
        $this->descriptionManager
            ->expects($this->once())
            ->method('create');

        $this->invokeMethod(
            $this->dishIntermediateManager,
            'manageDescription',
            [
                $this->dish,
                'qweqweqew'
            ]
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testManageDescriptionWhenDescriptionNotExists()
    {
        $this->descriptionManager
            ->expects($this->never())
            ->method('create');

        $this->invokeMethod(
            $this->dishIntermediateManager,
            'manageDescription',
            [
                $this->dish,
                ''
            ]
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testManageIngredientsWhenIngredientsToAddExists()
    {
        $this->dishManager
            ->expects($this->once())
            ->method('attachIngredients');

        $this->invokeMethod(
            $this->dishIntermediateManager,
            'manageIngredients',
            [
                $this->dish,
                [1, 2, 3, 4]
            ]
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testManageIngredientsWhenIngredientsToAddAndIngredientsToRemoveNotExists()
    {
        $this->dishManager
            ->expects($this->never())
            ->method('attachIngredients');
        $this->dishManager
            ->expects($this->never())
            ->method('detachIngredients');

        $this->invokeMethod(
            $this->dishIntermediateManager,
            'manageIngredients',
            [
                $this->dish,
                [1, 2]
            ]
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testManageIngredientsWhenIngredientsToRemoveExists()
    {
        $this->dishManager
            ->expects($this->once())
            ->method('detachIngredients');

        $this->invokeMethod(
            $this->dishIntermediateManager,
            'manageIngredients',
            [
                $this->dish,
                [1]
            ]
        );
    }

    /**
     * @param $object
     * @param $methodName
     * @param array $parameters
     * @return mixed
     * @throws \ReflectionException
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     *
     */
    public function tearDown()
    {
        parent::tearDown();

        $this->dish = null;
        $this->dishManager = null;
        $this->photoUploader = null;
        $this->dishObjectBuilder = null;
        $this->descriptionManager = null;
        $this->dishIntermediateManager = null;
    }
}
