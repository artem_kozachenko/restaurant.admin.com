<?php

namespace Tests\Feature;

use App\Services\Repositories\UserRepository;
use Tests\TestCase;

/**
 * Class EditorAccessUsersIndexTest
 * @package Tests\Unit
 */
class UserControllerTest extends TestCase
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->userRepository = new UserRepository();
    }

    /**
     * @dataProvider providerIndex
     * @param string $email
     * @param int $status
     */
    public function testIndex(string $email, int $status)
    {
        $user = $this->getUser($email);
        $response = $this->be($user)
            ->get(route('users.index'));
        $response->assertStatus($status);
    }

    /**
     * @dataProvider providerEdit
     * @param string $currentUserEmail
     * @param string $accessibleUserEmail
     * @param int $status
     */
    public function testEdit(string $currentUserEmail, string $accessibleUserEmail, int $status)
    {
        $data = $this->getUsers($currentUserEmail, $accessibleUserEmail);
        $response = $this->be($data['currentUser'])
            ->get(route('users.edit', $data['accessibleUser']));
        $response->assertStatus($status);
    }

    /**
     * @dataProvider providerUpdate
     * @param string $currentUserEmail
     * @param string $accessibleUserEmail
     * @param int $status
     */
    public function testUpdate(string $currentUserEmail, string $accessibleUserEmail, int $status)
    {
        $data = $this->getUsers($currentUserEmail, $accessibleUserEmail);
        $response = $this->be($data['currentUser'])
            ->put(route('users.update', $data['accessibleUser']), ['name' => str_random(5)]);
        $response->assertStatus($status);
    }

    /**
     * @dataProvider providerDelete
     * @param string $currentUserEmail
     * @param string $accessibleUserEmail
     * @param int $status
     */
    public function testDelete(string $currentUserEmail, string $accessibleUserEmail, int $status)
    {
        $data = $this->getUsers($currentUserEmail, $accessibleUserEmail);
        $response = $this->be($data['currentUser'])->put(route('users.destroy', $data['accessibleUser']));
        $response->assertStatus($status);
    }

    /**
     * @param string $email
     * @return mixed
     */
    protected function getUser(string $email)
    {
        return $this->userRepository->findByEmail($email);
    }

    /**
     * @param string $currentUserEmail
     * @param $accessibleUserEmail
     * @return mixed
     */
    protected function getUsers(string $currentUserEmail, $accessibleUserEmail)
    {
        $data['currentUser'] = $this->userRepository->findByEmail($currentUserEmail);
        $data['accessibleUser'] = $this->userRepository->findByEmail($accessibleUserEmail);

        return $data;
    }

    /**
     * @return array
     */
    public function providerIndex()
    {
        return [
            ['superadmin1@gmail.com', 200],
            ['admin1@gmail.com', 200],
            ['editor1@gmail.com', 404],
        ];
    }

    /**
     * @return array
     */
    public function providerEdit()
    {
        return [
            ['superadmin1@gmail.com', 'admin1@gmail.com', 200],
            ['superadmin1@gmail.com', 'editor1@gmail.com', 200],
            ['admin1@gmail.com', 'superadmin1@gmail.com', 404],
            ['admin1@gmail.com', 'admin2@gmail.com', 404],
            ['admin1@gmail.com', 'editor1@gmail.com', 200],
            ['editor1@gmail.com', 'superadmin1@gmail.com', 404],
            ['editor1@gmail.com', 'admin1@gmail.com', 404],
            ['editor1@gmail.com', 'editor2@gmail.com', 404],
        ];
    }

    /**
     * @return array
     */
    public function providerUpdate()
    {
        return [
            ['superadmin1@gmail.com', 'superadmin1@gmail.com', 404],
            ['superadmin1@gmail.com', 'admin1@gmail.com', 302],
            ['superadmin1@gmail.com', 'editor1@gmail.com', 302],
            ['admin1@gmail.com', 'admin1@gmail.com', 404],
            ['admin1@gmail.com', 'admin2@gmail.com', 404],
            ['admin1@gmail.com', 'superadmin1@gmail.com', 404],
            ['admin1@gmail.com', 'editor1@gmail.com', 302],
            ['editor1@gmail.com', 'editor1@gmail.com', 404],
            ['editor1@gmail.com', 'editor2@gmail.com', 404],
            ['editor1@gmail.com', 'superadmin1@gmail.com', 404],
            ['editor1@gmail.com', 'editor1@gmail.com', 404],
        ];
    }

    /**
     * @return array
     */
    public function providerDelete()
    {
        return [
            ['superadmin1@gmail.com', 'superadmin1@gmail.com', 404],
            ['superadmin1@gmail.com', 'admin1@gmail.com', 302],
            ['superadmin1@gmail.com', 'editor1@gmail.com', 302],
            ['admin1@gmail.com', 'admin1@gmail.com', 404],
            ['admin1@gmail.com', 'admin2@gmail.com', 404],
            ['admin1@gmail.com', 'superadmin1@gmail.com', 404],
            ['admin1@gmail.com', 'editor1@gmail.com', 302],
            ['editor1@gmail.com', 'editor1@gmail.com', 404],
            ['editor1@gmail.com', 'editor2@gmail.com', 404],
            ['editor1@gmail.com', 'superadmin1@gmail.com', 404],
            ['editor1@gmail.com', 'editor1@gmail.com', 404],
        ];
    }

    /**
     *
     */
    public function tearDown()
    {
        $this->userRepository = null;
    }
}
