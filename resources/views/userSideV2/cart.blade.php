@extends('layouts.user-side')

@section('title', 'Cart')

@section('head')
    <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('modal')
    <div style="margin-top: 17%" class="modal success fade" id="delete_message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Success</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Thanks for your order. Our manager will contact you soon.
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top: 17%" class="modal error fade" id="delete_message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Error</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    You have to add items to cart first.
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top: 17%" class="modal price-update fade" id="delete_message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    The discount for one of the ordered dishes has ended!
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-12" style="padding:30px 0 30px 0;">
        <div class="col-10 offset-1" style="height: calc(100vh - 125px); padding: 0;">
                <div class="col-12 cart-container">
                    <div class="col-12 row text-center cart-container-header">
                        <p class="col-4" style="font-size:22px">Name</p>
                        <p class="col-1" style="font-size:22px">Price</p>
                        <p class="col-1" style="font-size:22px">Quantity</p>
                        <p class="col-1" style="font-size:22px">Total</p>
                        <p class="col-3" style="font-size:22px">Discount time remain</p>
                    </div>
                    <div class="scrollable cart-container-content">
                        <ul class="list-group list-group-flush dish-container" style="width: 100%;">
                            @forelse($orderDishes['orderDishes'] as $orderDish)
                                <li class="list-group-item">
                                    <div class="row text-center">
                                        <div class="col-4">
                                            <div class="table">
                                                <p>{{ $orderDish['name'] }}</p>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="table">
                                                <p class="dish-price" data-id="{{ $orderDish['id'] }}">{{ $orderDish['price'] }}</p>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="table">
                                                <p class="col-2">{{ $orderDish['quantity'] }}</p>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="table">
                                                <p class="dish-total-price" data-id="{{ $orderDish['id'] }}"  class="col-2">{{ $orderDish['total'] }}</p>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="text-center table">
                                                <p data-id="{{ $orderDish['id'] }}" class="col-2 timer"></p>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="table">
                                                <div style="display:table-cell; vertical-align:middle">
                                                    <div class="add center">
                                                        <a data-id="{{ $orderDish['id'] }}" class="pushme red">
                                                            <span class="inner red" style="color: #FFE3E3;"><i class="fas fa-plus" style="color: #FFE3E3;"></i> Remove </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @empty
                                <div class="col-12 text-center">
                                    <h3>No dishes were found</h3>
                                </div>
                            @endforelse
                        </ul>
                    </div>
                    <div class="row text-center cart-container-footer">
                        <form id="confirm" class="col-12 row">
                            <div class="form-group col-6">
                                <label for="phone_number" class="control-label">Phone number</label>
                                <input type="text" class="form-control" name="phone_number" id="phone_number">
                            </div>
                            <div class="col-6">
                                <label for="delivery_type_id" class="control-label">Delivery type</label>
                                <br>
                                <select name="delivery_type_id" id="delivery_type_id" class="form-control">
                                    <option value="" selected>Select delivery type</option>
                                    @foreach($deliveryTypes as $deliveryType)
                                        <option data-price="{{ $deliveryType->price }}" value="{{ $deliveryType->id }}">{{ $deliveryType->name . ' - ' . $deliveryType->price }}$</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6">
                                <label class="control-label">Total price</label>
                                <br>
                                <p class="total-price">{{ $orderDishes['orderTotalPrice'] ? $orderDishes['orderTotalPrice'] : '0' }}$</p>
                            </div>
                            <div class="col-6">
                                <button name="confirm_order" type="submit" class="col-12 btn btn-success">
                                    Confirm order
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/modal.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.countdown.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js" integrity="sha256-fvFKHgcKai7J/0TM9ekjyypGDFhho9uKmuHiFVfScCA=" crossorigin="anonymous"></script>
    <script>
        let currentPrice = $('.total-price').text();
        let selectedDeliveryTypePrice = $('select[name="delivery_type_id"]').children('option:selected').data('price');
        let discounts = {!! json_encode($discounts) !!}
        function timerEnd(id) {
            let data = {'id': id};
            $.ajax({
                url: '{{ route('cart.recalculate') }}',
                type: 'POST',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function(response) {
                    $('.modal.price-update').modal('show').on('hidden.bs.modal', function () {
                        if  (selectedDeliveryTypePrice != null) {
                            $('.total-price')
                                .text(parseFloat(response.orderTotalPrice) + parseFloat(selectedDeliveryTypePrice) + '$')
                                .animate({
                                    'font-size': '1.7rem',
                                    'color': 'red',
                                }, 250)
                                .animate({
                                    'font-size': '1rem',
                                    'color': 'black'
                                }, 250);
                        } else {
                            $('.total-price')
                                .text(parseFloat(response.orderTotalPrice) + '$')
                                .animate({
                                    'font-size': '1.7rem',
                                    'color': 'red',
                                }, 250)
                                .animate({
                                    'font-size': '1rem',
                                    'color': 'black'
                                }, 250);
                        }
                        $('.dish-total-price[data-id="' + response.orderDishId + '"]')
                            .text(parseFloat(response.orderDishTotalPrice) + '$')
                            .animate({
                                'font-size': '1.7rem',
                                'color': 'red',
                            }, 250)
                            .animate({
                                'font-size': '1rem',
                                'color': 'black'
                            }, 250);
                        $('.dish-price[data-id="' + response.orderDishId + '"]')
                            .text(parseFloat(response.orderDishPrice) + '$')
                            .animate({
                                'font-size': '1.7rem',
                                'color': 'red',
                            }, 250)
                            .animate({
                                'font-size': '1rem',
                                'color': 'black'
                            }, 250);
                    });
                }
            });
        }
        for (let i = 0; i < discounts.length; i++) {
            $('.timer[data-id="' + discounts[i].id + '"]')
                .countdown(discounts[i].endTime, function (event) {
                    $(this).html(event.strftime('%H:%M:%S'));
                })
                .on('finish.countdown', function () {
                    timerEnd($(this).data('id'));
                });
        }
        $("#phone_number").mask('(999) 999 9999');
        $('.pushme').click(function () {
            $(this).parents('li').effect('slide', { direction: 'right', mode: 'hide'}, 300);
            let id = {'id': $(this).data('id')};
            $.ajax({
                url: '{{ route('orders.remove') }}',
                type: 'POST',
                data: id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function(response) {
                    if (selectedDeliveryTypePrice != null) {
                        $('.total-price').text(parseFloat(response) + parseFloat(selectedDeliveryTypePrice) +'$');
                    } else {
                        $('.total-price').text(parseFloat(response) + '$');
                    }
                    currentPrice = $('.total-price').text();
                }
            });
        });
        $('select[name="delivery_type_id"]').change(function () {
            selectedDeliveryTypePrice = $(this).children('option:selected').data('price');
            if  (selectedDeliveryTypePrice != null) {
                $('.total-price')
                    .text(parseFloat(currentPrice) + parseFloat(selectedDeliveryTypePrice) + '$')
                    .animate({
                        'font-size': '1.7rem',
                        'color': 'red',
                    }, 250)
                    .animate({
                        'font-size': '1rem',
                        'color': 'black'
                    }, 250);
            } else {
                $('.total-price')
                    .text(parseFloat(currentPrice) + '$')
                    .animate({
                        'font-size': '1.7rem',
                        'color': 'red',
                    }, 250)
                    .animate({
                        'font-size': '1rem',
                        'color': 'black'
                    }, 250);
            }
        });
        $('#confirm').validate({
            errorClass: 'is-invalid',
            submitHandler: function (form) {
                if ($('.dish-container').children('li:visible').length) {
                    let data = {
                        'phone_number': $('#phone_number').val(),
                        'delivery_type_id': $('select[name="delivery_type_id"] option:selected').val()
                    };
                    $.ajax({
                        url: '{{ route('orders.confirm') }}',
                        type: 'POST',
                        data: data,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function(response) {
                            $('.modal.success').modal('show').on('hidden.bs.modal', function () {
                                window.location.href = "{{ route('home') }}";
                            });
                        }
                    });
                } else {
                    $('.modal.error').modal('show');
                }
            },
            rules: {
                phone_number: 'required',
                delivery_type_id: {
                    valueNotEquals: ''
                },
            },
            messages: {
                phone_number: 'Phone number field is required.',
                delivery_type_id: {
                    valueNotEquals: 'You have to select delivery type.'
                },
            }
        });
    </script>
@endsection