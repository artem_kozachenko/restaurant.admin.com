@extends('layouts.user-side')

@section('title', 'Menu')

@section('head')
    <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('content')
    <div class="col-12" style="padding:47px 0 47px 0;">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel"  data-interval="false">
            <div class="carousel-inner">
                @php($activeCarousel = true)
                @forelse($menu->chunk(4) as $categories)
                    <div class="carousel-item {{ $activeCarousel ? 'active' : ''}}">
                        @php($activeCarousel = false)
                        <div class="offset-md-1 col-md-10">
                            <div class="row">
                                @foreach($categories as $category => $dishes)
                                    <div class="card col-3" style="width: 18rem; padding: 0">
                                        <img
                                            class="card-img-top"
                                            src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhMTExMWFhUXGBgaFhgXGBkbIBofGBoeGxoYHR0gHyggHR0lHRoWITEhJSkrLi4uHR8zODMtNygtLisBCgoKDg0OGxAQGzUmICUtLS8vLS0vLS0tLS0tNS0tLy0vLS01LS0tLS8tLS0tNS0tLS0tNS0tLS0tLS8tLS0tLf/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQMGAAECBwj/xAA9EAABAgQEBAMGBQMEAQUAAAABAhEAAyExBBJBUQVhcYETIpEGMqGxwfAUQtHh8SNSYjNyksKCBxVDU4P/xAAaAQACAwEBAAAAAAAAAAAAAAACAwABBAUG/8QAKxEAAgICAgEDAwQCAwAAAAAAAAECEQMhEjEEQVFhEyLwcYGRwQWxMqHh/9oADAMBAAIRAxEAPwA3/wBZOEhC5WJSG8R0TG1UkOlXUpBHYR5TPQ7cj9iPo7294F+MwcyUn/UDLlndSbDuHS/OPnOchSWzpUl7ZgRandjSMeaHGdr1FyR3KWQWAKidAHJ7awUrHqQGKFo/3JI+cHcD9m5k7Isz5clSi8sLcle1rP3JGkYvHT0zVSFAzFpUU5Kmouzac4z3Fg69RdgcY81JCQocw9e94uY9oZaEeYEC5NBmPrAkjBSMxHkziqmqEUsSNft4VYiT4k4IoaOSGZIFvhWEz4zfwG5XHiuicYGRMUZq8UFKmKoCknIC7AgqcAAM7CCUexGUZhNKtmYD6kwPIwcopJAepAqXpSOMPjZ8hf8ATWTKqVIWaJ3/AJg4zTdFS8WShzQXjPZlSAVyCqYLnMwI32c8gIr6ipyCC4dwp6alwe5guX7Q4gzz4ebIpT5LlidNjF2nYbCzkEzEOQnzZ2zpH9wI25GL6exUoSXZ51O4hNKPDCylH9qRlB5kAeY8zWAJMkqUEpCiolkpAvHpOK4RLloAQjMg2yEB9syi59CIjw2BWgHwZKJRP5gAVf8AIkn4xPq10jVDEpr7dlOMr8O6ZbzJ9lKTUS3ulH+W6vSM4VJ8NQWsJSQXdTEv84seK4FNN5h9IWzOGIS7qzHoTAvLemaMfjyj7HPtTxMYwSwcgWCSpaUF1uAA5Ac2hEvghZ0qUf8A81RnG5RQUnKwL6NC2XiJqapWpPQkRoxqXHTEZFUnYwwuHVKzPY0Ygh/UNFv4NwiXOSkIxJSs/lWkB+jGvYmKT+OnTMqCtZBIDOpVzfKSxPIXj2XgP9NAlKWVkAMShCWDWZJYdIJwt7M2WhBL4XMk5kzEgg1SUMrM1DcA2q36xDg8QvNlEuYlIBLqISGFbQ59tsWhOGIWgLBWlgVFPMkKHulszfGK/N8XA+FPU8/BTQkvMAKpearHYh7ih5QPCnoXxtDv8XlQVzFBKAHJUwH8fWI8HivxJMyoRQS3DOB+ZudfhD8y5UxIdKVpYKDsRWxEA4jjODSooVOQlQoU1ccmyvDGotbAUX6ASuIy0U94powr9/rB/C+IzZs0ZpplS7kg5VKZqPp2rARxuCNUHPyQEg+ilJ+UG8MMyccsrDlP+UwinNk0PrCZcF6/wPxYcjdpfyelSVpUAQQobgv8Y7YbQBwvACUhnUSaqJ1+gHKDe8aV0baN5Y0UxgJjlU0C5A7xZezYQY6CTAc7iSE6v0BjhHFZR/PFWkM+lkaug7JGZB9mOZagbKeO8sWKdo1lHKMYR00bCYuiWcAxvPEjxjxdFWcBUY8dxkSiiu8W414MtcwaJUSnYsWI/SPG/aJMzET5IUorcBSzskJBUBsLJiye1vG/GHhSCC4IWo+6Abl4rXHMfKQhIlyjMXkCHLswarDmHLPzjFmyOU1xAyRpgmOwcyerxFqEqQmylf8AVNydIAxfFQhJlYRJQFUUs++vv+UchEGMGLmnPMlzDs6SABowiSRgVBIUqirhNy2pO1NLxFFRSv8AgXJV2CTkLYS0A5R7wT+Y3JO/8Q+4bLKZLIH9RdG25fLvA2DxKAiZ5cylNlVQBHPnTSHPCcPOmHLIlsn/AOxYIp1rTpA5G5KhyiqS/kJw+GThpeRjMmGqrBIJ3UaCAJmGM0n81fdlhkj/AMjc87w/XwdEpvFV4y9rIH/iKnvDHDLSWfKGskMAOgYRnbp/JpWGc9zdL2RXcFwmYkMhIQNct+5uYOk8NmA0AB1JreG+Ox8uUlwSCbBzCDF8fWqxcenrEcWzd4/hKW0q+WOcMkoSc8xO7UPa8OOB43PLKiWD+VzdtI8/XNnTFPlLfCsWnhUlWUJqEgVOp6Q3GnF6NS/xeBfe5b9a6H+PkKWl0J8QagfdYquMkFJIOGynn/EP8DiVJBp/yv8AKkMsPiEzEATRmrY3TppDZYlP9ReTxXjvj0eMcbn+NOKCgICKNzgRfDmy2IND10j0X2p9hc6SvD+e5IbzV7Vir8O9icc/nlTACC2YkdL27xauKo5OTxm5XfYp4dKTInBc1KhlfKAl/NodmFT1aHC/bFCPdlqdx7xFQ9aA3a3WFvEOE4yUrw5gU+ahJfL3+sSYHgfhnNMZZdjq3bfnBqSBxf47Jlnta9yw4bHJxsoDEko/qFQCQFUslJflWLYjAonyDJ8UlBDOU29KRSvwiRbM4q29hSmgg3C4+bnQllAUITajwrlJStnol/hfFljSWn+ovmcIxnD5wmJ88sDICCSkockIULpqSRsTQwTxPCoxRTNyqRMAZSbgjkYvssKUAVJ8qnBCvSscI4XLcBIbkS47E17QWWM3/wATmT8KEJcl6FU4BwYFQAQr0H6R6bwvDJlpACcrRBhMAmWMxYAByYgx/Fk5aA0BNjp96RMGLjuXYHF5Xxh0MMbxlEsVP3tAU3j0wAnKBtVz6RV18TE5bM6abgBi9fgebiNYjFJBZSz/AIksAO+rw/lZuj4MI6lHY4X7RrW6UnzC4rTq0Ry+IFYzKSocialtuUKZZQQDQf5A/ZPeIkqIQknz7uWaL2O+njWoxoYr44kF8pykkORYinaFuN4ygEpyh9wdzGp2PSohCkkBqgZm/evKFyuHS1TKZhRwLdwOukDK30zRiWODuSLbwLjqM2QAg9bRcsPiAoc9o8i/ATJSipLqFDav72i88C4iJiUkllJI+ETHJp8WY/8AIeJCUVlx9epax0jCI5TMBjsJ5w84BoRvNGFMYwiFGPGPG8gjMvOIQ+YuIcS8JPhILq/Mfr15Qz9ieKCasYdcpU8//EFK8iC9VrTqBd30tWKZhpC5ighAdR1PxJO3OLFgQcOpPgk5kkeYXUo0pyZ6RkcYwVeorls9UxGCwODQVTUpmTGq9zsGJZI2HWKPNxUjETFqTKkSlAFiJYseQuf8j2aG2H9nz4K8Vi15lBKlS5I8wdnGeozEn8r9SbQm4LLUpE6cuUmpIHlTQjYNvtAyTr2NfjYeeRRktGuHezkzxETJU1C0gvlKCkEV3cGLJiVLlElSgSRcE9kjtyjnhktQSnMVbkgD0YWjOP4M+P4dVChDUoW1NhVojjo7GPxMUZaBcGozV5Urck1NGbUVsYixQX5mzIy3dnLX+9YdYXDplFKlJoACyQ47k6xvFSjOdaKirc9PgGinijQ5VaSWq7KzLwqpkwKW7a1A/gxPhOEDXS3z++sOpGDFrAFmO9mAuawZ+EDh3s3TpEULGpJdgUnCtlsKebk8HIyhYShRUaukaa9PWJDLsKV11ofusFSGAygMA5b6w+MKFyyA5wy1KNGSG97UjpBBkrJGU5c1y92v3/SO0qJBD86DeO1MpI8wDEVAcg6N8bwfFC/qyNnGFNNbDLV+9h3jRxSnSMzsK6PUj1uYmExIcB2Z9C7/AB0MAYyWkuR76SqopQGxFPW9YGS0Xj4ydNE3FsCmcAsUWBrYsbRXvwKglTCrmhDNWwi2TZpEol6liO/zMIsRi3SXSFrDG5sL6sS2h2gZUhmGM2tdJi3wSJechgoUKrAFn+rRLMxEvNLzFKiXSbsHszV0N9YKSSpIoMpYgX+dIXSeGAKWkoGVQCg5NGd2IteI4utDYzjb5DvCcTWJgQwKTqaX+Z1hypFXB/d9Yq2BwwlK86yQqodnOwcNYRaMNiBMFAA1tSYuPsxPkJalFa9WSomFSSgktT76Qgx8pSUqKSXqwfW9K+9tpD8IqGodYVe0UhXh50gljalagvXa7QUo6FYZJSpeoqmSgBmNCk1IATm3HqbxAvHy/d8PKTuBf75wWjFKmpZUtnHmzHLezUvblCLGYJRWqSnOVjdgw/vKrNAN+xqjFdTDDhvE91WSY7VqObgUsH9I0nDmQWnnOlZYEEgDkRBHDOFiUr+qfEWACCSWD/2i3cuYNISpszKQaZWdj9iLSBlk9E9fnQKUIQ65UtSq1ZwG5RtPEgqqZbEBno9dGvb5x3+GMtyQ6H8gD0fQ1ducYqQhKTlBUSxNXCaWBNBBbAuL+fkWjiM9KiQFEUBDUGop+0N+Ee0TkBSQ5o4+sComlNJYCn95Ny+gG9aNC2ZxIhRBTaoOViH2hcrW0zTFRyLjKK/s9UwWIsdDDMJjz72cxiy6lnIhgwVrz3+EXrCTApIIPpDoy5HA83x/pSCI23OOCTG3P2IMw0dNGAc457xpz9/zEslHzdw3CGUghJS594ipP+L7RNLnhKhlSQsGF+BxuVLVLQ24UEqWFKNY5k7u2TBglz36DqSZ05IQpSvN5QDQdegFYa4bColoTLlAkIIY2fc7dojlTh4qcrNLSSH1Ubj0aGGCWVNmQRlDsS97ChY6wzHH5O9hgoxugiVhggZwSBc8+0RYdEuYAvKQwIOZgelfpyjaZkxT5iEJYgAVJ0fYCOZU56JNvpQ00eHjKYRMwoYJqXG/3SI8FLACil8oJYU0DEgxHPyOpRJIsXJ1oUgc4lCSkZHCQoOnVreVojQSeqODmBLJoK51F6nRrxHkUpIJUlSS4VldJoba/TSMThDRWd1C1AEtzH1eClLCHKmJLVA94/bRaRbl7HAWlLAV5Jr8qdzG0FbuEMX1LP6XjgLmHzeVL/lUNBao6xOrGeGDmSaDRyOz/KGCWn6bO5STmqbioS7CIxLbMgAZXZy79aaxiEqWMzgHo5+bXjopUKhT1FTuKXiMG/k5TJJNVUSwDBu5+7PES2zLXm5abChH1jsqUapPm/MVGlNvlBSXYZGLgODbr8YF7Q2DadnWMYyQQcoeh2N25jSEM8UFQCLNq9/1htj54ElhQvQHvFbxGJZFdf0aETaejZ40Wo38sbYVSCxS7ilCPXpEk/EBIYjoaNFa4bNRNJqc1RmBIJAexBtWG+CkYdnYE7qLt621h8XozZIpPd/nyc4zEoULgilasNrVJjrhuKVKUF1y3IpQ/FxGvBlJIUlwnlRL7/O0dTsSmWQzqSewGxDaPAyXqNhLXFLT9y3IWVAKIuLaxDxNKlSV5QHZQD66V3e0K+EY9J8pUSQRf5AiHKnvcb9LQUXyRjnjeOZVeGrWQP6qSzjKUl0to8Sz5qxmIIzJUwSPzOa37GNcTlzM7py1/udw2xsxp3hWMbPCMvgqKw4PmAF9nrC+lRs48nev+iabNX40sqOUORm57KB050gmbh1k5/ESkiwFjz7jSFU3GzczLw4LMVEgFIH+RFO2kM0YNSyhXipQlmOVJoNAA9BUiJEk1VNv+ybBY450hSwDzYJ9Tp63MbwspSjZdk0IAA9fm0dSOHBJVmWFFJ/pqymnbQxNiMYuUUFYBCgxIO9Q4vvDF1sRLb+0jTgChiuWgJDl3s5oC3WJJ2FSQCWYOUsA9K1JOp05RtZlzUsVM+mY/lLp+kawayZf+ko7p+JJS7kkiLQtt9+oKOHTSSczoAJKhQ0uzxZPZriofw8qgGpmBqN4QjFEuvLMytbKaHfcCDcHjAKFZWaUGnKggFFJ2h2W8uNwkr/ovoEbaFPCeJBaQ7jYnlSsNmhtnncmN45cZGEc40ERonr6xoHrEsE+bsZwiZIU0xJFAQRZjry7xNg5wH8D9Y9F9p8OSDPlMUlCgtJSKFLDKQQ7hiGpV9YofFsKJalBJCkuMpYVo9xqKiOdLZ1IK3VDThs6iixNQHHO7fpDPhuJSc1FKUHZJSUtffk28C8ElD8OBQB3USdfto7Ti1BbIlmh95bAVN7uqHR0jpxV6GCACoHylW2iTtzMGmWGdCSVZspCa6UHK16CF+IllRJ95wKUSH+ezNGuFnwpZqEhQA961S5c8yfhDlrsXJX0GzlhJAKhmuSaNyGw56wLLxaXDK8rl+703jmbxJQmIliYrLdRCnFfdTsByMcYnEpCsxSlsxSSE1JFW9NYgUU16E0lVSELZHV1HcAnYtuawSmQgKKg5XuSTlbl9mN4RYmsggoY+VTWf5g/do4OBUlSgqZKKn/Kkn+NItICU1bTdBMmWogkkOWcpDet40uelLgqSL6ivb7rA2KwAocxUljnKfLUVDAVIZx6RvCmSkhKAMxtqQOsFYFJ77/b8/0S/jzQISoqo4AYFuZo1WjpU8KCZZCkq1Ls3fUxOVJoBRz/AD/ECqxRRmzFnZiRQNUDnpEZUd9IiWoibQ+UJuBbfXp6wwlYlKZedRNDXQcufaJpkgTk0A0PNxX9YScbWEITKFw7nd/sQrI+O0acEVlqL/f9AHF44zFkm37xW/aHH5QEBwVU6B6/QQ4w8wAuqpZxya0Uric4rxCia1Ydr/F4TjVvZt8qajFRivgdcKYkMA4etQ3oR6Q34VhJikkAjKasXNtDW1oQcPC9Cx8wPRgP2eLHwmdlJlqAL0SDUdSdWhyq9me3x+06xvEQSZZdSjoA5prSjDcQXgsMqan+oCEgMxcOf2hkichClMA7Cwbd6UbeIvxRSoP7qrgaH1N4Ol2xCnJqoqiXB5JTAMADR3JD3bcF7xYsPPCxSKTiVOaEANzLdD3hx7NuHcjzWvWBUmmHlxKWPk3sM4icpdnaoHT71hXxaW8vxMvvZb6aB+tosU6U482guAT+8KQVeN4akgyiCQtJs1ACNKm8XMXB2rXaEik/0wjMXXQvUh7MzUHOOxw+fKTlQfEQxpQEN84YYXCqOaWlSSiwzvmB20zUarxuYlRy5mCw7KZ0lrk8oBLQ3m70awU1GQCYpSVkALzEgve1j8oklTFpdWUTJaTVYvTlqzse8ScTlhUuXMITNKFj3R/cWYg3Fd4lwuLAdTKBNwAWfpaGR0Im7Vpfr+dmpBTOZaQkLFRY9AW1PwjrEIzHOMwWKEMa9jsYCw0yUlS0mWJasxIYVrs0MkzCEZrr0GgA0Ox/aLTsCUaev2/PYgXxFSFZVoZyC5qlVflygmZLdI8MgDUBrH76RErFEgZkEgsW5nT5VgNMhSVFSEq8z0KgW6Vr9IKyKK/T/QwRishyPlLC4tye20WLg3EnASo3t8vSKwiaonKcuzFxzZ2YnpB2CzJVl008vwzCBvYGfFGcKfZcCrnGZ+Y+EcjeNwRw6EuKkoWSWDkecNcEMX3cN6CseR+3ARKnGQke4XKnvmqzWdmc1j0DG8VQnEKKXyAlPajn1HpHmft0oHGTSC7hJ9Et9I5mOanL52duOGWNJvqgj2f40lKfCWwCiTmOlqdIcTcWoEpSl1bmg9db/GPPJK9ouOAxxUhISh1AAE7Uo56RoWnRthJSVk6QpSz4ikhFyxYl9ByG4vpGpk1AIUklJSKEl0kHTp8o5zzB/qS61diCGPJzSsbxHDPBQJoAW5bKsuEPbK967wzsK6ewnC49UxBomhdQatquGZ/27dYFQKgykgkEChI7aBUC4okqGc5UlLKCGzFtD+oe5gzDTZYJSh8qhROU3Tt2b0EWvkr00hvwmSVTUAzARmDpAZmFgxoLUjnGZkqJCMys5rtXfrWI+FTVZ87F0IWo5r2pTq0bwswqJBVdV2NdT9aVg01VCJJqd+y/OjhMyZMYhSQKOMpNHrV7xNLw6HdKBSxDgl9H/eJJQQVEmu70tqzco5n4cgZpRDag/wDUm8Foptt10ceFMWGfKl2PrYfqYMmYQTU+9mTdmuRQMYDRiSWAACSKmlunP7vB/DQpIC1KZAc5jSj0YGsA2g+LSs5wmLyJUpQCcgIoSXIO+/IRVOIY4rKlkF1Vr97QTxniQXMWoUD0blFV4xxMJJAqTo51+UIdyZtXHEnJ9v8AKJ8fxUID6sQBuYrspZcKLmte8DKKlqdV9P0hlw8Jsd6wxRUUY3leWfsl0OeHYmWsMp07UL9X5Q1wWIKlZgHNmajC52aEkk5fhTcfSHeAkC2Y1FSGodvsxSNF6DsOsEKUCXKqHMwESYqUf7lE/l1c7UFzEUtkuAQmmofvf7eCJJLhcygDhL3BNHI6W6waFy7tEeBwr+WYGIvW/L41iw4SckKCQzaAVZt4UzpgzIZQY0J6wUFk+RJDiuw+zBdLQFOXZYRPSqoNevf76wBjElLKQjMrYEA763jnCryywhjmNSdHLXGnIwUs+JLJF6t6fvSKu0K48JfAimpEw55ZMpb+ZKw4s5o9GBaBeHcKTV5sxTF/fAB7AONmiDE4zyKE4MahK011Ysb3el7wXhuFJRkHiLYioJAcm1SPhCqt2a9xjV1+e4U6mIlrLipSeVb3bnEPDMZNnqcKyB6qIuR+VqP1gnDiQhTspfUksRdwG5ROjDISfKtruCkEE3enM3eHJGaUttV+7RhxiEABZJfzAtqam37wJLVLWCZSylRzPXd7g0+UD4g+H7qVODmGV1JVW3zpSO0GTMVmy+cXTUE0N/3iXZFDirV/nwcyMetJyqScwLu/lawrtcw0kYhbukIu4rfptaAVOkslBCbrZQbQPXW9IklYVDpWk5dUBJNb7inWLVokuL3QfMl+bMHST7ySXSp7kc3gzChlJCaFrNQv01v9tACJqtVBnoWcu7nVrQ3wVWNrlwNS1TsYvsTP7Y7H8lflTUWEdgDl2iOSoZR0F6RLT7MQ4r7PM+N4Y1UmwHxMeecX4fOVnxADoQfDmNdPlcEj+0uQ/KPSMbP8qoh9nZIVhZzpBExa3cXZITseccnxH9zo9J5U2sKT90ePyl3ENcBjlJU70NDC3iEjw5q06BRbppHeHra8b5JNWZcORplzlHxB5pqslXypAL+uu7PBUiSSps5UhLFLEAqHN/pCng8wSyxIIZ7GmrkbwSC5NXCheobe1oBSo6Efu6Jp0oNlSSPMXuXv1p0MSIxJfKm/5Tfs+lu8A4bAgn31gOxArzuRBapiaJS6Q1xrV+vq8Hdkod+y6FHxvMnMUMDqXLt8GiNKCgqUonyu6TRmAs/3WB8CnLLTkcqJJffSn0hrIxSVZROBXblpV3u2obSGKqM75c3LtP8AnQqlYgKXagdTNcn3RBuHCypNWJNu9KxmN4YlaiZSwFFOtKC9BY+sT4RK0LTmBANSb20uWMCOu+gjj3hgpWWBCg7fm5EbgQk4jjVKOoD2/QaVrBHG57FSUqLKv++kI52IATmJuLnlCZO2aMaUIoXcXxoQTRyXasVaaorJUbwfjJ3jzCsJyhgAOmp5mOZWGFmq/rDY6Oflcsj+COWgsO0G4aUUkHe8bl4ViHdga/SCCGpfXq+kRhQjWw4IDkKAJYV/fSDMFiKgG/6frC5FOhG1q2g/DywUCp2Pb7ECh9oOVOZT9jXXQwQxLAl9epO0A/h8xSHJsr0Y+lIa8LolQYUNXbXlf7EEgZOtnU2SSkWzOPM1Q9G0uIMwcoA5QbG5q5P7VgSeujIDC+/fftDHh0sKUFaAO2jnl6xdk6iHY0KDB8wABoGI/UVifCTBTo9/poYXKxwzEPXTr125REjiCNUB9fr3iKaAeGTjVHHEZIE9plJSg4BsVPUd7wP4ZWrwpamSS6s10h3JT1ZhBPFsYDJVMH5CCxq4NCPiD2gdBlKQjISTQOlTGtqnR/lAdsYrUVa31/6GYgkhISqqFZW6JJY05O5+scnDFWYBSkhg6RlID6gs7V+cQS3S4KwJtgogEkDQt29YHmmeQSMqVJJo7ZgHcDcfWG2I4+zoY4ILlofM6VWA+gPJusconhYW6FUJBUkNtr9IAweLzBKxMUCKlgPLejNpDVGVdfFOY9GO/J7O0WiSTT2gdWAS4AXMQrQECrChswjqVwuYkkmaeTD4E+lqRJMxBlqyFQUSxDjRVdYG4yFpyqlnysyhsqhcciCKxTaLjzbSvskwjJWoLPmJoaEFqBvu8WDAz2BDDKSwbnct1ikrxaSQ7Wp61MPOALVSv5g0Asm6Q3N494+TL2glqkCOgP8AIeggdCCNfX+Ykc8vSDs820eW8ZnMMohzwNkSUy3ILVoLmp+JMVjFYoKmDUAvFo4ZNSpmUqmkc/xFR2POnaUfY879teGecrTWp1d4q2GUHY0b7aPYfaPhwUCdGjzPiXCsqiofN41p8ftZlhK9o7w8+hP+LV5NDTATEsyj/t684ryC16Qfh12rSBls6ODJXRZfwbBPmUBc1HmfbZo6RKSwmK938qeVq6mB+H4wUCw4AccukPZeH8QCZQS0hwLfpYiJFmt0bVLKUIyhiw/LQkh2ps4gaTiqqQEKzO3Z97VrDRSizkgUoqoNf8WtAAkgpUcwzG5D0d9DoB84ZJi4LWyXB+VVgpR8r6JfbnaDOKkIRQ+ZJ5Vd6QNwzAZXWpSiEgG3vK3vajwHxqe6lB3B1HOFSeh0Vcr9hfjZ+Y7b9Yq+NxhmeRHmFcxgvjfEWQwNTtpAuCohRFLCnM17RcdbEZZ8m0mdyMKEpHmqSHAGnOJBI8wy3A1esb8EhvpE0mYoKBZxZ9n1i7spRpEfgEe8KF/jB0qUc4KWYpLC9W/aJhhFs7BQUWpt/dE07C0/phili5c0NADF2WoA4kkALZgbt6doyVLUS7sBduX8xIt8qSVAhrN6iOcKfKRdj6colkcaDsPKCS4qlIq5u+og0DIlK28xFARd/wCSO0LZCwlCn6EHbRu71hmiarKETEsSyAq9H/iLsqnZLgFAkB2Kk2Y3zOBzN69IKfwwo6nfl9YFxmISk9xs9BalL1gTE4x2Z2gJ5EtGjFib36HZnAglxQht6mw5RDOxB95+v0iCcxBrf4GF83GZQQbiAGOSsc4zFvLKAHzEeXfU9aPSI+HJlkFQUsKzOkVGXqNW7RX8Hi1FYJoNOUPFTAokud3/AFgkxTVq0NBjkKCQoZim71Y6ttrAuMJASpwUE5RWupANjpeIOFqIWpBUNwTr/H1gmZh0TM4LhIPkH/bnrBpinGnoAmYZaTmlpPm94G2zhu3WJsJjS+UpUGowqKaQYEkAKAzAUZ2pzjMLOTlJPvAfdWgW0NjdbRDjMelUwrFmSkD/AGhn+cZieIuK/f2ITrmupRe5JtGOGrAcmMqNJewVOnAlJFW+sW32ZBK0JLtf4PFPwkkqYJBJ0oA0eg+y+EKPMqpZunIfrFRlcxPmZYwwNepZlHnG094G/EediO/yicLOx9I0N2zzNUeDYeYynqN/u8Wz2aKlAlIWQKlWRRAHW2h1gDg/ACMsyfLCh+WWX8z6qGieT1i/4bi2UFCsqEhCQhAokM/lA6NGeCjzSbpmzK5cbSsXzgFpYkE94qXG8ANUnu5PyPzi7fiAtIKCEVLhQzelQ0B4vDuD/cdR9tD5x9xOOVHkWNwpBqFNzgVM5tovvFOEHVRUe0VfHYApPu/EQlSrTNCvtEOCx4dLlov+B4gheHl0Yhk9xr97x5ovqPQw24Pj1JStLFSbuAfL15FvhBP4Nnj51ySmWteIClstR1brA07HBNBQc6wlncTBqSH3gSdjwXLuYo3vJFFnxPGVrFSw1FBCriePyyiTcH1+3hQrHEhhpWBcXmmCtBs8Uo7tmbL5SjCogyg7c4eYZKTJS1iWI6Et+sIkyyNuxhnwpdGJtDJdGfx5pyGCsNRxUUeJRL8yHF1D+axLLUA1SDetR0iXOkm6aAU1feF2dFQQYtSkOB/PeIsKFHOnKwdy5pXnBKpgBZ8zUejH4wIZ6a3HTeI5UGoWgaYWWQBTcaGOlS6hix0I1/aNnFDMxHd4hGNCSwAbnA8mW4RC8MwCVkgKZnPI1psYlxPEgpPlo9+rN9IRKm5ipyxf5xoTSD2bpBW2JuMXYanEEs53g/CzRlIYc3EIjMb9IIw89QrANDFlXTC8ViMtD984QYzElRpbpDTEYcqDkdr+sLFcMUCCGTyJ+zBxkvUw5vIuVLolwpaG0nFM0KQ6feSRzakES5w/mKZqxTjXYbi8W7MKuIK/HrDHXcNCxJzqDEeUE33iYgkhLehimNU0HjGrIYmkcHMUKCWzEFmjqVgZhslh/lr0aphtJ4Q4APdxQdaH5wFoRm8mMVUStiQqlC/OkN8NwQkZlHsxi0YDg/8Adv5QD+lfgIeowaRZqXo/xIglGUkc7J5r6RWuDcHI8zZexH0i1yJQSACzaUJMdS8O3mHyA+kFgda/e8Ox41FGDLmc2aQEPe8FiYkU+QgWURUU6RhQT+Qd/wCIapV0IavsS8FwIUPEP5rdNIg41gAQN4sGGQ1hYQuxpCgFaPtaEfRuNvtmpTfLXSEWGISpCVk5lFknd797Qwn4UnWnT94X8ZT/AKRH931vBiMYoix/4LhsG+pAzVbQvxuFHN/vR2iqcZwjnU9gIt2KCZhsklP9yFU9YVYrBpTdKOgSB8HgJRGQlRWuCcNSJkyZNl50S5ajlIfMpXlQG6qftHeJwiikBI8NH9qXF9ecXj2Y4ehaJqRTMpD9nYHlDWb7OpykNa0HHFJpMtZ4Rk7PEsbgpiC2UEbwC5saHvHrnEPZwK0hPM9kSqikuND+8XwfVBycXuzz1qM5giWrmfhFl4p7DzpaSqV5wPy69v0isSyp2LAi4L05QE4tdgRafQSiUSKk/CMTIq4Kn6ftEsmS969AIZI4WCKOOyf0hNjVrYNJnEBiC3MUrDDCzPKtRSgtlABGhf8AaBV4MpuSRyp8RG8OQnMAksb+YixcEODA8jdHyHxpjVBfIAlJBSMzD3HpvRr1iFMsDMAElQOVyAXICieQHlLQDPOY1WWYAJdwGDbM/aJPxDOQC5DEuljzZnfoRraL5It5qj2Sz2CEryJck5qPyDVo7K9IixiUJcZBlyqL61zZKvr5WHOBFTS5JJLhiliAwsBs29ebuYU8SnZihgQEGjqer3NBsB0hmNWzPlz0hvhMKpKVulK1Cp1YWy7E3NNAegIGFSE+JkT5mDFyAzuwfkk94VYTipz5RLPnJcFZIN6igYh3BLt8YYTMYQkJMs5Xf3y79clu0Sdp1YqOblsPmYSWc3kSUAe+KMWdt70ibDypZWtPhpASTZJJ95tH0hbhccqdOCUyiZinA8zBmN/LonnVoZmWxzZSSpRchQUMyTo6CzE0hb09srlZwZSQVZUJcZQWArfMQK6t84mVhUHKXoVJDMxF6HKA700bnDPB4RwFOc1KkZjt7wIP7k9ILRgGKyEkkggBgB83uHtSCjB+ouU0VOTPlLCjlGYe7Qsah6O5vtrEk6ShKZZTLS63dwd2DCJuA8CWZ4SpC0oSFO73JSXzZW/IKNvFy/8AYpYyE5vIXSx6XoXtyiOOvtZcc1PZWMLw+Ur3ZaWL7OAS1au4Gw/cnhsgJ8QBKQRZkvuz32htMnYWTNAVOlpmqslZa93GjvqRDfB8KQgKa6/eqOdRTnrA/TkyPMqK/hjU5kpQvK4UXZmcdNIfSE+dCBlBKHKiMx91wK6RNMwaCgJyK8rAaENS/wC3aJ14BKilQK0qSAAQ2lncVhkIUJnNM44TNURMQpgUlnAA3B05RxicatMwpdAAFDlBrlfrU07wcMMySEuCakggEnd2+kQfgwF5yFlRDEZnFsuo2hl0KVNtnCMZM8FS15cz+W1qaDqY3KxCzIK/zByPKNFN+sSYfhyAhSWWQpnzKGnQCO0YMJQUZl5NiU9Wdniifb6BTUYEg6v+0dpWrcQOuaWdqXJAP7R346+X33iuSB4sMwSAX3YPC6azzENYv2AEZGRuSXFAQf3P9F/RVOKpID7FxBmAQrIlStQ9VAXjUZGG6kzZLaR3OBNWT/yEBTph2S3JX7RkZE5MGhh7K4l5i0lrDV+9ukXbIGjIyNvjO4mHylUyBeGSdBEJwaYyMjQZ+TOF4QRSPbb2MTMHjShlmC40WNv92xjIyAyRUotMbinKMrQi4BwUJlLxU5PlRSWhSffW7Bx/aDU7tBkovUypRfXwkA/BMZGRz3HjSXsdCMuVt+9GT8MgiqQOj/QwKcLLNGBGlf1jIyFzghsJMFm8Jd2p0IgU8IWLH4PGRkI4jeTO08JceZTdUkfMQHxj2eSGKFDmRX1jIyCf2q0Cny0yHgPs8TNDqJbZg0W+ZwQNQPyvGRkEm5K2BL7Xogw/BlIWJkpIQtNnT2I2qDBmEkTC6piCATQACh1PKNRkVw3ZTkP8Lhwz5S+lAIKTLOgPwjIyNKM8mSoBDvm60iJaiagjvb4GNRkRkTs8n9oeHTJuNmKCpYSpQJL28oS41JpRiI9X4bhUplS0g+6lIBNyw1O8ZGQmGRu7H5MajVeoUqW+tOSj+0SIlEan1MZGQxKxDdaJUFrn5/pHeaMjInRVWaM3p/yjQncj2IMZGQvk7D4qrMUyhqI7ryjIyLAP/9k="
                                            alt="Card image cap"
                                            width="100%"
                                            height="200px">
                                        <div class="card-img-overlay">
                                            <h5 class="card-title" style="color: white; font-size: 35px; font-weight: bold;">Category</h5>
                                        </div>
                                        <li data-name="load" class="list-group-item" style="display: table; padding:0; height: 55px;">
                                            <button {{ count($dishes) > 6 ?: 'disabled' }} name="scroll_up" type="button" style="width: 100%; height: 100%; background-color: transparent; outline: none !important; border-color: transparent">
                                                <i class="fas fa-arrow-up"></i>
                                            </button>
                                        </li>
                                        <div class="container" style="max-height: 520px;">
                                            @php($activeDishes = true)
                                            @foreach($dishes->chunk(5) as $dishesChunk)
                                                <div class="dish-container {{ $activeDishes ? 'active' : '' }}"
                                                     style="max-height: 650px; {{ $activeDishes ? '' : 'display:none' }}">
                                                    @php($activeDishes = false)
                                                    <ul class="list-group list-group-flush col-12">
                                                        @foreach($dishesChunk as $dish)
                                                            <li class="list-group-item " style="display: table; padding: 15px 20px 0 20px">
                                                                <div class="row">
                                                                    <div class="col-10 float-left">
                                                                        <div class="row">
                                                                            <div class="col-7" style="word-wrap: break-word!important;  display: inline-block; padding: 10px 0 10px 0">
                                                                                <p style="padding: 15px 0 15px 0 ">{{ $dish->getName() }}</p>
                                                                            </div>
                                                                            <div class="col-3" style="display: inline-block; padding: 10px 0 10px 0">
                                                                                @if($dish->getPriceWithDiscount())
                                                                                    <p  style="display: table-cell; vertical-align: middle;  padding: 15px 0 15px 0"><del>{{ $dish->getPrice() }}</del> {{ $dish->getPriceWithDiscount() }}</p>
                                                                                @else
                                                                                    <p style="display: table-cell; vertical-align: middle;  padding: 15px 0 15px 0 ">{{ $dish->getPrice() }}</p>
                                                                                @endif
                                                                            </div>
                                                                            <div data-content="span" class="col-2" style="display: inline-block; padding: 10px 0 10px 0">
                                                                                <span style="{{ $dish->getQuantity() ? '' : 'display: none;' }} margin-top: 40%;" class="badge badge-pill badge-secondary">{{ $dish->getQuantity() }}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="float-right col-2">
                                                                        <div class="row">
                                                                            <div class="add center ">
                                                                                <a data-id="{{ $dish->getId() }}" class="pushme white plus">
                                                                                    <span class="inner white"><i class="fas fa-plus"></i></span>
                                                                                </a>
                                                                            </div>
                                                                            <div class="add center ">
                                                                                <a data-id="{{ $dish->getId() }}"class="pushme white minus">
                                                                                    <span class="inner white"><i class="fas fa-minus"></i></span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endforeach
                                        </div>
                                        <li class="list-group-item" style="display: table; padding:0; height: 55px;">
                                            <button {{ count($dishes) > 6 ?: 'disabled' }} name="scroll_down" type="button" style="width: 100%; height: 100%; background-color: transparent; outline: none !important; border-color: transparent">
                                                <i class="fas fa-arrow-down"></i>
                                            </button>
                                        </li>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-12 text-center">
                        <h1>No dishes were found.</h1>
                    </div>
                @endforelse
            </div>
            <a class="carousel-control-prev col-1 arrow-left" href="#carouselExampleControls" role="button" data-slide="prev" style="min-height:84vh;">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a name="load_more_categories" class="carousel-control-next col-1 arrow-right" href="#carouselExampleControls" role="button" data-slide="next" style="min-height:84vh;">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.pushme.plus').click(function () {
            var currentDishButton = $(this);
            var id = {'id': $(this).data('id')};
            $.ajax({
                url: '{{ route('orders.inc') }}',
                type: 'POST',
                data: id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function(response) {
                    $(currentDishButton).parents('li').find('div[data-content="span"] span').text(response).css('display', '');
                },
            });
        });
        $('.pushme.minus').click(function () {
            var currentDishButton = $(this);
            var id = {'id': $(this).data('id')};
            $.ajax({
                url: '{{ route('orders.dec') }}',
                type: 'POST',
                data: id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function(response) {
                    if (response == 0) {
                        (currentDishButton).parents('li').find('div[data-content="span"] span').css('display', 'none');
                    } else {
                        $(currentDishButton).parents('li').find('div[data-content="span"] span').text(response).css('display', '');
                    }
                },
            });
        });
        $('button[name="scroll_up"]').click(function () {
            var button = $(this);
            var current = $(button).parents('div.card').children('.container').children('.active');
            var prev = $(current).prev();
            if (! $(prev).length) {
                prev = $(button).parents('div.card').children('.container').children('.dish-container').last();
            }
            $(current).removeClass('active').slideUp(400);
            $(prev).addClass('active').slideDown(400);
        });
        $('button[name="scroll_down"]').click(function () {
            var button = $(this);
            var current = $(button).parents('div.card').children('.container').children('.active');
            var next = $(current).next();
            if (! $(next).length) {
                next = $(button).parents('div.card').children('.container').children('.dish-container').first();
            }
            $(current).removeClass('active').slideUp(400);
            $(next).addClass('active').slideDown(400);
        });
    </script>
@endsection
