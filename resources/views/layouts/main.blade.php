<!doctype html>
<html lang="en">
    <head>
        <title>@yield('title')</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
        <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
        @yield('styles')
    </head>
    <body>
        @yield('modal')
        <div class="container-fluid" style="padding: 0">
            <nav id="header" class="navbar navbar-light bg-light">
                <button class="navbar-toggler" type="button" name="collapser">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a href="{{ route('home') }}" id="user-side-link" class="btn btn-primary">User side</a>
                @auth
                    <ul class="nav nav-pills">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle dropdown-user"
                               data-toggle="dropdown"
                               href="#"
                               role="button"
                               aria-haspopup="true"
                               aria-expanded="false">{{ Auth::user()->name }}</a>
                            <div class="dropdown-menu main text-center" style="padding:0">
                                <a class="dropdown-item inner-shadow"
                                   href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                    Logout <i class="fas fa-sign-out-alt"></i>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                @endauth
            </nav>
            <div id="nav" class="navbar navbar-light bg-light" role="tablist">
                <div id="main-links" class="list-group" style="width:100%">
                    @guest
                        <a href="{{ route('login.form') }}" class="list-group-item list-group-item-action inner-shadow">Login</a>
                        <a href="{{ route('password.request') }}" class="list-group-item list-group-item-action inner-shadow">Reset password</a>
                    @else
                        <a name="menus" href="{{ route('menus.index') }}" class="list-group-item list-group-item-action inner-shadow">Menus</a>
                        <a name="categories" href="{{ route('categories.index') }}" class="list-group-item list-group-item-action inner-shadow">Categories</a>
                        <a name="ingredients" href="{{ route('ingredients.index') }}" class="list-group-item list-group-item-action inner-shadow">Ingredients</a>
                        <a name="dishes" href="{{ route('dishes.index') }}" class="list-group-item list-group-item-action inner-shadow">Dishes</a>
                        <a name="delivery_types" href="{{ route('delivery_types.index') }}" class="list-group-item list-group-item-action inner-shadow">Delivery types</a>
                        @if(Auth::user()->role->name == App\Models\Role::ADMIN || Auth::user()->role->name == App\Models\Role::SUPER_ADMIN)
                            <a name="discounts" href="{{ route('discounts.index')}}" class="list-group-item list-group-item-action inner-shadow">Discounts</a>
                            <a name="users" href="{{ route('users.index')}}" class="list-group-item list-group-item-action inner-shadow">Users</a>
                        @endif
                    @endguest
                </div>
            </div>
            <div id="content" class="col-md-12">
                @include('parts.message')
                @yield('content')
            </div>
        </div>
        <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
        <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
        <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="{{ URL::asset('js/validation-functions.js') }}"></script>
        <script src="{{ URL::asset('js/admin-side.js') }}"></script>
        @yield('scripts')
    </body>
</html>
