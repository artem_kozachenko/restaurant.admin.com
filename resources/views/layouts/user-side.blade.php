<!doctype html>
<html lang="en">
    <head>
        <title>@yield('title')</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
        <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
        @yield('head')
    </head>
    <body>
        @yield('modal')
        @yield('preloader')
        <div id="search-results">
            <div id="start"></div>
        </div>
        <div id="wrapper">
            <div class="container-fluid" style="padding: 0">
                <nav id="header" class="navbar navbar-expand-lg  navbar-dark bg-dark" style="width:100%">
                    <a class="navbar-brand" href="{{ route('home') }}">Restaurant</a>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li name="menu" class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">Menu</a>
                            </li>
                            <li name="dishes" class="nav-item">
                                <a class="nav-link" href="{{ route('dishes') }}">Dishes</a>
                            </li>
                            <li name="cart" class="nav-item">
                                <a class="nav-link" href="{{ route('cart') }}">Cart</a>
                            </li>
                        </ul>
                    </div>
                    <form id="search-form" class="form-inline col-lg-6" method="get" action="{{ route('dishes') }}">
                        <input name="search" id="search" class="form-control col-lg-12" type="search" placeholder="Search" aria-label="Search">
                    </form>
                </nav>
                <div id="content" class="col-md-12" style="width:100%">
                    @yield('content')
                </div>
            </div>
        </div>
        <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
        <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
        <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
        <script src="{{ URL::asset('js/user-side.js') }}"></script>
        <script src="{{ URL::asset('js/search.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
        <script src="{{ URL::asset('js/validation-functions.js') }}"></script>
        @yield('scripts')
    </body>
</html>
