@if(session('error') or session('success') or session('info'))
    <br>
    <div class="container-fluid m-t-md">
        @if (session('error'))
            @foreach(collect(session()->pull('error')) as $message)
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {{ $message }}
                </div>
            @endforeach
        @endif
        @if (session('success'))
            @foreach(collect(session()->pull('success')) as $message)
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {{ $message }}
                </div>
            @endforeach
        @endif

        @if (session('info'))
            @foreach(collect(session()->pull('info')) as $message)
                <div class="alert alert-info alert-dismissable" role="alert">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {{ $message }}
                </div>
            @endforeach
        @endif
    </div>
@endif
