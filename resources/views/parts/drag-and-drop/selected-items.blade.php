<div data-for="{{ $type }}" class="form-group drop-items single-form drop-down-container {{ $errors->has($type . '_checked') ? 'error-drop' : '' }}">
    <div class="fixed">
        <p class="text-center">Selected</p>
        <input name="selected-items-search"
               class="form-control mr-sm-2"
               data-role="search"
               data-ul-type="selected-{{ $type }}"
               type="search"
               placeholder="Search"
               aria-label="Search">
    </div>
    <div class="scrollable">
        <ul data-type="selected-{{ $type }}" class="droptrue selected-items">
            <input {{ $checked ? 'checked' : '' }} name="{{ $type }}_checked" type="checkbox" style="width: 0; height: 0" class="form-check-input">
            <input type="hidden" name="{{ $type }}" value="">
            @if($double)
                @foreach($items as $item)
                    <li data-id="{{ $item->id }}" class="list-group-item">{{ get_class($item) == \App\Models\MenuDish::class ? $item->dish->name : $item->name }}
                        @if($input  )
                            @include('parts.drag-and-drop.parts.input', ['item' => $item])
                        @endif
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
