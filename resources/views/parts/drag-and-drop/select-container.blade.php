<div class="col-md-12" style="padding: 0">
    <p class="control-label text-center" style="margin: 30px">{{ strtoupper($required['title']) }}</p>
    @include('parts.drag-and-drop.available-items', [
        'items' => $required['firsItems'],
        'type' => $required['firstType'],
        'input' => $flags['input']
    ])
    @include('parts.drag-and-drop.selected-items', [
        'items' => $optional['secondItems'],
        'type' => $optional['secondType'] ?? $required['firstType'],
        'double' => $flags['double'],
        'input' => $flags['input'],
        'checked' => $flags['checked']
    ])
    <hr>
</div>
