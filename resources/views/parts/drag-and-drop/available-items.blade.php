<div data-for="{{ $type }}" class="form-group drop-items single-form drop-down-container {{ $errors->has($type . '_checked') ? 'error-drop' : '' }}">
    <div class="fixed">
        <p class="text-center">Available</p>
        <input name="available-items-search"
               class="form-control mr-sm-2"
               data-role="search"
               data-ul-type="available-{{ $type }}"
               type="search"
               placeholder="Search"
               aria-label="Search">
    </div>
    <div class="scrollable">
        <ul data-type="available-{{ $type }}" class="droptrue available-items-{{ $type }}">
            @foreach($items as $item)
                <li data-id="{{ $item->id }}" class="list-group-item">
                    {{ $item->name }}
                    @if($input)
                        @include('parts.drag-and-drop.parts.disabled-input', ['item' => $item])
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>
