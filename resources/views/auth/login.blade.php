@extends('layouts.main')

@section('title', 'Login')

@section('content')
    <div class="container-fluid col-md-6">
        <div style="padding-top: 20%;">
            <form id="login" action="{{ route('login') }}" class="single-form" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('email') }}">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Enter password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="remember" name="remember">
                    <label class="form-check-label" for="remember">Remember me</label>
                </div>
                <button name="login" type="submit" class="btn btn-primary col-md-12">Log In</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#login').validate({
            errorClass: 'is-invalid',
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 6
                },

            },
            messages: {
                email: {
                    required: 'Email field is required.',
                },
                password: {
                    required: 'Password field is required.',
                }
            }
        });
    </script>
@endsection
