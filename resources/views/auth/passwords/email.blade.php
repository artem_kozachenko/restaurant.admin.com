@extends('layouts.main')

@section('title', 'Reset password')

@section('content')
    <div class="container-fluid col-md-6">
        <div style="padding-top: 25%;">
            <form id="reset-email" class="single-form" method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="Enter email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary col-md-12">Send Password Reset Link</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#reset-email').validate({
            errorClass: 'is-invalid',
            rules: {
                email: {
                    required: true,
                    email: true,
                },
            },
            messages: {
                email: {
                    required: 'Email field is required.',
                },
            }
        });
    </script>
@endsection
