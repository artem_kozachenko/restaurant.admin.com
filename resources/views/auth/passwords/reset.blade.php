@extends('layouts.main')

@section('title', 'Reset password')

@section('content')
    <div class="container-fluid col-md-6">
        <div style="padding-top: 15%;">
            <form class="single-form" method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="Enter email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Enter password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Password confirmation</label>
                    <input name="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" id="password_confirmation" placeholder="Confirm password">
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary col-md-12">Reset Password</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#register').validate({
            errorClass: 'is-invalid',
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 8,
                },
                password_confirmation: {
                    required: true,
                    minlength: 8,
                    equalTo: '#password'
                }
            },
            messages: {
                name: 'Name field is required.',
                email: {
                    required: 'Email field is required.',
                },
                password: {
                    required: 'Password field is required.',
                },
                password_confirmation: {
                    required: 'Password confirmation field is required.',
                    equalTo: 'Confirmation do not match.',
                },
            }
        });
    </script>
@endsection
