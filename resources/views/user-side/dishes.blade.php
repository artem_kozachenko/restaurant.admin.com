@extends('layouts.user-side')

@section('title', 'Dishes')

@section('content')
    <div class="offset-md-1 col-md-10" style="padding-top:50px;">
        <div class="row">
            @forelse($dishes as $dish)
                <div class="col-3 p-3">
                    <a class="card-a" href="{{ route('dish', $dish) }}">
                        <div class="card border-primary">
                            <img class="card-img-top"
                                 src="http://restaurant.admin.com/storage/dishes/{{ $dish->photo ? $dish->photo->name : 'default.png' }}"
                                 alt="{{ $dish->photo }}"
                                 width="100%"
                                 height="180px">
                            <div class="card-header bg-transparent border-primary">
                                <h5 class="card-title">{{ $dish->name }}</h5>
                            </div>
                            <div class="card-body" style="min-height: 250px; max-height: 250px">
                                <p class="card-text dish-description">{!! $dish->description ? $dish->description->content : 'No' !!}</p>
                            </div>
                            <ul class="list-group list-group-flush ">
                                <li class="list-group-item border-primary">Price: {{ $dish->price }}$</li>
                            </ul>
                        </div>
                    </a>
                </div>
            @empty
                <din class="col-12 text-center">
                    <h3 >No dishes were found.</h3>
                </din>
            @endforelse
        </div>
        <diw class="d-flex justify-content-center">
            {{ $dishes->links() }}
        </diw>
    </div>
@endsection
