@extends('layouts.user-side')

@section('title', 'Menu')

@section('content')
    <div class="offset-md-1 col-md-10" style="padding-top:50px;">
            <div class="row">
                @forelse($menu as $category => $dishes)
                    <div class="col-3" style="padding: 10px">
                        <div class="card" data-toggle="modal" data-target="#{{ $dishes->first()->getCategoryUrl() }}">
                            <img class="card-img-top" src="http://restaurant.admin.com/storage/dishes/default.png" alt="Card image cap" width="150px" height="200px">
                            <div class="card-img-overlay">
                                <h2 class="card-title">{{ $category }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade col-12" id="{{ $dishes->first()->getCategoryUrl() }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog " style="margin: 25px 0 25px 0 ; max-width: 100%; " role="document">
                            <div class="modal-content" style="height: calc(100vh - 50px)">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">{{ $category }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body menu" style="overflow-x: hidden; overflow-y: overlay">
                                    <div class="row">
                                    @foreach($dishes as $dish)
                                            <div class="col-3">
                                                <a class="card-a" href="{{ route('dish', $dish->getId()) }}">
                                                    <div class="card border-primary">
                                                        <img class="card-img-top"
                                                             src="http://restaurant.admin.com/storage/dishes/{{ $dish->getPhoto() ? $dish->getPhoto() : 'default.png' }}"
                                                             alt="{{ $dish->getPhoto() }}"
                                                             width="100%"
                                                             height="180px">
                                                        <div class="card-header bg-transparent border-primary">
                                                            <h5 class="card-title">{{ $dish->getName() }}</h5>
                                                        </div>
                                                        <div class="card-body" style="min-height: 250px; max-height: 250px">
                                                            <p class="card-text dish-description">{!! $dish->getDescription() ? $dish->getDescription() : 'No' !!}</p>
                                                        </div>
                                                        <ul class="list-group list-group-flush ">
                                                            @if(is_null($dish->getPriceWithDiscount()))
                                                                <li class="list-group-item border-primary">Price: {{ $dish->getPrice() }}$</li>
                                                            @else
                                                                <li class="list-group-item border-primary">Price: <del>{{ $dish->getPrice() }}$</del> {{ $dish->getPriceWithDiscount() }}$</li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <din class="col-12 text-center">
                        <h3>No dishes were found.</h3>
                    </din>
                @endforelse

            </div>
    </div>
@endsection
