@extends('layouts.user-side')

@section('title', "$dish->name")

@section('content')
    <div class="offset-md-1 col-md-10" style="padding-top:50px;">
        <div class="single-form">
            <img src="http://restaurant.admin.com/storage/dishes/{{ $dish->photo ? $dish->photo->name : 'default.png' }}"
                 alt="{{ $dish->name }}"
                 width="300px"
                 height="200px">
            <p>Name: {{ $dish->name }}</p>
            <p>Price: {{ $dish->price }}</p>
            <p>Category: {{ $dish->category->name }}</p>
            <p>Description: {!! $dish->description ? $dish->description->content : 'No' !!}</p>
            <p>Ingredients:</p>
            @foreach($dish->ingredients as $ingredient)
                <p>{{ $ingredient->name }}</p>
            @endforeach
        </div>
    </div>
@endsection
