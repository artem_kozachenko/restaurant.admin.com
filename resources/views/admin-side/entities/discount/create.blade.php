@extends('layouts.main')

@section('title', 'Create discount')

@section('content')
    <div class="offset-md-1 col-md-10">
        <form id="create" class="single-form" action="{{ route('discounts.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name" class="control-label">Discount name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="percent" class="control-label">Percent</label>
                <input type="text" class="form-control {{ $errors->has('percent') ? ' is-invalid' : '' }}" name="percent" id="percent" value="{{ old('percent') }}">
                @if ($errors->has('percent'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('percent') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="from" class="control-label">From</label>
                    <br>
                    <input type="time" id="from" name="from" class="form-control {{ $errors->has('from') ? ' is-invalid' : '' }}">
                    @if ($errors->has('from'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('from') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="to" class="control-label">To</label>
                    <br>
                    <input type="time" id="to" name="to" class="form-control {{ $errors->has('to') ? ' is-invalid' : '' }}">
                    @if ($errors->has('to'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('to') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-success">Create</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js" integrity="sha256-fvFKHgcKai7J/0TM9ekjyypGDFhho9uKmuHiFVfScCA=" crossorigin="anonymous"></script>
    <script>
        $("#percent").mask('99.99');
        $('#create').validate({
            errorClass: 'is-invalid',
            rules: {
                name: 'required',
                percent: {
                    required: true,
                    number: true
                },
                from: {
                    required: true,
                    time: 'from'
                },
                to: {
                    required: true,
                    time: 'to'
                },
            },
            messages: {
                name: 'Discount name field is required.',
                percent: {
                    required: 'Percent field is required.'
                },
            }
        });
    </script>
@endsection
