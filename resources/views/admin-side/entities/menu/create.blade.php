@extends('layouts.main')

@section('title', 'Add menu')

@section('content')
    <div class="offset-md-1 col-md-10">
        <form id="create" class="single-form" action="{{ route('menus.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="date" class="control-label">Select week</label>
                <input type="text"
                       name="date"
                       id="datepicker"
                       class="form-control {{ $errors->has('date') ? ' is-invalid' : '' }}">
                @if ($errors->has('date'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
            </div>
            @include('parts.drag-and-drop.select-container', [
                $required = [
                    'firsItems' => $remainDishes,
                    'firstType' => 'dishes',
                    'title' => 'dishes',
                ],
                $optional = [
                    'secondItems' => $lastMenuDishes,
                    'secondType' => 'dishes',
                ],
                $flags = [
                    'input' => false,
                    'double' => true,
                    'checked' => false,
                ],
            ])
            <button type="submit" class="btn btn-success">Add</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/select-container.js') }}"></script>
    <script>
        $( function() {
            $('#datepicker').datepicker({
                showWeek: true,
                weekHeader: "W",
                firstDay: 1
            });
        } );
        $('#create').validate({
            errorClass: 'is-invalid',
            submitHandler: function (form) {
                collectItems('dishes');
                form.submit();
            },
            errorPlacement: function (error, element) {
                removeError(error, element, ['dishes_checked']);
            },
            rules: {
                date: 'required',
                dishes_checked: {
                    'checked': 'dishes'
                },
            },
            messages: {
                date: 'Date field is required.',
            }
        });
    </script>
@endsection
