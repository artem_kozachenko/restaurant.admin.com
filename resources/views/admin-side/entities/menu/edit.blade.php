@extends('layouts.main')

@section('title', 'Update menu')

@section('content')
    <div class="offset-md-1 col-md-10">
        <form id="update" class="single-form" action="{{ route('menus.update', $menu) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('parts.drag-and-drop.select-container', [
                $required = [
                    'firsItems' => $unusedDishes,
                    'firstType' => 'dishes',
                    'title' => 'dishes',
                ],
                $optional = [
                    'secondItems' => $usedDishes,
                    'secondType' => 'dishes',
                ],
                $flags = [
                    'input' => true,
                    'double' => true,
                    'checked' => true,
                ],
            ])
            <button type="submit" class="btn btn-success">Update</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/select-container.js') }}"></script>
    <script>
        $( function() {
            $('#datepicker').datepicker({
                showWeek: true,
                weekHeader: "W",
                firstDay: 1
            });
        } );
        $('#update').validate({
            errorClass: 'is-invalid',
            submitHandler: function (form) {
                collectItems('dishes');
                form.submit();
            },
            errorPlacement: function (error, element) {
                removeError(error, element, ['dishes_checked']);
            },
            rules: {
                dishes_checked: {
                    'checked': 'dishes'
                },
            },
        });
    </script>
@endsection
