@extends('layouts.main')

@section('title', 'Menus')

@section('modal')
    <div class="modal fade" id="delete_message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure that you want to delete this menu?
                </div>
                <div class="modal-footer">
                    <button type="button" name="confirm" class="btn btn-success col-md-6">Confirm</button>
                    <button type="button" name="close" class="btn btn-secondary col-md-6" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="offset-md-1 col-md-10">
        <div class="col-md-12" style="margin-top: 25px">
            <a href="{{ route('menus.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Add new menu</a>
        </div>
        <hr>
        @forelse($menus as $menu)
            <div class="accordion" id="accordion{{ $menu->id }}">
                <div class="card">
                    <div class="card-header" id="heading{{ $menu->id }}">
                        <h5 class="mb-0">
                            <button class="btn btn-link"
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#{{ $menu->id }}"
                                    aria-expanded="false"
                                    aria-controls="{{ $menu->id }}">
                                {{ $menu->date }}
                            </button>
                            <form id="form{{ $menu->id }}" class="float-right" action="{{ route('menus.destroy', $menu) }}" method="POST">
                                {{ csrf_field() }}
                                <a href="{{ route('menus.edit', $menu) }}" class="btn btn-success">
                                    <i class="far fa-edit"></i>
                                </a>
                                <input type="hidden" name="_method" value="DELETE">
                                <button name="predeletion" type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_message">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </form>
                        </h5>
                    </div>
                    <div id="{{ $menu->id }}"
                         class="collapse"
                         aria-labelledby="heading{{ $menu->id }}"
                         data-parent="#accordion{{ $menu->id }}">
                        <div class="card-body">
                            <table class="table table-hover">
                                <thead>
                                <tr class="text-uppercase text-center row">
                                    <th class="col-md-6 list-group-item-primary">Name</th>
                                    <th class="col-md-6 list-group-item-primary">Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($menu->dishes as $dish)
                                    <tr class="text-center row">
                                        <td class="col-md-6">{{ $dish->dish->name }}</td>
                                        <td class="col-md-6">{{ $dish->price }}</td>
                                    </tr>
                                @empty
                                    <tr class="text-center row">
                                        <td class="col-md-12">No dishes were found.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="card-header">
                <h5 class="mb-0 text-center">
                    <span>No menus were found.</span>
                </h5>
            </div>
        @endforelse
        <hr>
        <diw class="d-flex justify-content-center">
            {{ $menus->links() }}
        </diw>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/modal.js') }}"></script>
@endsection
