@extends('layouts.main')

@section('title', 'Users')

@section('modal')
    <div class="modal fade" id="delete_message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure that you want to delete this user?
                </div>
                <div class="modal-footer">
                    <button type="button" name="confirm" class="btn btn-success col-md-6">Confirm</button>
                    <button type="button" name="close" class="btn btn-secondary col-md-6" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="offset-md-1 col-md-10">
        <div class="col-md-12" style="margin-top: 25px">
            <a href="{{ route('pre.create.form') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Add new user</a>
        </div>
        <hr>
        <table class="table table-hover" style="margin-top: 25px; margin-bottom: 25px;">
            <thead>
            <tr class="text-uppercase text-center row">
                <th class="col-md-3 list-group-item-primary">Name</th>
                <th class="col-md-3 list-group-item-primary">Email</th>
                <th class="col-md-3 list-group-item-primary">Role</th>
                <th class="col-md-3 list-group-item-primary">Manage</th>
            </tr>
            </thead>
            <tbody>
            @forelse($users as $user)
                <tr class="text-center row">
                    <td class="col-md-3 d-flex align-items-center justify-content-center">
                        <p>{{ $user->name }}</p>
                    </td>
                    <td class="col-md-3 d-flex align-items-center justify-content-center">
                        <p>{{ $user->email }}</p>
                    </td>
                    <td class="col-md-3 d-flex align-items-center justify-content-center">
                        <p>{{ $user->role->name }}</p>
                    </td>
                    <td class="col-md-3 d-flex align-items-center justify-content-center">
                        <form id="{{ $user->id }}" action="{{ route('users.destroy', $user) }}" method="POST">
                            {{ csrf_field() }}
                            <a href="{{ route('users.edit', $user) }}" class="btn btn-success">
                                <i class="far fa-edit"></i>
                            </a>
                            <input type="hidden" name="_method" value="DELETE">
                            <button name="predeletion" type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_message">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="2" class="text-center">No categories were found</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <diw class="d-flex justify-content-center">
            {{ $users->links() }}
        </diw>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/modal.js') }}"></script>
@endsection
