@extends('layouts.main')

@section('title', 'Users')

@section('content')
    <div class="offset-md-1 col-md-10">
        <form id="update" class="single-form" action="{{ route('users.update', $user) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $user->name }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="email" class="control-label">Email address</label>
                <input type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter email" name="email" id="email" value="{{ $user->email }}">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="role_id" class="control-label">Role</label>
                <br>
                <select name="role_id" id="role_id" class="form-control {{ $errors->has('role_id') ? ' is-invalid' : '' }}">
                    <option value="{{ $user->role_id }}" selected>{{ $user->role->name }}</option>
                    @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                </select>
                @if ($errors->has('role_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('role_id') }}</strong>
                    </span>
                @endif
            </div>
            <button class="btn btn-success col-12">Update</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#update').validate({
            errorClass: 'is-invalid',
            rules: {
                name: 'required',
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                name: 'Name field is required.',
                email: {
                    required: 'Email field is required.'
                },
            }
        });
    </script>
@endsection
