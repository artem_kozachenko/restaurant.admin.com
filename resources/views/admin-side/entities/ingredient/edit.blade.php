@extends('layouts.main')

@section('title', 'Update ingredient')

@section('content')
    <div class="offset-md-1 col-md-10" style="padding-top: 10%;">
        <form id="update" action="{{ route('ingredients.update', $ingredient) }}" class="single-form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="id" value="{{ $ingredient->id }}">
            <div class="form-group">
                <label for="name" class="control-label">Ingredient name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $ingredient->name }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="photo" class="control-label">Ingredient photo</label>
                <br>
                <input type="file" name="photo" id="photo">
            </div>
            <button class="btn btn-success">Update</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#update').validate({
            errorClass: 'is-invalid',
            rules: {
                name: 'required'
            },
            messages: {
                name: 'Ingredient name field is required.'
            }
        });
    </script>
@endsection
