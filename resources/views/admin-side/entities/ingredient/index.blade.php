@extends('layouts.main')

@section('title', 'Ingredients')

@section('modal')
    <div class="modal fade" id="delete_message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure that you want to delete this ingredient?
                </div>
                <div class="modal-footer">
                    <button type="button" name="confirm" class="btn btn-success col-md-6">Confirm</button>
                    <button type="button" name="close" class="btn btn-secondary col-md-6" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="offset-md-1 col-md-10">
        <div class="col-md-12" style="margin-top: 25px">
            <a href="{{ route('ingredients.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Add new ingredient</a>
        </div>
        <hr>
        <table class="table table-hover" style="margin-top: 25px; margin-bottom: 25px;">
            <thead>
            <tr class="text-uppercase text-center row">
                <th class="col-md-3 list-group-item-primary">Name</th>
                <th class="col-md-6 list-group-item-primary">Image</th>
                <th class="col-md-3 list-group-item-primary">Manage</th>
            </tr>
            </thead>
            <tbody>
            @forelse($ingredients as $ingredient)
                <tr class="text-center row">
                    <td class="col-md-3 d-flex align-items-center justify-content-center">
                        <p href="{{ route('ingredients.show', $ingredient) }}">{{ $ingredient->name }}</p>
                    </td>
                    <td class="col-md-6 d-flex align-items-center justify-content-center">
                        <img class="img img-fluid"
                             src="http://restaurant.admin.com/storage/ingredients/{{ $ingredient->photo ? $ingredient->photo->name : 'default.png' }}"
                             alt="{{ $ingredient->name }}"
                             width="200px"
                             height="150px">
                    </td>
                    <td class="col-md-3 d-flex align-items-center justify-content-center">
                        <form id="{{ $ingredient->name }}" action="{{ route('ingredients.destroy', $ingredient) }}" method="POST">
                            {{ csrf_field() }}
                            <a href="{{ route('ingredients.edit', $ingredient) }}" class="btn btn-success">
                                <i class="far fa-edit"></i>
                            </a>
                            <input type="hidden" name="_method" value="DELETE">
                            <button name="predeletion" type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_message">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">No ingredients were found</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <hr>
        <diw class="d-flex justify-content-center">
            {{ $ingredients->links() }}
        </diw>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/modal.js') }}"></script>
@endsection
