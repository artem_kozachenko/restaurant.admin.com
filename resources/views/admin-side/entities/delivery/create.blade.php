@extends('layouts.main')

@section('title', 'Add delivery type')

@section('content')
    <div class="offset-md-1 col-md-10" style="padding: 10%">
        <form id="create" class="single-form" action="{{ route('delivery_types.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name" class="control-label">Delivery type name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="price" class="control-label">Price</label>
                <input type="text" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" id="price" value="{{ old('price') }}">
                @if ($errors->has('price'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                @endif
            </div>
            <button class="btn btn-success">Add</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#create').validate({
            errorClass: 'is-invalid',
            rules: {
                name: 'required',
                price: {
                    required: true,
                    number: true
                },
            },
            messages: {
                name: 'Delivery type name field is required.',
                price: {
                    required: 'Price field is required.',
                },
            }
        });
    </script>
@endsection
