@extends('layouts.main')

@section('title', 'Update delivery type')

@section('content')
    <div class="offset-md-1 col-md-10" style="padding: 10%">
        <form id="update" class="single-form" action="{{ route('delivery_types.update', $deliveryType) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="name" class="control-label">Delivery type name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $deliveryType->name }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="price" class="control-label">Price</label>
                <input type="text" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" id="price" value="{{ $deliveryType->price }}">
                @if ($errors->has('price'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                @endif
            </div>
            <button class="btn btn-success">Update</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#update').validate({
            errorClass: 'is-invalid',
            rules: {
                name: 'required',
                price: {
                    required: true,
                    number: true
                },
            },
            messages: {
                name: 'Delivery type name field is required.',
                price: {
                    required: 'Price field is required.',
                },
            }
        });
    </script>
@endsection
