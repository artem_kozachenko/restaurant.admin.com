@extends('layouts.main')

@section('title', 'Add category')

@section('content')
    <div class="offset-md-1 col-md-10" style="padding: 10%">
        <form id="create" class="single-form" action="{{ route('categories.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name" class="control-label">Category name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="parent" class="control-label">Parent category(if necessary)</label>
                <br>
                <select name="parent_category_id" id="parent" class="form-control">
                    <option value="" selected>Select parent category</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-success">Add</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#create').validate({
            errorClass: 'is-invalid',
            rules: {
                name: 'required',
            },
            messages: {
                name: 'Category name field is required.',
            }
        });
    </script>
@endsection
