@extends('layouts.main')

@section('title', 'Update category')

@section('content')
    <div class="offset-md-1 col-md-10" style="padding: 10%">
        <form id="update" class="single-form" action="{{ route('categories.update', $category) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="id" value="{{ $category->id }}">
            <div class="form-group">
                <label for="name" class="control-label">Category name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $category->name }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="parent" class="control-label">Parent category(if necessary)</label>
                <br>
                <select name="parent_category_id" id="parent" class="form-control">
                    @if(! is_null($category->parent_category_id))
                        <option value="{{ $category->parentCategory->id }}" selected>{{ $category->parentCategory->name }}</option>
                    @else
                        <option value="" selected>Select parent category</option>
                    @endif
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-success">Update</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#update').validate({
            errorClass: 'is-invalid',
            rules: {
                name: 'required',
            },
            messages: {
                name: 'Category name field is required.',
            }
        });
    </script>
@endsection

