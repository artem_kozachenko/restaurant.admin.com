@extends('layouts.main')

@section('title',  $dish->name)

@section('content')
    <div class="offset-md-1 col-md-10">
        <div class="col-md-12" style="margin-top: 25px">
            <a href="{{ route('dishes.index') }}" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back to dishes</a>
        </div>
        <hr>
        <div class="single-form">
            <img src="http://restaurant.admin.com/storage/dishes/{{ $dish->photo ? $dish->photo->name : 'default.png' }}"
                 alt="{{ $dish->name }}"
                 width="300px"
                 height="200px">
            <p>Name: {{ $dish->name }}</p>
            <p>Price: {{ $dish->price }}</p>
            <p>Category: {{ $dish->category->name }}</p>
            <p>Discount: {{ $dish->discount ? $dish->discount->name . ' - ' . $dish->discount->percent . '%' : 'No' }}</p>
            <p>Description: {!!  $dish->description ? $dish->description->content : 'No' !!}</p>
            <p>Ingredients:</p>
            @foreach($dish->ingredients as $ingredient)
                <p>{{ $ingredient->name }}</p>
            @endforeach
            <hr>
            <form action="{{ route('dishes.destroy', $dish) }}" method="POST">
                {{ csrf_field() }}
                <a href="{{ route('dishes.edit', $dish) }}" class="btn btn-success" style="width: 49%">
                    <i class="far fa-edit"></i> Update
                </a>
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger" style="width: 49%">
                    <i class="fas fa-trash-alt"></i> Delete
                </button>
            </form>
        </div>
    </div>
@endsection
