@extends('layouts.main')

@section('title', 'Update dish')

@section('content')
    <div class="offset-md-1 col-md-10">
        <form id="update" class="single-form" action="{{ route('dishes.update', $dish) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="id" value="{{ $dish->id }}">
            <div class="form-group">
                <label for="name" class="control-label">Dish name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $dish->name }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="price" class="control-label">Dish price</label>
                <input type="text" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" id="name" value="{{ $dish->price }}">
                @if ($errors->has('price'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="photo" class="control-label">Dish photo</label>
                <br>
                <input type="file" name="photo" id="photo">
            </div>
            <div class="form-group">
                <label for="category" class="control-label">Category</label>
                <br>
                <select name="category_id" id="category" class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}">
                    <option value="{{ $dish->category->id }}" selected>{{ $dish->category->name }}</option>
                    @foreach($categories as $category)
                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                    @endforeach
                </select>
                @if ($errors->has('category_id'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="discount" class="control-label">Discount</label>
                <br>
                <select name="discount_id" id="discount" class="form-control {{ $errors->has('discount_id') ? ' is-invalid' : '' }}">
                    @if ($dish->discount_id)
                        <option value="">Remove discount</option>
                        <option value="{{ $dish->discount->id }}" selected>{{ $dish->discount->name . ' - ' . $dish->discount->percent . '%' }}</option>
                    @else
                        <option value="" selected>Select discount</option>
                    @endif
                    @foreach($discounts as $discount)
                        <option value="{{ $discount->id }}">{{ $discount->name . ' - ' . $discount->percent . '%'}}</option>
                    @endforeach
                </select>
                @if ($errors->has('discount_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('discount_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="description" class="control-label">Description</label>
                <br>
                <textarea name="description" class="summernote form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" id="description">{{ $dish->description ? $dish->description->content : '' }}</textarea>
                @if ($errors->has('description'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
                @endif
            </div>
            @include('parts.drag-and-drop.select-container', [
                $required = [
                    'firsItems' => $unusedIngredients,
                    'firstType' => 'ingredients_ids',
                    'title' => 'recipe',
                ],
                $optional = [
                    'secondItems' => $usedIngredients,
                    'secondType' => 'ingredients_ids',
                ],
                $flags = [
                    'input' => false,
                    'double' => true,
                    'checked' => true
                ],
            ])
            <button class="btn btn-success">Update</button>
        </form>
    </div>
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/select-container.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $('.summernote').summernote();
        $('#update').validate({
            errorClass: 'is-invalid',
            submitHandler: function (form) {
                collectItems('ingredients_ids');
                form.submit();
            },
            errorPlacement: function (error, element) {
                removeError(error, element, ['ingredients_ids_checked']);
            },
            rules: {
                name: 'required',
                price: {
                    'required': true,
                    'number': true
                },
                ingredients_ids_checked: {
                    'checked': 'ingredients_ids'
                },
            },
            messages: {
                name: 'Dish name field is required.',
                price: {
                    required: 'Dish price field is required.'
                },
            }
        });
    </script>
@endsection
