@extends('layouts.main')

@section('title', 'Dish export')

@section('content')
    <div class="offset-md-1 col-md-10">
        <form id="export" class="single-form" action="{{ route('dishes.history') }}" method="get">
            <div class="form-group">
                <label for="dish_id" class="control-label">Dish name</label>
                <br>
                <select name="dish_id" id="dish_id" class="form-control {{ $errors->has('dish_id') ? ' is-invalid' : '' }}">
                    <option value="" selected>Select dish</option>
                    @foreach($dishes as $dish)
                        <option value="{{ $dish->id }}">{{ $dish->name }}</option>
                    @endforeach
                </select>
                @if ($errors->has('dish_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('dish_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="date_from" class="control-label">From</label>
                    <br>
                    <input type="date" id="date_from" name="date_from" class="form-control {{ $errors->has('date_from') ? ' is-invalid' : '' }}" value="2018-01-01">
                    @if ($errors->has('date_from'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date_from') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="date_to" class="control-label">To</label>
                    <br>
                    <input type="date" id="date_to" name="date_to" class="form-control {{ $errors->has('date_to') ? ' is-invalid' : '' }}" value="{{ (string) \Carbon\Carbon::today()->addDay()->format('Y-m-d') }}">
                    @if ($errors->has('date_to'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date_to') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <button class="btn btn-success">Show history</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#export').validate({
            errorClass: 'is-invalid',
            rules: {
                dish_id: {
                    required: true,
                    valueNotEquals: '',
                    digits: true
                },
                date_from: {
                    required: true,
                },
                date_to: {
                    required: true,
                },
            },
            messages: {
                dish_id: {
                    valueNotEquals: 'You have to select dish.'
                },
            }
        });
    </script>
@endsection
