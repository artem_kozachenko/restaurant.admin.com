@extends('layouts.main')

@section('title', 'Create dish')

@section('content')
    <div class="offset-md-1 col-md-10">
        <form id="create" class="single-form" action="{{ route('dishes.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name" class="control-label">Dish name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="price" class="control-label">Dish price</label>
                <input type="text" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" id="price" value="{{ old('price') }}">
                @if ($errors->has('price'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="photo" class="control-label">Dish photo</label>
                <br>
                <input type="file" name="photo" id="photo">
            </div>
            <div class="form-group">
                <label for="category" class="control-label">Category</label>
                <br>
                <select name="category_id" id="category" class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}">
                    <option value="" selected>Select category</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
                @if ($errors->has('category_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="discount" class="control-label">Discount</label>
                <br>
                <select name="discount_id" id="discount" class="form-control {{ $errors->has('discount_id') ? ' is-invalid' : '' }}">
                    <option value="" selected>Select discount</option>
                    @foreach($discounts as $discount)
                        <option value="{{ $discount->id }}">{{ $discount->name . ' - ' . $discount->percent . '%'  }}</option>
                    @endforeach
                </select>
                @if ($errors->has('discount_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('discount_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="description" class="control-label">Description</label>
                <br>
                <textarea name="description" class="summernote" id="description">{{ old('description') }}</textarea>
            </div>
            @include('parts.drag-and-drop.select-container', [
                $required = [
                    'firsItems' => $ingredients,
                    'firstType' => 'ingredients_ids',
                    'title' => 'DRAG AND DROP REQUIRED INGREDIENTS',
                ],
                $optional = [
                    'secondItems' => null,
                    'secondType' => null,
                ],
                $flags = [
                    'input' => false,
                    'double' => false,
                    'checked' => false
                ],
            ])
            <br>
            <button type="submit" class="btn btn-success">Create</button>
        </form>
    </div>
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/select-container.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $('.summernote').summernote();
        $(document).ready(function(){
            $('#create').validate({
                submitHandler: function (form) {
                    collectItems('ingredients_ids');
                    form.submit();
                },
                errorClass: 'is-invalid',
                errorPlacement: function (error, element) {
                    removeError(error, element, ['ingredients_ids_checked']);
                },
                rules: {
                    name: 'required',
                    price: {
                        'required': true,
                        'number': true
                    },
                    category_id: {
                        valueNotEquals: ''
                    },
                    ingredients_ids_checked: {
                        'checked': 'ingredients_ids'
                    },
                },
                messages: {
                    name: 'Dish name field is required.',
                    price: {
                        required: 'Dish price field is required.'
                    },
                    category_id: {
                        valueNotEquals: 'You have to select category.'
                    },
                }
            });
        });
    </script>
@endsection
