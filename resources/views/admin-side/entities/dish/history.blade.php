@extends('layouts.main')

@section('title', 'History')

@section('content')
    <div class="offset-md-1 col-md-10">
        <div class="col-md-12" style="margin-top: 25px">
            <form action="{{ route('dishes.export') }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="dish_id" value="{{ $dish_id }}">
                <input type="hidden" name="date_from" value="{{ $date_from }}">
                <input type="hidden" name="date_to" value="{{ $date_to }}">
                <button type="submit" class="btn btn-primary">Export</button>
            </form>
        </div>
        <hr>
        <table class="table table-hover" style="margin-top: 25px; margin-bottom: 25px;">
            <thead>
            <tr class="text-uppercase text-center row">
                <th class="col-md-3 list-group-item-primary">Name</th>
                <th class="col-md-6 list-group-item-primary">Price</th>
                <th class="col-md-3 list-group-item-primary">Date</th>
            </tr>
            </thead>
            <tbody>
            @forelse($data as $row)
                <tr class="text-center row">
                    <td class="col-md-3 d-flex align-items-center justify-content-center">
                        <p>{{ $row->name }}</p>
                    </td>
                    <td class="col-md-6 d-flex align-items-center justify-content-center">
                        <p>{{ $row->price }}</p>
                    </td>
                    <td class="col-md-3 d-flex align-items-center justify-content-center">
                        <p>{{ $row->date }}</p>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">No history was found</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <diw class="d-flex justify-content-center">
            {{ $data->links() }}
        </diw>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/modal.js') }}"></script>
@endsection
