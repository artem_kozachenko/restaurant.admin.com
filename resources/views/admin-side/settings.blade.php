@extends('layouts.main')

@section('title', 'Settings')

@section('content')
    <div class="container-fluid col-md-12">
        <div class="single-form ">
            <div class="row">
                <div class="col-4">
                    <h4 class="text-center">Change password</h4>
                    <form id="change-password" action="#" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="current_password">Current password</label>
                            <input name="current_password" type="password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" placeholder="Enter current password">
                            @if ($errors->has('current_password'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('current_password') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="new_password">New password</label>
                            <input name="new_password" type="password" class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}" placeholder="Enter new password">
                            @if ($errors->has('new_password'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('new_password') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm password</label>
                            <input name="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="Confirm password">
                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary col-md-12">Change password</button>
                    </form>
                </div>
                <div class="col-4">
                    <h4 class="text-center">Change email</h4>
                    <form id="change-email" action="#" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email">New name</label>
                            <input name="new_email" type="email" class="form-control{{ $errors->has('new_email') ? ' is-invalid' : '' }}" placeholder="Enter email" value="{{ old('new_email') }}">
                            @if ($errors->has('new_email'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('new_email') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Enter password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary col-md-12">Change email</button>
                    </form>
                </div>
                <div class="col-4">
                    <h4 class="text-center">Change name</h4>
                    <form id="change-name" action="#" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="new_name">New email</label>
                            <input name="new_name" type="text" class="form-control{{ $errors->has('new_name') ? ' is-invalid' : '' }}" placeholder="Enter new name" value="{{ old('new_name') }}">
                            @if ($errors->has('new_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('new_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary col-md-12">Change name</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
