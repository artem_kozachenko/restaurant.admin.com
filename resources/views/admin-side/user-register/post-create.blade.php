@extends('layouts.single')

@section('title', 'Create user')

@section('content')
    <div class="offset-md-1 col-md-10" style="padding-top: 9%">
        <form id="create" class="single-form" action="{{ route('post.create.user', $token) }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Enter name" name="name" id="name" value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Enter password">
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password_confirmation">Confirm password</label>
                <input name="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" id="password_confirmation" placeholder="Retype password">
                @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-success">Create</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#create').validate({
            errorClass: 'is-invalid',
            rules: {
                name: 'required',
                password: {
                    required: true,
                    minlength: 8
                },
                password_confirmation: {
                    required: true,
                    minlength: 8,
                    equalTo: '#password'
                }
            },
            messages: {
                name: 'Name field is required.',
                password: {
                    required: 'Password field is required',
                },
                password_confirmation: {
                    required: 'Password confirmation field is required',
                    equalTo: 'The password confirmation does not match.'
                },
            }
        });
    </script>
@endsection
