@extends('layouts.main')

@section('title', 'Create user')

@section('content')
    <div class="offset-md-1 col-md-10">
        <form id="create" class="single-form" action="{{ route('pre.create.user') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="email" class="control-label">Email address</label>
                <input type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter email" name="email" id="email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="role_id" class="control-label">Role</label>
                <br>
                <select name="role_id" id="role_id" class="form-control {{ $errors->has('role_id') ? ' is-invalid' : '' }}">
                    <option value="" selected>Select role</option>
                    @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                </select>
                @if ($errors->has('role_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('role_id') }}</strong>
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-success">Create</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#create').validate({
            errorClass: 'is-invalid',
            rules: {
                email: {
                    required: true,
                    email: true
                },
                role_id: {
                    valueNotEquals: ''
                },
            },
            messages: {
                email: {
                    required: 'Email field is required.'
                },
                role_id: {
                    valueNotEquals: 'You have to select role.'
                },
            }
        });
    </script>
@endsection
