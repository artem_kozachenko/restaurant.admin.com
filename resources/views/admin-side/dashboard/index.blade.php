@extends('layouts.main')

@section('title', 'Dashboard')

@section('head')
    <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('content')
    <div class="offset-md-1 col-md-10" style="">


        <form id="getData" class="single-form">
            <div class="row">
                <input type="hidden" name="dishes_ids">
                <div class="form-group col-md-6">
                    <label for="date_from" class="control-label">From</label>
                    <br>
                    <input type="date" id="date_from" name="date_from" class="form-control {{ $errors->has('date_from') ? ' is-invalid' : '' }}" value="2018-01-01">
                    @if ($errors->has('date_from'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date_from') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="date_to" class="control-label">To</label>
                    <br>
                    <input type="date" id="date_to" name="date_to" class="form-control {{ $errors->has('date_to') ? ' is-invalid' : '' }}" value="{{ (string) \Carbon\Carbon::today()->addDay()->format('Y-m-d') }}">
                    @if ($errors->has('date_to'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date_to') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            @include('parts.drag-and-drop.select-container', [
                $required = [
                    'firsItems' => $dishes,
                    'firstType' => 'dishes_ids',
                    'title' => 'Select required dishes',
                ],
                $optional = [
                    'secondItems' => null,
                    'secondType' => null,
                ],
                $flags = [
                    'input' => false,
                    'double' => false,
                    'checked' => false
                ],
            ])
            <button name="get_data" class="btn btn-success col-12">Show history</button>
        </form>
        <div id="chart_div" style="width: 100%; height: 500px; margin-bottom: 50px;display: none"></div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{ URL::asset('js/select-container.js') }}"></script>
    <script>
        $('#getData').submit(function (e) {
            e.preventDefault();
            collectItems('dishes_ids');
            let data = $(this).serialize();
            $.ajax({
                url: '{{ route('dashboard.getChartData') }}',
                type: 'GET',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function(response) {
                    console.log(response);
                    if (response.length != 0) {
                        $('#chart_div').show();
                        google.charts.load('current', {packages: ['corechart', 'line']});
                        google.charts.setOnLoadCallback(drawLineColors);
                        function drawLineColors() {
                            let data = new google.visualization.arrayToDataTable(response);
                            let options = {
                                hAxis: {
                                    title: 'Time',
                                },
                                vAxis: {
                                    title: 'Quantity'
                                },
                            };
                            let chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                            chart.draw(data, options);
                        }

                    }
                },
            });
        });
    </script>
@endsection
