<?php

return [
    \App\Services\CacheMagic\DishCache::class => [
        'methods' => [
            'whereNameLikeLimit' => [
                'tags' => ['dishes'],
                'key' => 'dishes_search_autocomplete',
                'time' => 60,
            ],
            'paginate' => [
                'tags' => ['dishes'],
                'key' => 'dishes_all',
                'time' => 60,
            ],
            'whereNameLikePaginate' => [
                'tags' => ['dishes'],
                'key' => 'dishes_search',
                'time' => 60,
            ],
        ],
    ],
    'facade-cache' => [
        'repositories' => [
            \App\Services\CacheFacade\Dish\Paginate::class =>
                \App\Services\Repositories\DishRepository::class,
            \App\Services\CacheFacade\Dish\WhereNameLikeLimit::class =>
                \App\Services\Repositories\DishRepository::class,
            \App\Services\CacheFacade\Dish\WhereNameLikePaginate::class =>
                \App\Services\Repositories\DishRepository::class,
        ]
    ]
];
