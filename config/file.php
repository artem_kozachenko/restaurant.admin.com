<?php

return [
    'path' => [
        \App\Models\Dish::class => 'dishes',
        \App\Models\Ingredient::class => 'ingredients',
    ],
];
