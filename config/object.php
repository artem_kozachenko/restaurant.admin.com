<?php

return [
    \App\Services\DTO\Builders\DishObjectBuilder::class => [
        'object' => \App\Services\DTO\Objects\DishObject::class,
        'validation' => [],
        'methods' => [
            'name' => 'setName',
            'price' => 'setPrice',
            'category_id' => 'setCategoryId',
            'discount_id' => 'setDiscountId',
            'photo' => 'setPhoto',
            'ingredients_ids' => 'setIngredientsIds',
            'description' => 'setDescription',
        ],
    ],
    \App\Services\DTO\Builders\MenuObjectBuilder::class => [
        'object' => \App\Services\DTO\Objects\MenuObject::class,
        'validation' => [],
        'methods' => [
            'date' => 'setDate',
            'dishes' => 'setDishes',
        ],
    ],
    \App\Services\DTO\Builders\ThisWeekMenuDishObjectBuilder::class => [
        'object' => \App\Services\DTO\Objects\ThisWeekMenuDishObject::class,
        'validation' => [],
        'methods' => [
            'id' => 'setId',
            'name' => 'setName',
            'quantity' => 'setQuantity',
            'price' => 'setPrice',
            'photo' => 'setPhoto',
            'discount' => 'setDiscount',
            'category' => 'setCategory',
            'description' => 'setDescription',
            'priceWithDiscount' => 'setPriceWithDiscount'
        ],
    ],
];
