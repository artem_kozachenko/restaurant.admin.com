<?php

return [
    \App\Services\Files\Export\Objects\MenuDish::class => [
        'titles' => [
            'name',
            'price',
            'date'
        ],
    ]
];
