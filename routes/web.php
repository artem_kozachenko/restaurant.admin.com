<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*______________________________________________________________________________________________________________________
|
|                                              USER SIDE
| ______________________________________________________________________________________________________________________
*/
Route::namespace('UserSide')->group(function () {
    Route::get('/', function () {
        return redirect()->route('home');
    });
    Route::get('/dishes', 'DishController@showDishes')->name('dishes');
    Route::get('/dish/{dish}', 'DishController@showDish')->name('dish');
    Route::get('/search/dishes/autocomplete', 'SearchController@findDishesAutocomplete')->name('search.dishes.auto');
});
/*______________________________________________________________________________________________________________________
|
|                                              USER SIDE V2
| ______________________________________________________________________________________________________________________
*/
Route::namespace('UserSideV2')->group(function () {
    Route::prefix('v2')->group(function () {
        Route::get('/', 'MenuController@index')->name('home');
        Route::get('/cart', 'CartController@showCart')->name('cart');
        Route::post('/cart/recalculate', 'CartController@recalculateTotalPrice')->name('cart.recalculate');
        Route::any('/order/inc_item', 'OrderController@increaseOrderCounter')->name('orders.inc');
        Route::any('/order/dec_item', 'OrderController@decreaseOrderCount')->name('orders.dec');
        Route::any('/order/remove_item', 'OrderController@removeOrderItem')->name('orders.remove');
        Route::any('/order/confirm_order', 'OrderController@confirmOrder')->name('orders.confirm');
    });
});
/*______________________________________________________________________________________________________________________
|
|                                              ADMIN SIDE
| ______________________________________________________________________________________________________________________
*/
Route::prefix('admin')->group(function () {
    Route::namespace('AdminSide')->group(function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('/dashboard/get_chart_data', 'DashboardController@getChartData')->name('dashboard.getChartData');
        /*______________________________________________________________________________________________________________
        |
        |                                              AUTH
        | ______________________________________________________________________________________________________________
        */
        Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login.form');
        Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::post('/password/reset', 'Auth\ForgotPasswordController@reset');
        Route::post('/login', 'Auth\LoginController@login')->name('login');
        Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
        /*______________________________________________________________________________________________________________
        |
        |                                              DISH
        | ______________________________________________________________________________________________________________
        */
        Route::middleware(['auth'])->group(function () {
            Route::get('/dishes/export', 'ExportController@showForm')->name('dishes.export.form');
            Route::match(['post', 'get'], '/dishes/history', 'ExportController@showHistory')->name('dishes.history');
            Route::post('/dishes/export', 'ExportController@export')->name('dishes.export');
        });
        /*______________________________________________________________________________________________________________
        |
        |                                              RESOURCES
        | ______________________________________________________________________________________________________________
        */
        Route::middleware(['auth'])->group(function () {
            Route::namespace('Resources')->group(function () {
                Route::resources([
                    'ingredients' => 'IngredientController',
                    'categories' => 'CategoryController',
                    'dishes' => 'DishController',
                    'menus' => 'MenuController',
                    'discounts' => 'DiscountController',
                    'delivery_types' => 'DeliveryTypeController',
                ]);
            });
        });
        /*______________________________________________________________________________________________________________
        |
        |                                              USER
        | ______________________________________________________________________________________________________________
        */
        Route::get('/users/create/{token}', 'UserRegisterController@showPostCreateForm')
            ->middleware('token.exists:user_registers')
            ->name('post.create.form');
        Route::post('/users/create/{token}', 'UserRegisterController@postCreateUser')->name('post.create.user');
        Route::middleware(['auth'])->group(function () {
            Route::middleware(['min.access.level:2'])->group(function () {
                Route::get('/users/create', 'UserRegisterController@showPreCreateForm')
                    ->name('pre.create.form');
                Route::post('/users/create', 'UserRegisterController@preCreateUser')
                    ->name('pre.create.user');
                Route::namespace('Resources')->group(function () {
                    Route::get('/users', 'UserController@index')
                        ->name('users.index');
                    Route::middleware(['compare.access.level'])->group(function () {
                        Route::get('/users/{user}/edit', 'UserController@edit')
                            ->name('users.edit');
                        Route::put('/users/{user} ', 'UserController@update')
                            ->name('users.update');
                        Route::delete('/users/{user}', 'UserController@destroy')
                            ->name('users.destroy');
                    });
                });
            });
        });
    });
});
