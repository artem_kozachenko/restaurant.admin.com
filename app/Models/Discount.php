<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Discount
 * @package App\Models
 */
class Discount extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'percent',
        'from',
        'to',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dishes()
    {
        return $this->hasMany(Dish::class);
    }
}
