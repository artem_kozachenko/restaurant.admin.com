<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRegister
 * @package App\Models
 */
class UserRegister extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Set created_at column
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'role_id',
        'token'
    ];
}
