<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 */
class Category extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'url_name',
        'parent_category_id',
    ];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url_name';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dishes()
    {
        return $this->hasMany(Dish::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parentCategory()
    {
        return $this->hasOne(
            Category::class,
            'id',
            'parent_category_id'
        );
    }
}
