<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MenuDish
 * @package App\Models
 */
class MenuDish extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Set created_at column
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'menu_id',
        'price',
        'dish_id',
        'create_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function dish()
    {
        return $this->hasOne(Dish::class, 'id', 'dish_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
