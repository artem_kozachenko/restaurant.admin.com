<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderDish
 * @package App\Models
 */
class OrderDish extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'order_id',
        'menu_dish_id',
        'quantity',
        'price',
        'total_price',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menuDish()
    {
        return $this->belongsTo(MenuDish::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
