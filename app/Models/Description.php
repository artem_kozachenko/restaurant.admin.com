<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Description
 * @package App\Models
 */
class Description extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'content',
        'descriptionable_id',
        'descriptionable_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function descriptionable()
    {
        return $this->morphTo();
    }
}
