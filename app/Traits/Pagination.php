<?php

namespace App\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Class Pagination
 * @package App\Traits
 */
trait Pagination
{
    /**
     * @param $items
     * @param int $perPage
     * @param array $options
     * @param null $page
     * @return LengthAwarePaginator
     */
    protected function paginate($items, $perPage = 15, $options = [], $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
