<?php

namespace App\Traits;

use App\Services\DTO\Objects\ResponseMessageObject;

/**
 * Trait HasResponseMessage
 * @package App\Traits
 */
trait HasResponseMessage
{
    /**
     * @param string $status
     * @param string $message
     * @return ResponseMessageObject
     */
    public function makeResponse(string $status, string $message)
    {
        $responseMessage = $this->getResponse();
        $responseMessage->setStatus($status);
        $responseMessage->setMessage($message);

        return $responseMessage;
    }

    /**
     * @return ResponseMessageObject
     */
    protected function getResponse()
    {
        return new ResponseMessageObject();
    }
}
