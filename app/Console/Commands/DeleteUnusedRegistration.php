<?php

namespace App\Console\Commands;

use App\Services\Generators\TimeGenerator;
use App\Services\Managers\UserRegisterManager;
use Illuminate\Console\Command;

/**
 * Class DeleteUnusedRegistration
 * @package App\Console\Commands
 */
class DeleteUnusedRegistration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:deleteUserRegisters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete unused registration requests.';

    /**
     * @var TimeGenerator
     */
    protected $timeGenerator;

    /**
     * Create a new command instance.
     *
     * @param TimeGenerator $timeGenerator
     * @return void
     */
    public function __construct(TimeGenerator $timeGenerator)
    {
        parent::__construct();

        $this->timeGenerator = $timeGenerator;
    }

    /**
     * Execute the console command.
     *
     * @param UserRegisterManager $userRegisterManager
     * @return mixed
     */
    public function handle(UserRegisterManager $userRegisterManager)
    {
        $time = $this->timeGenerator->yesterday();
        $userRegisterManager->deleteByTime($time);
    }
}
