<?php

namespace App\Jobs;

use App\Services\Managers\UserRegisterManager;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class DeleteAfter24Hour
 * @package App\Jobs
 */
class DeleteAfter24Hour implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $token;

    /**
     * Create a new job instance.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @param UserRegisterManager $userRegisterManager
     * @return void
     */
    public function handle(UserRegisterManager $userRegisterManager)
    {
        $userRegisterManager->deleteByToken($this->token);
    }
}
