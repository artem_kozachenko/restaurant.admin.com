<?php

namespace App\Exceptions;

use Exception;

/**
 * Class RepositoryFactoryException
 * @package App\Exceptions
 */
class RepositoryFactoryException extends Exception
{
    //
}
