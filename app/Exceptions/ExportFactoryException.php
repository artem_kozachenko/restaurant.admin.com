<?php

namespace App\Exceptions;

use Exception;

/**
 * Class ExportFactoryException
 * @package App\Exceptions
 */
class ExportFactoryException extends Exception
{

}
