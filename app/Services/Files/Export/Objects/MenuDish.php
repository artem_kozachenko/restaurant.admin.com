<?php

namespace App\Services\Files\Export\Objects;

/**
 * Class Dish
 * @package App\Services\Files\Export\Objects
 */
class MenuDish extends BaseObject
{
    /**
     * @param array $params
     * @return mixed
     */
    protected function getData(array $params)
    {
        $data = $this->repository
            ->getDishHistory(
                $params['dish_id'],
                $params['date_from'],
                $params['date_to']
            );

        return $data;
    }
}
