<?php

namespace App\Services\Files\Export\Objects;

use App\Services\Factories\ExportFactory;
use App\Services\Factories\RepositoryFactory;
use App\Services\Repositories\MenuDishRepository;

/**
 * Class BaseObject
 * @package App\Services\Files\Export\Objects
 */
abstract class BaseObject
{
    /**
     * @var
     */
    protected $exporter;

    /**
     * @var array|mixed
     */
    protected $titles = [];

    /**
     * @var
     */
    protected $repository;

    /**
     * @var ExportFactory
     */
    protected $exportFactory;

    /**
     * @var RepositoryFactory
     */
    protected $repositoryFactory;

    /**
     * BaseObject constructor.
     * @param ExportFactory $exportFactory
     * @param RepositoryFactory $repositoryFactory
     * @param MenuDishRepository $menuDishRepository
     */
    public function __construct(
        ExportFactory $exportFactory,
        RepositoryFactory $repositoryFactory,
        MenuDishRepository $menuDishRepository
    ) {
        $config = $this->getConfig();
        $this->titles = $config['titles'];
        $this->exportFactory = $exportFactory;
        $this->repositoryFactory = $repositoryFactory;
    }

    /**
     * @param array $params
     * @return mixed
     */
    abstract protected function getData(array $params);

    /**
     * @param array $params
     * @param string $format
     * @throws \Exception
     * @return mixed
     */
    public function export(array $params, string $format)
    {
        $this->setExporter($format);
        $this->setRepository();
        $dishHistory = $this->getData($params)->toArray();
        $filePath = $this->writeFile($dishHistory);

        return $filePath;
    }

    /**
     * @param string $format
     * @throws \Exception
     */
    protected function setExporter(string $format)
    {
        $this->exporter = $this->exportFactory->get($format);
    }

    /**
     * @throws \App\Exceptions\RepositoryFactoryException
     */
    protected function setRepository()
    {
        $this->repository = $this->repositoryFactory->get(static::class);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function writeFile(array $data)
    {
        $filePath = $this->exporter->writeFile($data, $this->titles);

        return $filePath;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getConfig()
    {
        return config('export.' . static::class);
    }
}
