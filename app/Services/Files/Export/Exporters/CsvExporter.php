<?php

namespace App\Services\Files\Export\Exporters;

use App\Services\Files\Export\Interfaces\ExporterInterface;

/**
 * Class CsvExporter
 * @package App\Services\Files\Export\Exporters
 */
class CsvExporter extends BaseExporter implements ExporterInterface
{
    /**
     * @var resource
     */
    protected $file;

    /**
     * @param array $data
     * @param array $titles
     * @return mixed|string
     */
    public function writeFile(array $data, array $titles = [])
    {
        $fileName = $this->generateName();
        $filePath = $this->getFilePath($fileName, 'csv');
        $this->processFile($filePath, $data, $titles);

        return $filePath;
    }

    /**
     * @param string $filePath
     * @param array $data
     * @param array $titles
     */
    protected function processFile(string $filePath, array $data, array $titles = [])
    {
        $this->openFile($filePath);
        $this->setTitles($titles);
        $this->setContent($data);
        $this->closeFile();
    }

    /**
     * @param string $filePath
     */
    protected function openFile(string $filePath)
    {
        $this->file = fopen($filePath, 'w');
    }

    /**
     * @param array $titles
     */
    protected function setTitles(array $titles = [])
    {
        fputcsv($this->file, $titles);
    }

    /**
     * @param array $data
     */
    protected function setContent(array $data)
    {
        foreach ($data as $row) {
            fputcsv($this->file, $row);
        }
    }

    /**
     *
     */
    protected function closeFile()
    {
        fclose($this->file);
    }
}
