<?php

namespace App\Services\Files\Export\Exporters;

/**
 * Class BaseExporter
 * @package App\Services\Files\Export\Exporters
 */
abstract class BaseExporter
{
    /**
     * @return string
     */
    protected function generateName()
    {
        return str_random(10);
    }

    /**
     * @param string $fileName
     * @param string $format
     * @return string
     */
    protected function getFilePath(string $fileName, string $format)
    {
        return storage_path('app/public/temp/') . $fileName . '.' . $format;
    }
}
