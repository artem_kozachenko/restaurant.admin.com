<?php

namespace App\Services\Files\Export\Interfaces;

/**
 * Interface ExporterInterface
 * @package App\Services\Files\Export\Interfaces
 */
interface ExporterInterface
{
    /**
     * @param array $data
     * @param array $titles
     * @return mixed
     */
    public function writeFile(array $data, array $titles = []);
}
