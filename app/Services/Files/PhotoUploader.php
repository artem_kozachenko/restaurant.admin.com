<?php

namespace App\Services\Files;

use App\Services\Managers\PhotoManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileUploader
 * @package App\Services\File
 */
class PhotoUploader
{
    /**
     * @var array|\Illuminate\Config\Repository|mixed
     */
    protected $paths = [];

    /**
     * @var PhotoManager
     */
    protected $photoManager;

    /**
     * PhotoUploader constructor.
     * @param PhotoManager $photoManager
     */
    public function __construct(PhotoManager $photoManager)
    {
        $this->photoManager = $photoManager;
        $this->paths = $this->getAvailablePaths();
    }

    /**
     * @param Model $model
     * @param UploadedFile $file
     */
    public function manageFile(Model $model, UploadedFile $file)
    {
        $data = $this->uploadFile($model, $file);
        $data = array_merge($data, $this->prepareProperties($model));
        $this->photoManager->create($data);
    }

    /**
     * @param Model $model
     * @param UploadedFile $file
     * @return mixed
     */
    protected function uploadFile(Model $model, UploadedFile $file)
    {
        $data['name'] = rand() . '.' . $file->extension();
        $data['path'] = Storage::putFileAs($this->paths[get_class($model)], $file, $data['name']);

        return $data;
    }

    /**
     * @param Model $model
     * @return mixed
     */
    protected function prepareProperties(Model $model)
    {
        $data['photoable_id'] = $model->id;
        $data['photoable_type'] = get_class($model);

        return $data;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getAvailablePaths()
    {
        return config('file.path');
    }
}
