<?php

namespace App\Services\Repositories;

use App\Models\OrderDish;

/**
 * Class OrderDishRepository
 * @package App\Services\Repositories
 */
class OrderDishRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return OrderDish::class;
    }

    /**
     * @param string $dishId
     * @param string $orderId
     * @return mixed
     */
    public function firstByDishAndOrderId(string $dishId, string $orderId)
    {
        return OrderDish::where([
            ['order_id', $orderId],
            ['menu_dish_id', $dishId],
        ])->first();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function allByDateAndMenuDishesIds(string $from, string  $to, array $ids)
    {
        return OrderDish::whereBetween('updated_at', [$from, $to])->whereIn('menu_dish_id', $ids)->get();
    }
}
