<?php

namespace App\Services\Repositories;

use App\Models\Photo;

/**
 * Class PhotoRepository
 * @package App\Services\Repositories
 */
class PhotoRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Photo::class;
    }

    /**
     * @param int $photobleId
     * @param string $photoableType
     * @return mixed
     */
    public function oldPhoto(int $photobleId, string $photoableType)
    {
        return Photo::where([
            ['photoable_id',  $photobleId],
            ['photoable_type', $photoableType]
        ])->first();
    }
}
