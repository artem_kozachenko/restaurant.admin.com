<?php

namespace App\Services\Repositories;

use App\Models\MenuDish;

/**
 * Class MenuDishRepository
 * @package App\Services\Repositories
 */
class MenuDishRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return MenuDish::class;
    }

    /**
     * @param int $dishId
     * @param string $dateFrom
     * @param string $dateTo
     * @return mixed
     */
    public function getDishHistory(int $dishId, string $dateFrom, string $dateTo)
    {
        return MenuDish::select('d.name as name', 'menu_dishes.price as price', 'menu_dishes.created_at as date')
            ->join('dishes as d', 'd.id', '=', 'menu_dishes.dish_id')
            ->where('menu_dishes.dish_id', $dishId)
            ->whereBetween('menu_dishes.created_at', [$dateFrom, $dateTo])
            ->orderBy('menu_dishes.created_at', 'desc')
            ->get();
    }

    /**
     * @param string $dishId
     * @param string $menuId
     * @return mixed
     */
    public function findByDishAndMenuId(string $dishId, string $menuId)
    {
        return MenuDish::where([
            ['menu_id', $menuId],
            ['dish_id', $dishId],
        ])
            ->first();
    }

    /**
     * @param array $dishesIds
     * @return mixed
     */
    public function allByDishesIds(array $dishesIds)
    {
        return MenuDish::whereIn('dish_id', $dishesIds)->get();
    }
}
