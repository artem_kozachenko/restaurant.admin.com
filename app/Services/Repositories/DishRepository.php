<?php

namespace App\Services\Repositories;

use App\Models\Dish;

/**
 * Class DishRepository
 * @package App\Services\Repositories
 */
class DishRepository extends BaseRepository
{
    protected function getClass()
    {
        return Dish::class;
    }

    /**
     * @param string $name
     * @param int $quantity
     * @return mixed
     */
    public function whereNameLikePaginate(string $name, int $quantity)
    {
        return Dish::select('id', 'price', 'name', 'category_id')
            ->where('name', 'like', '%' . $name . '%')
            ->paginate($quantity);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function whereNameLikeLimit(string $name)
    {
        return Dish::select('id', 'price', 'name', 'category_id')
            ->where('name', 'like', '%' . $name . '%')
            ->limit(15)
            ->get();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function whereNotIn(array $ids)
    {
        return Dish::whereNotIn('id', $ids)
            ->get();
    }
}
