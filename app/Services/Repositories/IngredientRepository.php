<?php

namespace App\Services\Repositories;

use App\Models\Ingredient;

/**
 * Class IngredientRepository
 * @package App\Services\Repositories
 */
class IngredientRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Ingredient::class;
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function whereNotIn(array $ids)
    {
        return Ingredient::whereNotIn('id', $ids)
            ->get();
    }
}
