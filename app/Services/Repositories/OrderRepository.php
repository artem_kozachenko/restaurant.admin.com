<?php

namespace App\Services\Repositories;

use App\Models\Order;

/**
 * Class OrderRepository
 * @package App\Services\Repositories
 */
class OrderRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Order::class;
    }

    /**
     * @param $sessionId
     * @return mixed
     */
    public function checkExistingUncompletedBySessionId(string $sessionId)
    {
        return Order::where([
            ['session_id', $sessionId],
            ['completed', 0]
        ])->exists();
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    public function firstUncompletedBySessionId(string $sessionId)
    {
        return Order::where([
            ['session_id', $sessionId],
            ['completed', 0]
        ])->first();
    }
}
