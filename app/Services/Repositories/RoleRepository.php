<?php

namespace App\Services\Repositories;

use App\Models\Role;

/**
 * Class RoleRepository
 * @package App\Services\Repositories
 */
class RoleRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Role::class;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function allExceptCurrent(string $id)
    {
        return Role::where('id', '!=', $id)->get();
    }

    /**
     * @return mixed
     */
    public function notAll()
    {
        return Role::whereNotIn('name', ['Admin', 'Super admin'])->get();
    }
}
