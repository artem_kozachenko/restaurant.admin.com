<?php

namespace App\Services\Repositories;

use App\Models\DeliveryType;

/**
 * Class DeliveryTypeRepository
 * @package App\Services\Repositories
 */
class DeliveryTypeRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return DeliveryType::class;
    }
}
