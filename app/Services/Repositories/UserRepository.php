<?php

namespace App\Services\Repositories;

use App\Models\Role;
use App\Models\User;

/**
 * Class UserRepository
 * @package App\Services\Repositories
 */
class UserRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return User::class;
    }

    /**
     * @param string $quantity
     * @return mixed
     */
    public function adminsAndEditorsPaginate(string $quantity)
    {
        return User::select('users.*')
            ->join('roles as r', 'r.id', '=', 'users.role_id')
            ->whereIn('r.name', [Role::ADMIN, Role::EDITOR])
            ->paginate($quantity);
    }

    /**
     * @param string $quantity
     * @return mixed
     */
    public function editorsPaginate(string $quantity)
    {
        return User::select('users.*')
            ->join('roles as r', 'r.id', '=', 'users.role_id')
            ->where('r.name', Role::EDITOR)
            ->paginate($quantity);
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function findByEmail(string $email)
    {
        return User::where('email', $email)->first();
    }
}
