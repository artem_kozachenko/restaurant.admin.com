<?php

namespace App\Services\Repositories;

use App\Models\Description;

/**
 * Class DescriptionRepository
 * @package App\Services\Repositories
 */
class DescriptionRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Description::class;
    }

    /**
     * @param int $descriptionableId
     * @param string $descriptionableType
     * @return mixed
     */
    public function oldDescription(int $descriptionableId, string $descriptionableType)
    {
        return Description::where([
            ['descriptionable_id',  $descriptionableId],
            ['descriptionable_type', $descriptionableType]
        ])->first();
    }
}
