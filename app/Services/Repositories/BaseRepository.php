<?php

namespace App\Services\Repositories;

/**
 * Class BaseRepository
 * @package App\Services\Repositories
 */
abstract class BaseRepository
{
    /**
     * @var
     */
    protected $entity;

    /**
     * @return mixed
     */
    abstract protected function getClass();

    /**
     * BaseRepository constructor.
     */
    public function __construct()
    {
        $this->entity = $this->getClass();
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->entity::all();
    }

    /**
     * @param string/array $id
     * @return mixed
     */
    public function findById($id)
    {
        return $this->entity::find($id);
    }

    /**
     * @param $quantity
     * @return mixed
     */
    public function paginate($quantity)
    {
        return $this->entity::paginate($quantity);
    }
}
