<?php

namespace App\Services\Repositories;

use App\Models\UserRegister;

/**
 * Class UserRegisterRepository
 * @package App\Services\Repositories
 */
class UserRegisterRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return UserRegister::class;
    }

    /**
     * @param string $token
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function firstByToken(string $token)
    {
        return UserRegister::where('token', $token)->first();
    }
}
