<?php

namespace App\Services\Repositories;

use App\Models\Category;

/**
 * Class CategoryRepository
 * @package App\Services\Repositories
 */
class CategoryRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Category::class;
    }

    /**
     * @param string $current
     * @param $parent
     * @return mixed
     */
    public function allExceptCurrentAndParent(string $current, int $parent = null)
    {
        return Category::where('id', '!=', $current)
            ->where('id', '!=', $parent)
            ->orderBy('name')
            ->get();
    }

    /**
     * @param string $current
     * @return mixed
     */
    public function allExceptCurrent(string $current)
    {
        return Category::where('id', '!=', $current)
            ->orderBy('name')
            ->get();
    }
}
