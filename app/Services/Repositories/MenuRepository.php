<?php

namespace App\Services\Repositories;

use App\Models\Menu;
use App\Services\Generators\TimeGenerator;
use Carbon\Carbon;

/**
 * Class MenuRepository
 * @package App\Services\Repositories
 */
class MenuRepository extends BaseRepository
{
    /**
     * @var TimeGenerator
     */
    protected $timeGenerator;

    /**
     * MenuRepository constructor.
     * @param TimeGenerator $timeGenerator
     */
    public function __construct(TimeGenerator $timeGenerator)
    {
        parent::__construct();

        $this->timeGenerator = $timeGenerator;
    }

    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Menu::class;
    }

    /**
     * @return Menu|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function currentWeekMenu()
    {
        $currentWeek = $this->currentWeek();

        return Menu::with(
            'dishes',
            'dishes.dish',
            'dishes.dish.photo',
            'dishes.dish.description',
            'dishes.dish.discount',
            'dishes.dish.category'
        )
            ->where('date', $currentWeek)
            ->first();
    }

    /**
     * @return mixed
     */
    public function currentWeekMenuId()
    {
        $currentWeek = $this->currentWeek();

        return Menu::select('id')
            ->where('date', $currentWeek)
            ->first();
    }

    /**
     * @return Menu[]|\Illuminate\Database\Eloquent\Collection
     */
    public function lastBeforeNow()
    {
        $currentWeek = $this->currentWeek();

        return Menu::where('date', '<', $currentWeek)->orderBy('date', 'desc')->first();
    }

    /**
     * @return Carbon|string
     */
    protected function currentWeek()
    {
        return $this->timeGenerator->currentWeek();
    }
}
