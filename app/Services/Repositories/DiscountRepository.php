<?php

namespace App\Services\Repositories;

use App\Models\Discount;

/**
 * Class DiscountRepository
 * @package App\Services\Repositories
 */
class DiscountRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Discount::class;
    }

    /**
     * @param string $current
     * @return mixed
     */
    public function allExceptCurrent(string $current)
    {
        return Discount::where('id', '!=', $current)
            ->orderBy('name')
            ->get();
    }
}
