<?php

namespace App\Services\Searchers;

use App\Http\Resources\DishAutocompleteCollection;
use App\Services\CacheFacade\Dish\WhereNameLikeLimit;

/**
 * Class DishSearcher
 * @package App\Services\Searchers
 */
class DishSearcher
{
    /**
     * @var WhereNameLikeLimit
     */
    protected $whereNameLikeLimit;

    /**
     * DishSearcher constructor.
     * @param WhereNameLikeLimit $whereNameLikeLimit
     */
    public function __construct(WhereNameLikeLimit $whereNameLikeLimit)
    {
        $this->whereNameLikeLimit = $whereNameLikeLimit;
    }

    /**
     * @param string $search
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Exception
     */
    public function search(string $search)
    {
        $dishes = $this->getDishes($search);

        return DishAutocompleteCollection::collection($dishes);
    }

    /**
     * @param string $search
     * @return mixed
     * @throws \Exception
     */
    protected function getDishes(string $search)
    {
        return $this->whereNameLikeLimit->getCache(
            'dishes_search_autocomplete' . $search,
            60,
            ['dishes'],
            ['search' => $search]
        );
    }
}
