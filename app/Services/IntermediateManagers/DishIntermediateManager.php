<?php

namespace App\Services\IntermediateManagers;

use App\Models\Dish;
use App\Services\DTO\Builders\DishObjectBuilder;
use App\Services\DTO\Objects\DishObject;
use App\Services\Files\PhotoUploader;
use App\Services\Managers\DescriptionManager;
use App\Services\Managers\DishManager;
use App\Traits\HasResponseMessage;
use Illuminate\Http\UploadedFile;

/**
 * Class DishIntermediateManager
 * @package App\Services\IntermediateManagers
 */
class DishIntermediateManager
{
    use HasResponseMessage;

    /**
     * @var DishManager
     */
    protected $dishManager;

    /**
     * @var PhotoUploader
     */
    protected $photoUploader;

    /**
     * @var DishObjectBuilder
     */
    protected $dishObjectBuilder;

    /**
     * @var DescriptionManager
     */
    protected $descriptionManager;

    /**
     * DishIntermediateManager constructor.
     * @param DishManager $dishManager
     * @param PhotoUploader $photoUploader
     * @param DishObjectBuilder $dishObjectBuilder
     * @param DescriptionManager $descriptionManager
     */
    public function __construct(
        DishManager $dishManager,
        PhotoUploader $photoUploader,
        DishObjectBuilder $dishObjectBuilder,
        DescriptionManager $descriptionManager
    ) {
        $this->dishManager = $dishManager;
        $this->photoUploader = $photoUploader;
        $this->dishObjectBuilder = $dishObjectBuilder;
        $this->descriptionManager = $descriptionManager;
    }

    /**
     * @param array $params
     * @return \App\Services\DTO\Objects\ResponseMessageObject
     */
    public function create(array $params)
    {
        $dishObject = $this->buildObject($params);
        if ($dishObject) {
            $this->createStrategy($dishObject);

            return $this->makeResponse('success', 'New dish has been successfully created.');
        } else {
            return $this->makeResponse('error', 'Something went wrong while dish creating.');
        }
    }

    /**
     * @param Dish $dish
     * @param array $params
     * @return \App\Services\DTO\Objects\ResponseMessageObject
     */
    public function update(Dish $dish, array $params)
    {
        $dishObject = $this->buildObject($params);
        if ($dishObject) {
            $this->updateStrategy($dish, $dishObject);

            return $this->makeResponse('success', 'Dish has been successfully updated.');
        } else {
            return $this->makeResponse('error', 'Something went wrong while dish updating.');
        }
    }

    /**
     * @param array $params
     * @return mixed|null
     */
    protected function buildObject(array $params)
    {
        return $this->dishObjectBuilder->build($params);
    }

    /**
     * @param DishObject $dishObject
     */
    protected function createStrategy(DishObject $dishObject)
    {
        $dish = $this->dishManager->create($dishObject->toArray());
        $this->manageRelations($dish, $dishObject);
    }

    /**
     * @param Dish $dish
     * @param DishObject $dishObject
     */
    protected function updateStrategy(Dish $dish, DishObject $dishObject)
    {
        $this->dishManager->update($dish, $dishObject->toArray());
        $this->manageRelations($dish, $dishObject);
    }

    /**
     * @param Dish $dish
     * @param DishObject $dishObject
     */
    protected function manageRelations(Dish $dish, DishObject $dishObject)
    {
        $this->managePhoto($dish, $dishObject->getPhoto());
        $this->manageDescription($dish, $dishObject->getDescription());
        $this->manageIngredients($dish, $dishObject->getIngredientsIds());
    }

    /**
     * @param Dish $dish
     * @param UploadedFile $photo
     */
    protected function managePhoto(Dish $dish, UploadedFile $photo = null)
    {
        if ($photo) {
            $this->photoUploader->manageFile($dish, $photo);
        }
    }

    /**
     * @param Dish $dish
     * @param string $description
     */
    protected function manageDescription(Dish $dish, string $description)
    {
        if ($description) {
            $descriptionData['content'] = $description;
            $descriptionData['descriptionable_id'] = $dish->id;
            $descriptionData['descriptionable_type'] = get_class($dish);
            $this->descriptionManager->create($descriptionData);
        }
    }

    /**
     * @param Dish $dish
     * @param array $possibleIngredientsIds
     */
    protected function manageIngredients(Dish $dish, array $possibleIngredientsIds)
    {
        $currentIngredientsIds = $dish->ingredients->pluck('id')->toArray();
        $ingredientsToAdd = array_diff($possibleIngredientsIds, $currentIngredientsIds);
        $ingredientsToRemove = array_diff($currentIngredientsIds, $possibleIngredientsIds);
        if ($ingredientsToAdd) {
            $this->dishManager->attachIngredients($dish, $ingredientsToAdd);
        }
        if ($ingredientsToRemove) {
            $this->dishManager->detachIngredients($dish, $ingredientsToRemove);
        }
    }
}
