<?php

namespace App\Services\IntermediateManagers;

use App\Models\Ingredient;
use App\Services\Files\PhotoUploader;
use App\Services\Managers\IngredientManager;
use Illuminate\Http\UploadedFile;

/**
 * Class IngredientIntermediateManager
 * @package App\Services\IntermediateManagers
 */
class IngredientIntermediateManager
{
    /**
     * @var PhotoUploader
     */
    protected $photoUploader;

    /**
     * @var IngredientManager
     */
    protected $ingredientManager;

    /**
     * IngredientIntermediateManager constructor.
     * @param PhotoUploader $photoUploader
     * @param IngredientManager $ingredientManager
     */
    public function __construct(
        PhotoUploader $photoUploader,
        IngredientManager $ingredientManager
    ) {
        $this->photoUploader = $photoUploader;
        $this->ingredientManager = $ingredientManager;
    }

    /**
     * @param array $params
     */
    public function create(array $params)
    {
        $ingredient = $this->ingredientManager->create($params);
        $this->managePhoto($ingredient, $params['photo'] ?? null);
    }

    /**
     * @param Ingredient $ingredient
     * @param array $params
     */
    public function update(Ingredient $ingredient, array $params)
    {
        $this->ingredientManager->update($ingredient, $params);
        $this->managePhoto($ingredient, $params['photo'] ?? null);
    }

    /**
     * @param Ingredient $ingredient
     * @param UploadedFile $photo
     */
    protected function managePhoto(Ingredient $ingredient, UploadedFile $photo = null)
    {
        if (isset($photo)) {
            $this->photoUploader->manageFile($ingredient, $photo);
        }
    }
}
