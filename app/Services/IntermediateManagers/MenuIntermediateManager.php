<?php

namespace App\Services\IntermediateManagers;

use App\Models\Menu;
use App\Services\DTO\Builders\MenuObjectBuilder;
use App\Services\DTO\Objects\MenuObject;
use App\Services\Generators\TimeGenerator;
use App\Services\Managers\MenuDishManager;
use App\Services\Managers\MenuManager;
use App\Services\Repositories\DishRepository;
use App\Traits\HasResponseMessage;
use Illuminate\Support\Collection;

/**
 * Class MenuIntermediateManager
 * @package App\Services\IntermediateManagers
 */
class MenuIntermediateManager
{
    use HasResponseMessage;

    /**
     * @var MenuManager
     */
    protected $menuManager;

    /**
     * @var TimeGenerator
     */
    protected $timeGenerator;

    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * @var MenuDishManager
     */
    protected $menuDishManager;

    /**
     * @var MenuObjectBuilder
     */
    protected $menuObjectBuilder;

    /**
     * MenuIntermediateManager constructor.
     * @param MenuManager $menuManager
     * @param TimeGenerator $timeGenerator
     * @param DishRepository $dishRepository
     * @param MenuDishManager $menuDishManager
     * @param MenuObjectBuilder $menuObjectBuilder
     */
    public function __construct(
        MenuManager $menuManager,
        TimeGenerator $timeGenerator,
        DishRepository $dishRepository,
        MenuDishManager $menuDishManager,
        MenuObjectBuilder $menuObjectBuilder
    ) {
        $this->menuManager = $menuManager;
        $this->timeGenerator = $timeGenerator;
        $this->dishRepository = $dishRepository;
        $this->menuDishManager = $menuDishManager;
        $this->menuObjectBuilder = $menuObjectBuilder;
    }

    /**
     * @param array $params
     * @return \App\Services\DTO\Objects\ResponseMessageObject
     */
    public function create(array $params)
    {
        $menuObject = $this->menuObjectBuilder->build($params);
        if ($menuObject) {
            $this->createStrategy($menuObject);

            return $this->makeResponse('success', 'New menu has been successfully created.');
        } else {
            return $this->makeResponse('error', 'Something went wrong while menu creating.');
        }
    }

    /**
     * @param Menu $menu
     * @param array $params
     * @return \App\Services\DTO\Objects\ResponseMessageObject
     */
    public function update(Menu $menu, array $params)
    {
        $dishes = $this->dishesData($menu->dishes->pluck('id')->toArray(), $params);
        $this->updateStrategy($dishes, $menu->id);

        return $this->makeResponse('success', 'Menu has been successfully updated.');
    }

    /**
     * @param MenuObject $menuObject
     */
    protected function createStrategy(MenuObject $menuObject)
    {
        $menu = $this->menuManager->create($menuObject->toArray());
        $this->manageMenuDishCreation($menuObject->getDishes(), $menu->id);
    }

    /**
     * @param array $dishes
     * @param string $menuId
     */
    protected function updateStrategy(array $dishes, string $menuId)
    {
        if (! empty($dishes['create'])) {
            $this->manageMenuDishCreation($dishes['create'], $menuId, $dishes['createPrices']);
        }
        if (! empty($dishes['update'])) {
            $this->menuDishManager->updateByIds($dishes['update'], $dishes['updatePrices']);
        }
        if (! empty($dishes['delete'])) {
            $this->menuDishManager->deleteByIds($dishes['delete']);
        }
    }

    /**
     * @param string|null $dishesIds
     * @return array
     */
    protected function dishesIds(string $dishesIds = null)
    {
        $data = [];
        if ($dishesIds) {
            $data = explode(',', $dishesIds);
        }

        return $data;
    }

    /**
     * @param array $currentDishesIds
     * @param array $params
     * @return mixed
     */
    protected function dishesData(array $currentDishesIds, array $params)
    {
        $possibleDishesIds = $this->dishesIds($params['dishes']);
        $prices = [];
        foreach ($possibleDishesIds as $id) {
            $prices[$id] = $params[sprintf('price_%s', $id)];
        }
        $dishes['create'] = array_diff($possibleDishesIds, $currentDishesIds);
        $dishes['update'] = array_intersect($possibleDishesIds, $currentDishesIds);
        $dishes['delete'] = array_diff($currentDishesIds, $possibleDishesIds);
        $dishes['createPrices'] = array_intersect_key($prices, array_flip($dishes['create']));
        $dishes['updatePrices'] = array_intersect_key($prices, array_flip($dishes['update']));

        return $dishes;
    }

    /**
     * @param array $requiredDishesIds
     * @param string $menuId
     * @param array|null $prices
     */
    protected function manageMenuDishCreation(array $requiredDishesIds, string $menuId, array $prices = null)
    {
        $dishes = $this->getDishes($requiredDishesIds);
        $preparedDishes = $this->prepareDishes($dishes, $menuId, $prices);
        $this->menuDishManager->create($preparedDishes);
    }

    /**
     * @param array $dishesIds
     * @return mixed
     */
    protected function getDishes(array $dishesIds)
    {
        return $this->dishRepository->findById($dishesIds);
    }

    /**
     * @param Collection $dishes
     * @param string $menuId
     * @param array $prices
     * @return array
     */
    protected function prepareDishes(Collection $dishes, string $menuId, array $prices = null)
    {
        $now = $this->timeGenerator->now('Europe/Kiev', 'Y-m-d H:i:s');
        $preparedDishes = [];
        foreach ($dishes as $dish) {
            $currentDish['menu_id'] = $menuId;
            $currentDish['price'] = $prices ? $prices[$dish->id] : $dish->price;
            $currentDish['dish_id'] = $dish->id;
            $currentDish['created_at'] = $now;
            array_push($preparedDishes, $currentDish);
        }

        return $preparedDishes;
    }
}
