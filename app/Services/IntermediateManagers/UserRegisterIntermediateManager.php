<?php

namespace App\Services\IntermediateManagers;

use App\Services\Generators\StringGenerator;
use App\Services\Generators\TimeGenerator;
use App\Services\Managers\UserManager;
use App\Services\Managers\UserRegisterManager;
use App\Services\Repositories\UserRegisterRepository;

/**
 * Class UserRegisterIntermediateManager
 * @package App\Services\IntermediateManagers
 */
class UserRegisterIntermediateManager
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var TimeGenerator
     */
    protected $timeGenerator;

    /**
     * @var StringGenerator
     */
    protected $stringGenerator;

    /**
     * @var UserRegisterManager
     */
    protected $userRegisterManager;

    /**
     * @var UserRegisterRepository
     */
    protected $userRegisterRepository;

    /**
     * UserRegisterIntermediateManager constructor.
     * @param UserManager $userManager
     * @param TimeGenerator $timeGenerator
     * @param StringGenerator $stringGenerator
     * @param UserRegisterManager $userRegisterManager
     * @param UserRegisterRepository $userRegisterRepository
     */
    public function __construct(
        UserManager $userManager,
        TimeGenerator $timeGenerator,
        StringGenerator $stringGenerator,
        UserRegisterManager $userRegisterManager,
        UserRegisterRepository $userRegisterRepository
    ) {
        $this->userManager = $userManager;
        $this->timeGenerator = $timeGenerator;
        $this->stringGenerator = $stringGenerator;
        $this->userRegisterManager = $userRegisterManager;
        $this->userRegisterRepository = $userRegisterRepository;
    }

    /**
     * @param array $params
     */
    public function preCreate(array $params)
    {
        $params['token'] = $this->stringGenerator->random(60);
        $params['created_at'] = $this->timeGenerator->now();
        $this->userRegisterManager->create($params);
    }

    /**
     * @param array $params
     * @param string $token
     */
    public function postCreate(array $params, string $token)
    {
        $userRegistered = $this->userRegisterRepository->firstByToken($token);
        $data = $this->prepareUserData($params, $userRegistered);
        $this->userManager->create($data);
        $this->userRegisterManager->deleteByToken($token);
    }

    /**
     * @param array $params
     * @param $userRegistered
     * @return mixed
     */
    protected function prepareUserData(array $params, $userRegistered)
    {
        $data['name'] = $params['name'];
        $data['password'] = $this->stringGenerator->bcrypt($params['password']);
        $data['email'] = $userRegistered->email;
        $data['role_id'] = $userRegistered->role_id;

        return $data;
    }
}
