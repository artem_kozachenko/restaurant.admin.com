<?php

namespace App\Services\Factories;

use App\Exceptions\RepositoryFactoryException;
use App\Services\Repositories\BaseRepository;

/**
 * Class RepositoryFactory
 * @package App\Services\Factories
 */
class RepositoryFactory
{
    /**
     * @var array
     */
    protected $repositories;

    /**
     * @param string $class
     * @return BaseRepository
     * @throws RepositoryFactoryException
     */
    public function get(string $class): BaseRepository
    {
        try {
            $this->getRepositories();

            return $this->build($class);
        } catch (\Throwable $e) {
            throw new RepositoryFactoryException($e->getMessage());
        }
    }

    /**
     * @param $class
     * @return BaseRepository
     */
    protected function build($class): BaseRepository
    {
        return new $this->repositories[$class]();
    }

    /**
     *
     */
    protected function getRepositories()
    {
        $this->repositories = config('repositories');
    }
}
