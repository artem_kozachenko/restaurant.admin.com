<?php

namespace App\Services\Factories;

use App\Exceptions\ExportFactoryException;
use App\Services\Files\Export\Exporters\CsvExporter;
use App\Services\Files\Export\Interfaces\ExporterInterface;

/**
 * Class ExportFactory
 * @package App\Services\Factories
 */
class ExportFactory
{
    /**
     * @var array
     */
    protected $instance = [
        'csv' => CsvExporter::class,
    ];

    /**
     * @param string $type
     * @return ExporterInterface
     * @throws \Exception
     */
    public function get(string $type): ExporterInterface
    {
        try {
            return $this->build($type);
        } catch (\Throwable $e) {
            throw new ExportFactoryException($e->getMessage());
        }
    }

    /**
     * @param string $type
     * @return ExporterInterface
     */
    protected function build(string $type): ExporterInterface
    {
        return new $this->instance[$type]();
    }
}
