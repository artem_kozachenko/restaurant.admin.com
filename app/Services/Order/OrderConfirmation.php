<?php

namespace App\Services\Order;

use App\Models\Order;
use App\Services\Managers\OrderManager;
use App\Services\Repositories\OrderRepository;
use App\Services\Repositories\DeliveryTypeRepository;

/**
 * Class OrderConfirmation
 * @package App\Services\Order
 */
class OrderConfirmation
{
    /**
     * @var OrderManager
     */
    protected $orderManager;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var DeliveryTypeRepository
     */
    protected $deliveryTypeRepository;

    /**
     * OrderConfirmation constructor.
     * @param OrderManager $orderManager
     * @param OrderRepository $orderRepository
     * @param DeliveryTypeRepository $deliveryTypeRepository
     */
    public function __construct(
        OrderManager $orderManager,
        OrderRepository $orderRepository,
        DeliveryTypeRepository $deliveryTypeRepository
    ) {
        $this->orderManager = $orderManager;
        $this->orderRepository = $orderRepository;
        $this->deliveryTypeRepository = $deliveryTypeRepository;
    }

    /**
     * @param array $params
     * @param array $cookies
     */
    public function confirmOrder(array $params, array $cookies)
    {
        $order = $this->getFirstUncompletedOrder($cookies['restaurant_session']);
        $deliveryType = $this->getDeliveryType($params['delivery_type_id']);
        $data = $this->prepareParams(
            $params['phone_number'],
            $order->total_price,
            $deliveryType->price,
            $params['delivery_type_id']
        );
        $this->updateOrder($order, $data);
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    protected function getFirstUncompletedOrder(string $sessionId)
    {
        return $this->orderRepository->firstUncompletedBySessionId($sessionId);
    }

    /**
     * @param string $deliveryTypeId
     * @return mixed
     */
    protected function getDeliveryType(string $deliveryTypeId)
    {
        return $this->deliveryTypeRepository->findById($deliveryTypeId);
    }

    /**
     * @param string $phoneNumber
     * @param float $orderTotalPrice
     * @param float $deliveryTypePrice
     * @param string $deliveryTypeId
     * @return mixed
     */
    protected function prepareParams(
        string $phoneNumber,
        float $orderTotalPrice,
        float $deliveryTypePrice,
        string $deliveryTypeId
    ) {
        $data['phone_number'] = $phoneNumber;
        $data['total_price'] = $orderTotalPrice + $deliveryTypePrice;
        $data['delivery_type_id'] = $deliveryTypeId;
        $data['completed'] = 1;

        return $data;
    }

    /**
     * @param Order $order
     * @param array $data
     */
    protected function updateOrder(Order $order, array $data)
    {
        $this->orderManager->update($order, $data);
    }
}
