<?php

namespace App\Services\Order;

use App\Services\Managers\OrderDishManager;
use App\Services\Managers\OrderManager;
use Illuminate\Support\Collection;

/**
 * Class PriceUpdater
 * @package App\Services\Order
 */
class PriceUpdater
{
    /**
     * @var OrderManager
     */
    protected $orderManager;

    /**
     * @var OrderDishManager
     */
    protected $orderDishManager;

    /**
     * PriceUpdater constructor.
     * @param OrderManager $orderManager
     * @param OrderDishManager $orderDishManager
     */
    public function __construct(
        OrderManager $orderManager,
        OrderDishManager $orderDishManager
    ) {
        $this->orderManager = $orderManager;
        $this->orderDishManager = $orderDishManager;
    }

    /**
     * @param string $id
     * @param string $totalPrice
     */
    public function orderTotalPriceUpdate(string $id, string $totalPrice)
    {
        $this->orderManager->updateById($id, ['total_price' => $totalPrice]);
    }

    /**
     * @param string $id
     * @param string $totalPrice
     */
    public function orderDishTotalPriceUpdate(string $id, string $totalPrice)
    {
        $this->orderDishManager->updateById($id, ['total_price' => $totalPrice]);
    }

    /**
     * @param Collection $dishes
     */
    public function orderDishesTotalPriceUpdate(Collection $dishes)
    {
        foreach ($dishes as $dish) {
            $this->orderDishTotalPriceUpdate($dish['id'], $dish['totalPrice']);
        }
    }
}
