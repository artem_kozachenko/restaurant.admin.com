<?php

namespace App\Services\Order;

use App\Models\OrderDish;
use App\Services\Managers\OrderDishManager;
use App\Services\Managers\OrderManager;
use App\Services\Repositories\MenuDishRepository;
use App\Services\Repositories\MenuRepository;
use App\Services\Repositories\OrderDishRepository;
use App\Services\Repositories\OrderRepository;

/**
 * Class OrderDishCounterChanger
 * @package App\Services
 */
class OrderDishCounterChanger
{
    /**
     * @var OrderManager
     */
    protected $orderManager;

    /**
     * @var MenuRepository
     */
    protected $menuRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var OrderDishManager
     */
    protected $orderDishManager;

    /**
     * @var MenuDishRepository
     */
    protected $menuDishRepository;

    /**
     * @var OrderDishRepository
     */
    protected $orderDishRepository;

    /**
     * OrderDishCounterChanger constructor.
     * @param OrderManager $orderManager
     * @param MenuRepository $menuRepository
     * @param OrderRepository $orderRepository
     * @param OrderDishManager $orderDishManager
     * @param MenuDishRepository $menuDishRepository
     * @param OrderDishRepository $orderDishRepository
     */
    public function __construct(
        OrderManager $orderManager,
        MenuRepository $menuRepository,
        OrderRepository $orderRepository,
        OrderDishManager $orderDishManager,
        MenuDishRepository $menuDishRepository,
        OrderDishRepository $orderDishRepository
    ) {
        $this->orderManager = $orderManager;
        $this->menuRepository = $menuRepository;
        $this->orderRepository = $orderRepository;
        $this->orderDishManager = $orderDishManager;
        $this->menuDishRepository = $menuDishRepository;
        $this->orderDishRepository = $orderDishRepository;
    }

    /**
     * @param array $params
     * @param array $cookies
     * @return int|mixed
     */
    public function increaseOrderDishCounter(array $params, array $cookies)
    {
        $data = $this->getRequiredData($cookies['restaurant_session'], $params['id']);
        if ($data['orderDish']) {
            return $this->increase($data['orderDish']);
        } else {
            return $this->createOrderDish($data['order']->id, $data['menuDish']->id);
        }
    }

    /**
     * @param array $params
     * @param array $cookies
     * @return int|mixed
     * @throws \Exception
     */
    public function decreaseOrderDishCount(array $params, array $cookies)
    {
        $data = $this->getRequiredData($cookies['restaurant_session'], $params['id']);
        if ($data['orderDish']) {
            return $this->decrease($data['orderDish']);
        }

        return 0;
    }

    /**
     * @param string $sessionId
     * @param string $dishId
     * @return mixed
     */
    protected function getRequiredData(string $sessionId, string $dishId)
    {
        $data['order'] = $this->createOrderIfNotExists($sessionId);
        $menu = $this->getThisWeekMenuId();
        $data['menuDish'] = $this->getMenuDish($dishId, $menu->id);
        $data['orderDish'] = $this->getOrderDish($data['menuDish']->id, $data['order']->id);

        return $data;
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    protected function createOrderIfNotExists(string $sessionId)
    {
        return $this->checkOrder($sessionId)
            ? $this->firstUncompletedOrder($sessionId)
            : $this->createOrder($sessionId);
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    protected function checkOrder(string $sessionId)
    {
        return $this->orderRepository->checkExistingUncompletedBySessionId($sessionId);
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    protected function firstUncompletedOrder(string $sessionId)
    {
        return $this->orderRepository->firstUncompletedBySessionId($sessionId);
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    protected function createOrder(string $sessionId)
    {
        return $this->orderManager->create(['session_id' => $sessionId, 'completed' => 0]);
    }

    /**
     * @return mixed
     */
    protected function getThisWeekMenuId()
    {
        return $this->menuRepository->currentWeekMenuId();
    }

    /**
     * @param string $dishId
     * @param string $menuId
     * @return mixed
     */
    protected function getMenuDish(string $dishId, string $menuId)
    {
        return $this->menuDishRepository->findByDishAndMenuId($dishId, $menuId);
    }

    /**
     * @param string $menuDishId
     * @param string $orderID
     * @return mixed
     */
    protected function getOrderDish(string $menuDishId, string $orderID)
    {
        return $this->orderDishRepository->firstByDishAndOrderId($menuDishId, $orderID);
    }

    /**
     * @param OrderDish $orderDish
     * @return int|mixed
     */
    protected function increase(OrderDish $orderDish)
    {
        $orderDish->quantity += 1;
        $orderDish->save();

        return $orderDish->quantity;
    }

    /**
     * @param string $orderId
     * @param string $dishId
     * @return int
     */
    protected function createOrderDish(string $orderId, string $dishId)
    {
        $dishData = [
            'order_id' => $orderId,
            'menu_dish_id' => $dishId,
            'quantity' => 1,
        ];
        $this->orderDishManager->create($dishData);

        return 1;
    }

    /**
     * @param OrderDish $orderDish
     * @return int|mixed
     * @throws \Exception
     */
    protected function decrease(OrderDish $orderDish)
    {
        $orderDish->quantity -= 1;
        if ($orderDish->quantity == 0) {
            $orderDish->delete();

            return 0;
        } else {
            $orderDish->save();

            return $orderDish->quantity;
        }
    }
}
