<?php

namespace App\Services\Order;

use App\Models\Order;
use App\Services\Managers\OrderManager;
use App\Services\Managers\OrderDishManager;
use App\Services\Repositories\OrderRepository;

/**
 * Class OrderDishRemover
 * @package App\Services\Order
 */
class OrderDishRemover
{
    /**
     * @var OrderManager
     */
    protected $orderManager;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var OrderDishManager
     */
    protected $orderDishManager;

    /**
     * OrderDishRemover constructor.
     * @param OrderManager $orderManager
     * @param OrderRepository $orderRepository
     * @param OrderDishManager $orderDishManager
     */
    public function __construct(
        OrderManager $orderManager,
        OrderRepository $orderRepository,
        OrderDishManager $orderDishManager
    ) {
        $this->orderManager = $orderManager;
        $this->orderRepository = $orderRepository;
        $this->orderDishManager = $orderDishManager;
    }

    /**
     * @param array $params
     * @param array $cookies
     * @return mixed
     */
    public function removeOrderDish(array $params, array $cookies)
    {
        $order = $this->getFirstUncompletedOrder($cookies['restaurant_session']);
        $this->deleteOrderDish($params['id'], $order->id);
        $totalPrice = $this->countOrderTotalPrice($order);
        $order = $this->updateOrder($order, $totalPrice);

        return $order->total_price;
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    protected function getFirstUncompletedOrder(string $sessionId)
    {
        return $this->orderRepository->firstUncompletedBySessionId($sessionId);
    }

    /**
     * @param string $dishId
     * @param string $orderId
     */
    protected function deleteOrderDish(string $dishId, string $orderId)
    {
        $this->orderDishManager->deleteByDishAndOrderID($dishId, $orderId);
    }

    /**
     * @param Order $order
     * @return int
     */
    protected function countOrderTotalPrice(Order $order)
    {
        $totalPrice = 0;
        foreach ($order->dishes as $orderDish) {
            $totalPrice += $orderDish->total_price;
        }

        return $totalPrice;
    }

    /**
     * @param Order $order
     * @param string $totalPrice
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function updateOrder(Order $order, string $totalPrice)
    {
        return $this->orderManager->update($order, ['total_price' => $totalPrice]);
    }
}
