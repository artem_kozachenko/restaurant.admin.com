<?php

namespace App\Services\CacheMagic;

/**
 * Class BaseCache
 * @package App\Services\Cache_magic
 */
class BaseCache
{
    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @var array|mixed
     */
    protected $methods = [];

    /**
     * BaseCache constructor.
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
        $params = config(sprintf('object-cache.%s', get_class($this)));
        $this->methods = $params['methods'];
    }

    /**
     * @param string $methodName
     * @param array $optionalParams
     * @return mixed
     * @throws \Exception
     */
    public function getCache(string $methodName, array $optionalParams = [])
    {
        $data = $this
            ->cache
            ->get(
                $this->methods[$methodName]['key'],
                $this->methods[$methodName]['tags']
            );
        if (! $data) {
            $data = $this->{$methodName}($optionalParams);
            $this
                ->cache
                ->set(
                    $this->methods[$methodName]['key'],
                    $data,
                    $this->methods[$methodName]['time'],
                    $this->methods[$methodName]['tags']
                );
        }

        return $data;
    }
}
