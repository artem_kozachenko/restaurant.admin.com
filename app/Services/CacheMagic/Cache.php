<?php

namespace App\Services\CacheMagic;

/**
 * Class Cache
 * @package App\Services\Cache_magic
 */
class Cache
{
    /**
     * @param string $key
     * @param array $tags
     * @return mixed
     * @throws \Exception
     */
    public function get(string $key, array $tags = [])
    {
        return cache()->tags($tags)->get($key);
    }

    /**
     * @param string $key
     * @param $data
     * @param int $time
     * @param array $tags
     * @throws \Exception
     */
    public function set(string $key, $data, int $time, array $tags = [])
    {
        method_exists($data, 'currentPage')
            ? cache()->tags($tags)->put($key . '_' . $data->currentPage(), $data, $time)
            : cache()->tags($tags)->put($key, $data, $time);
    }

    /**
     * @param array $tags
     * @throws \Exception
     */
    public function flushByTags(array $tags)
    {
        cache()->tags($tags)->flush();
    }
}
