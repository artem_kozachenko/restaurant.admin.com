<?php

namespace App\Services\CacheMagic;

use App\Services\Repositories\DishRepository;

/**
 * Class DishCache
 * @package App\Services\Cache
 */
class DishCache extends BaseCache
{
    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * DishCache constructor.
     * @param DishRepository $dishRepository
     * @param Cache $cache
     */
    public function __construct(DishRepository $dishRepository, Cache $cache)
    {
        parent::__construct($cache);

        $this->dishRepository = $dishRepository;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function whereNameLikePaginate(array $params)
    {
        $dishes = $this->dishRepository->whereNameLikePaginate($params['search'], $params['quantity']);
        $dishes->load('photo', 'category');

        return $dishes;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function paginate(array $params)
    {
        $dishes = $this->dishRepository->paginate($params['quantity']);
        $dishes->load('photo', 'category');

        return $dishes;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function whereNameLikeLimit(array $params)
    {
        $dishes = $this->dishRepository->whereNameLikeLimit($params['search']);
        $dishes->load('photo', 'category');

        return $dishes;
    }
}
