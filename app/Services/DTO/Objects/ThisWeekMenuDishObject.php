<?php

namespace App\Services\DTO\Objects;

/**
 * Class ThisWeekMenuDishObject
 * @package App\Services\DTO\Objects
 */
class ThisWeekMenuDishObject
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $quantity;

    /**
     * @var string
     */
    protected $price;

    /**
     * @var string/null
     */
    protected $photo;

    /**
     * @var string
     */
    protected $discount;

    /**
     * @var string
     */
    protected $category;

    /**
     * @var string/null
     */
    protected $description;

    /**
     * @var string
     */
    protected $priceWithDiscount;

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'proto' => $this->getPhoto(),
            'discount' => $this->getDiscount(),
            'category' => $this->getCategory(),
            'quantity' => $this->getQuantity(),
            'description' => $this->getDescription(),
            'priceWithDiscount' => $this->getPriceWithDiscount(),
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string/null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param string $quantity
     */
    public function setQuantity(string $quantity = null): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice(string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string/null
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param array $photo
     */
    public function setPhoto(array $photo): void
    {
        $this->photo = $photo
            ? $photo['name']
            : null;
    }

    /**
     * @return string/null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param array $discount
     */
    public function setDiscount(array $discount = null): void
    {
        $this->discount = $discount
            ? $discount['percent']
            : null;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param array $category
     */
    public function setCategory(array $category): void
    {
        $this->category = $category['name'];
    }

    /**
     * @return string/null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param array $description
     */
    public function setDescription(array $description = null): void
    {
        $this->description = $description
            ? $description['content']
            : null;
    }

    /**
     * @return string
     */
    public function getPriceWithDiscount()
    {
        return $this->priceWithDiscount;
    }

    /**
     * @param string $priceWithDiscount
     */
    public function setPriceWithDiscount(string $priceWithDiscount = null): void
    {
        $this->priceWithDiscount = $priceWithDiscount;
    }
}
