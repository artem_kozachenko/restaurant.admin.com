<?php

namespace App\Services\DTO\Objects;

use Illuminate\Http\UploadedFile;

/**
 * Class DishObject
 * @package App\Services\DTO\Objects
 */
class DishObject
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $price;

    /**
     * @var UploadedFile/null
     */
    protected $photo = null;

    /**
     * @var string
     */
    protected $categoryId;

    /**
     * @var string
     */
    protected $discountId;

    /**
     * @var string/null
     */
    protected $description = '';

    /**
     * @var array
     */
    protected $ingredientsIds = [];

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'photo' => $this->getPhoto(),
            'category_id' => $this->getCategoryId(),
            'discount_id' => $this->getDiscountId(),
            'description' => $this->getDescription(),
            'ingredientsIds' => $this->getIngredientsIds(),
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice(string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return UploadedFile/null
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param UploadedFile $photo
     */
    public function setPhoto(UploadedFile $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return string/null
     */
    public function getDiscountId()
    {
        return $this->discountId;
    }

    /**
     * @param mixed $discountId
     */
    public function setDiscountId(string $discountId): void
    {
        $this->discountId = $discountId;
    }

    /**
     * @return string/null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getIngredientsIds(): array
    {
        return $this->ingredientsIds;
    }

    /**
     * @param string $ingredientsIds
     */
    public function setIngredientsIds(string $ingredientsIds): void
    {
        $this->ingredientsIds = explode(',', $ingredientsIds);
    }
}
