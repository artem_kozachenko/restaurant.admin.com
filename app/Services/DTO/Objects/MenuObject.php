<?php

namespace App\Services\DTO\Objects;

/**
 * Class MenuObject
 * @package App\Services\DTO\Objects
 */
class MenuObject
{
    /**
     * @var string
     */
    protected $date;

    /**
     * @var array
     */
    protected $dishes = [];

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'date' => $this->getDate(),
            'dishes' => $this->getDishes(),
        ];
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function getDishes(): array
    {
        return $this->dishes;
    }

    /**
     * @param mixed $dishes
     */
    public function setDishes($dishes): void
    {
        $this->dishes = explode(',', $dishes);
    }
}
