<?php

namespace App\Services\DTO\Builders;

/**
 * Class DishObjectBuilder
 * @package App\Services\DTO\Builders
 */
class DishObjectBuilder extends BaseBuilder
{
    /**
     * @param array $params
     * @return bool
     */
    protected function validate(array $params): bool
    {
        return true;
    }
}
