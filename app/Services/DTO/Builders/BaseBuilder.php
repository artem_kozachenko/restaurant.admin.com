<?php

namespace App\Services\DTO\Builders;

/**
 * Class BaseBuilder
 * @package App\Services\DTO\Builders
 */
abstract class BaseBuilder
{
    /**
     * @var
     */
    protected $params;

    /**
     * @var array
     */
    protected $methods = [];

    /**
     * @var array
     */
    protected $validation = [];

    /**
     * @var string
     */
    protected $message = 'Error.';

    /**
     * @param array $params
     * @return bool
     */
    abstract protected function validate(array $params): bool;

    /**
     * BaseBuilder constructor.
     */
    public function __construct()
    {
        $this->params = config(sprintf('object.%s', get_class($this)));
        $this->methods = $this->params['methods'];
        $this->validation = $this->params['validation'];
    }

    /**
     * @param array $params
     * @return mixed|null
     */
    public function build(array $params)
    {
        $object = $this->getObject();
        if ($this->validate($params)) {
            $object = $this->prepareObject($object, $params);

            return $object;
        } else {
            return null;
        }
    }

    /**
     * @return mixed
     */
    protected function getObject()
    {
        return new $this->params['object']();
    }

    /**
     * @param $object
     * @param array $params
     * @return mixed
     */
    protected function prepareObject($object, array $params)
    {
        foreach ($this->methods as $value => $method) {
            isset($params[$value])
                ? $object->{$method}($params[$value])
                : null;
        }

        return $object;
    }
}
