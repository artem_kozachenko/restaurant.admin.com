<?php

namespace App\Services\DTO\Builders;

/**
 * Class ThisWeekMenuDishObjectBuilder
 * @package App\Services\DTO\Builders
 */
class ThisWeekMenuDishObjectBuilder extends BaseBuilder
{
    /**
     * @param array $params
     * @return bool
     */
    protected function validate(array $params): bool
    {
        return true;
    }
}
