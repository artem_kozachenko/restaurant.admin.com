<?php

namespace App\Services\CacheFacade\Dish;

use App\Services\CacheFacade\Dish\DataSources\Repository;

/**
 * Class Paginate
 * @package App\Services\Cache_facade\Dish
 */
class Paginate extends Repository
{
    /**
     * @param array $params
     * @return mixed
     */
    protected function getData(array $params)
    {
        $dishes = $this->repository->paginate($params['quantity']);
        $dishes->load('photo', 'category');

        return $dishes;
    }
}
