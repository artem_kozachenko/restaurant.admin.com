<?php

namespace App\Services\CacheFacade\Dish;

/**
 * Class DishCache
 * @package App\Services\Cache
 */
class Facade
{
    /**
     * @var Paginate
     */
    protected $paginate;

    /**
     * @var WhereNameLikeLimit
     */
    protected $whereNameLikeLimit;

    /**
     * @var WhereNameLikePaginate
     */
    protected $whereNameLikePaginate;

    /**
     * DishCache constructor.
     * @param Paginate $paginate
     * @param WhereNameLikeLimit $whereNameLikeLimit
     * @param WhereNameLikePaginate $whereNameLikePaginate
     */
    public function __construct(
        Paginate $paginate,
        WhereNameLikeLimit $whereNameLikeLimit,
        WhereNameLikePaginate $whereNameLikePaginate
    ) {
        $this->paginate = $paginate;
        $this->whereNameLikeLimit = $whereNameLikeLimit;
        $this->whereNameLikePaginate = $whereNameLikePaginate;
    }

    /**
     * @param array $params
     * @param string|null $page
     * @return mixed
     * @throws \Exception
     */
    public function paginate(array $params, string $page = null)
    {
        return $this
            ->paginate
            ->getCache(
                'dishes_all',
                60,
                ['dishes'],
                $params,
                $page
            );
    }

    /**
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function whereNameLikeLimit(array $params)
    {
        return $this
            ->whereNameLikeLimit
            ->getCache(
                'dishes_search_autocomplete',
                60,
                ['dishes'],
                $params,
                null
            );
    }

    /**
     * @param array $params
     * @param string|null $page
     * @return mixed
     * @throws \Exception
     */
    public function whereNameLikePaginate(array $params, string $page = null)
    {
        return $this
            ->whereNameLikePaginate
            ->getCache(
                'dishes_search',
                60,
                ['dishes'],
                $params,
                $page
            );
    }
}
