<?php

namespace App\Services\CacheFacade\Dish;

use App\Services\CacheFacade\Dish\DataSources\Repository;

/**
 * Class WhereNameLikePaginate
 * @package App\Services\Cache_facade\Dish
 */
class WhereNameLikePaginate extends Repository
{
    /**
     * @param array $params
     * @return mixed
     */
    protected function getData(array $params)
    {
        $dishes = $this->repository->whereNameLikePaginate($params['search'], $params['quantity']);
        $dishes->load('photo', 'category');

        return $dishes;
    }
}
