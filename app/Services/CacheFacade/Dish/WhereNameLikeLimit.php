<?php

namespace App\Services\CacheFacade\Dish;

use App\Services\CacheFacade\Dish\DataSources\Repository;

/**
 * Class WhereNameLikeLimit
 * @package App\Services\Cache_facade\Dish
 */
class WhereNameLikeLimit extends Repository
{
    /**
     * @param array $params
     * @return mixed
     */
    protected function getData(array $params)
    {
        $dishes = $this->repository->whereNameLikeLimit($params['search']);
        $dishes->load('photo', 'category');

        return $dishes;
    }
}
