<?php

namespace App\Services\CacheFacade\DataSources\Interfaces;

/**
 * Interface SourceInterface
 * @package App\Services\CacheFacade\DataSources\Interfaces
 */
interface SourceInterface
{
    /**
     * @return object
     */
    public function getSource();
}
