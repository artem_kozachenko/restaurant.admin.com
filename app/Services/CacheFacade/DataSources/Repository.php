<?php

namespace App\Services\CacheFacade\Dish\DataSources;

use App\Services\CacheFacade\BaseCache;
use App\Services\CacheFacade\DataSources\Interfaces\SourceInterface;
use App\Services\CacheMagic\Cache;

/**
 * Class Repository
 * @package App\Services\CacheFacade\Dish\DataSources
 */
abstract class Repository extends BaseCache implements SourceInterface
{
    /**
     * @var mixed
     */
    protected $repository;

    /**
     * Repository constructor.
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        parent::__construct($cache);

        $this->repository = $this->getSource();
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        $source = config(sprintf('object-cache.facade-cache.repositories.%s', get_class($this)));

        return new $source();
    }
}
