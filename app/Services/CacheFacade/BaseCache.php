<?php

namespace App\Services\CacheFacade;

use App\Services\CacheMagic\Cache;

/**
 * Class BaseCache
 * @package App\Services\Cache_magic
 */
abstract class BaseCache
{
    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @param array $params
     * @return mixed
     */
    abstract protected function getData(array $params);

    /**
     * BaseCache constructor.
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $key
     * @param int $time
     * @param array $tags
     * @param array $optionalParams
     * @param string|null $page
     * @return mixed
     * @throws \Exception
     */
    public function getCache(string $key, int $time, array $tags = [], array $optionalParams = [], string $page = null)
    {
        if (! is_null($page)) {
            $key = $this->resetKey($key, $page);
        }
        $data = $this->cache->get($key, $tags);
        if (! $data) {
            $data = $this->getData($optionalParams);
            $this->cache->set($key, $data, $time, $tags);
        }

        return $data;
    }

    /**
     * @param string $key
     * @param string $page
     * @return string
     */
    protected function resetKey(string $key, string $page)
    {
        return $key . '_' . $page;
    }
}
