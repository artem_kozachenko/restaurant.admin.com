<?php

namespace App\Services\DataTransformers;

use App\Models\MenuDish;
use App\Services\DTO\Builders\ThisWeekMenuDishObjectBuilder;
use App\Services\Generators\PriceGenerator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class MenuData
 * @package App\Services\DataTransformers
 */
class MenuData
{
    /**
     * @var PriceGenerator
     */
    protected $priceGenerator;

    /**
     * @var ThisWeekMenuDishObjectBuilder
     */
    protected $thisWeekMenuDishObjectBuilder;

    /**
     * MenuData constructor.
     * @param PriceGenerator $priceGenerator
     * @param ThisWeekMenuDishObjectBuilder $thisWeekMenuDishObjectBuilder
     */
    public function __construct(
        PriceGenerator $priceGenerator,
        ThisWeekMenuDishObjectBuilder $thisWeekMenuDishObjectBuilder
    ) {
        $this->priceGenerator = $priceGenerator;
        $this->thisWeekMenuDishObjectBuilder = $thisWeekMenuDishObjectBuilder;
    }

    /**
     * @param MenuDish $menuDish
     * @param Collection|null $order
     * @return mixed|null
     */
    public function getCurrentWeekMenuDish(MenuDish $menuDish, Collection $order = null)
    {
        $dish = $menuDish->dish->toArray();
        $dish['quantity'] = isset($order[$menuDish->id])
            ? $order[$menuDish->id]
            : null;
        $dish['price'] = $menuDish->price;
        $dish['priceWithDiscount'] = $dish['discount']
            ? $this->priceGenerator->generatePriceWithDiscount(
                $dish['price'],
                $dish['discount']
            )
            : null;
        $dish = $this->thisWeekMenuDishObjectBuilder->build($dish);

        return $dish;
    }
}
