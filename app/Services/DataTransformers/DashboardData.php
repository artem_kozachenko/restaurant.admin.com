<?php

namespace App\Services\DataTransformers;

use Illuminate\Support\Collection;

/**
 * Class DashboardData
 * @package App\Services\DataTransformers
 */
class DashboardData
{
    /**
     * @param Collection $orderDishes
     * @param array $dates
     * @return array
     */
    public function transformChartData(Collection $orderDishes, array $dates)
    {
        $dishesNamesWithArray = array_fill_keys($orderDishes->pluck('menuDish.dish.name')->toArray(), []);
        $dishesNamesWithQuantity = array_fill_keys($orderDishes->pluck('menuDish.dish.name')->toArray(), 0);
        $orderDishes = $this->groupByDate($orderDishes)->toArray();
        $dates = $this->fillDatesWithDishes($dates, $dishesNamesWithArray);
        $orderDishes = $this->addMissingDates($dates, $orderDishes);
        $data = $this->transformOrderDishes($orderDishes);
        $data = $this->addMissingDishesNames($data, $dishesNamesWithQuantity);
        $this->sortByDate($data);
        $data = $this->createChartArray($data);

        return $data;
    }

    /**
     * @param Collection $orderDishes
     * @return Collection
     */
    protected function groupByDate(Collection $orderDishes)
    {
        return $orderDishes->groupBy([
            function ($item) {
                return $item->updated_at->format('Y-m-d');
            },
            'menuDish.dish.name'
        ]);
    }

    /**
     * @param array $dates
     * @param array $dishesNames
     * @return array
     */
    protected function fillDatesWithDishes(array $dates, array $dishesNames)
    {
        return array_fill_keys($dates, $dishesNames);
    }

    /**
     * @param array $dates
     * @param array $orderDishes
     * @return array
     */
    protected function addMissingDates(array $dates, array $orderDishes)
    {
        $diff = array_diff_key($dates, $orderDishes);
        $orderDishes = array_merge($orderDishes, $diff);

        return $orderDishes;
    }

    /**
     * @param array $orderDishes
     * @return array
     */
    protected function transformOrderDishes(array $orderDishes)
    {
        $data = [];
        foreach ($orderDishes as $date => $orderDishesNames) {
            $dataRow = $this->generateDataRow($orderDishesNames, $date);
            $data[] = $dataRow;
        }

        return $data;
    }

    /**
     * @param array $orderDishesNames
     * @param string $date
     * @return array
     */
    protected function generateDataRow(array $orderDishesNames, string $date)
    {
        $dataRow = [];
        $dataRow = $this->setDate($dataRow, $date);
        foreach ($orderDishesNames as $name => $orderDishes) {
            $quantity = $this->generateQuantity($orderDishes);
            $dataRow[$name] = $quantity;
        }

        return $dataRow;
    }

    /**
     * @param array $dataRow
     * @param string $date
     * @return array
     */
    protected function setDate(array $dataRow, string $date)
    {
        $dataRow['Week'] = $date;

        return $dataRow;
    }

    /**
     * @param array $orderDishes
     * @return int
     */
    protected function generateQuantity(array $orderDishes)
    {
        $quantity = 0;
        foreach ($orderDishes as $orderDish) {
            $quantity += $orderDish['quantity'];
        }

        return $quantity;
    }

    /**
     * @param array $data
     * @param array $dishesNames
     * @return array
     */
    protected function addMissingDishesNames(array $data, array $dishesNames)
    {
        $tempData = [];
        foreach ($data as $dataRow) {
            $diff = array_diff_key($dishesNames, $dataRow);
            $dataRow = array_merge($dataRow, $diff);
            $tempData[] = $dataRow;
        }

        return $tempData;
    }

    protected function sortByDate(array &$data)
    {
        asort($data);
    }

    /**
     * @param array $data
     * @return array
     */
    protected function createChartArray(array $data)
    {
        $header = true;
        $chartArray = [];
        foreach ($data as $dataRow) {
            $chartArray = $header
                ? $this->setChartArrayHeader($chartArray, $dataRow, $header)
                : $this->setChartArrayContent($chartArray, $dataRow);
        }

        return $chartArray;
    }

    /**
     * @param array $chartArray
     * @param array $dataRow
     * @param string $header
     * @return array
     */
    protected function setChartArrayHeader(array $chartArray, array $dataRow, string &$header)
    {
        $chartArray[] = array_keys($dataRow);
        $header = false;

        return $chartArray;
    }

    /**
     * @param array $chartArray
     * @param array $dataRow
     * @return array
     */
    protected function setChartArrayContent(array $chartArray, array $dataRow)
    {
        $chartArray[] = array_values($dataRow);

        return $chartArray;
    }
}
