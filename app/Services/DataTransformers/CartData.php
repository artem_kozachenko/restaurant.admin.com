<?php

namespace App\Services\DataTransformers;

use App\Models\Order;
use App\Models\OrderDish;
use App\Services\Generators\PriceGenerator;

/**
 * Class CartData
 * @package App\Services\DataTransformers
 */
class CartData
{
    /**
     * @var PriceGenerator
     */
    protected $priceGenerator;

    /**
     * CartData constructor.
     * @param PriceGenerator $priceGenerator
     */
    public function __construct(PriceGenerator $priceGenerator)
    {
        $this->priceGenerator = $priceGenerator;
    }

    /**
     * @param OrderDish $orderDish
     * @return mixed
     */
    public function getOrderDish(OrderDish $orderDish)
    {
        $data['id'] = $orderDish->id;
        $data['name'] = $orderDish->menuDish->dish->name;
        $data['price'] = $this->getPrice($orderDish);
        $data['quantity'] = $orderDish->quantity;
        $data['total'] = $data['price'] * $data['quantity'];

        return $data;
    }

    /**
     * @param Order $order
     * @param OrderDish $orderDish
     * @return mixed
     */
    public function getUpdatedPrices(Order $order, OrderDish $orderDish)
    {
        $data['orderId'] = $order->id;
        $data['orderTotalPrice'] = $order->total_price;
        $data['orderTotalPrice'] -= $orderDish->total_price;
        $data['orderDishPrice'] = $this->getPrice($orderDish);
        $data['orderDishTotalPrice'] = $data['orderDishPrice'] * $orderDish->quantity;
        $data['orderTotalPrice'] += $data['orderDishTotalPrice'];
        $data['orderDishId'] = $orderDish->id;

        return $data;
    }

    /**
     * @param OrderDish $orderDish
     * @return float|null
     */
    protected function getPrice(OrderDish $orderDish)
    {
        $price = null;
        if ($orderDish->menuDish->dish->discount_id) {
            $price = $this->getPriceWithDiscount($orderDish);
        }

        return $price
            ? $price
            : $orderDish->menuDish->price;
    }

    /**
     * @param OrderDish $orderDish
     * @return float|null
     */
    protected function getPriceWithDiscount(OrderDish $orderDish)
    {
        $price = $this->priceGenerator->generatePriceWithDiscount(
            $orderDish->menuDish->price,
            $orderDish->menuDish->dish->discount->toArray()
        );

        return $price;
    }
}
