<?php

namespace App\Services\Generators;

/**
 * Class NameGenerator
 * @package App\Services\Generators
 */
class NameGenerator
{
    /**
     * @param string $name
     * @return string
     */
    public function urlName(string $name)
    {
        return strtolower(str_replace(' ', '-', $name));
    }
}
