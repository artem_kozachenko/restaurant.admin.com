<?php

namespace App\Services\Generators;

use Carbon\Carbon;

/**
 * Class TimeGenerator
 * @package App\Services\Generators
 */
class TimeGenerator
{
    /**
     * @param string $tz
     * @param string $format
     * @return string
     */
    public function now(string $tz = 'Europe/Kiev', string $format = 'H:i:s')
    {
        return Carbon::now($tz)->format($format);
    }

    /**
     * @param string $time
     * @param string $tz
     * @param string $format
     * @return string
     */
    public function createFromTimeString(string $time, string $tz = 'Europe/Kiev', string $format = 'H:i:s')
    {
        return Carbon::createFromTimeString($time, $tz)->format($format);
    }

    /**
     * @return Carbon
     */
    public function yesterday()
    {
        return Carbon::yesterday();
    }

    /**
     * @return string
     */
    public function currentWeek()
    {
        return Carbon::now()->format('Y-W');
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $format
     * @return array
     * @throws \Exception
     */
    public function createInterval(string $from, string $to, string $format)
    {
        $dates = [];
        $begin = new \DateTime($from);
        $begin->sub(new \DateInterval('P1D'));
        $end = new \DateTime($to);
        $end->add(new \DateInterval('P1D'));
        $interval = new \DateInterval($format);
        $dateRange = new \DatePeriod($begin, $interval, $end);
        foreach ($dateRange as $date) {
            $dates[] = $date->format("Y-m-d") . "";
        }

        return $dates;
    }
}
