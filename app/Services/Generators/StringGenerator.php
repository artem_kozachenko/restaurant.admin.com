<?php

namespace App\Services\Generators;

use Illuminate\Support\Str;

/**
 * Class StringGenerator
 * @package App\Services\Generators
 */
class StringGenerator
{
    /**
     * @param int $length
     * @return string
     */
    public function random(int $length)
    {
        return Str::random($length);
    }

    /**
     * @param string $string
     * @return string
     */
    public function bcrypt(string $string)
    {
        return bcrypt($string);
    }
}
