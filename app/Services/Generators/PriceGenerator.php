<?php

namespace App\Services\Generators;

/**
 * Class PriceGenerator
 * @package App\Services\Generators
 */
class PriceGenerator
{
    /**
     * @var TimeGenerator
     */
    protected $timeGenerator;

    /**
     * PriceGenerator constructor.
     * @param TimeGenerator $timeGenerator
     */
    public function __construct(TimeGenerator $timeGenerator)
    {
        $this->timeGenerator = $timeGenerator;
    }

    /**
     * @param string $price
     * @param array $discountData
     * @return float|null
     */
    public function generatePriceWithDiscount(string $price, array $discountData)
    {
        $now = $this->timeGenerator->now();

        return $now > $discountData['from'] && $now < $discountData['to']
            ? $this->calculatePrice($price, $discountData['percent'])
            : null;
    }

    /**
     * @param $price
     * @param $percent
     * @return float
     */
    protected function calculatePrice($price, $percent)
    {
        $price = $price - ($price * ($percent / 100));

        return round($price, 2);
    }
}
