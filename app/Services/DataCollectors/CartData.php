<?php

namespace App\Services\DataCollectors;

use App\Models\Discount;
use App\Models\Order;
use App\Models\OrderDish;
use App\Services\Generators\TimeGenerator;
use App\Services\Repositories\OrderDishRepository;
use App\Services\Repositories\OrderRepository;
use App\Services\Repositories\DeliveryTypeRepository;
use App\Services\DataTransformers\CartData as CartDataTransformer;

/**
 * Class CartData
 * @package App\Services\DataCollectors
 */
class CartData
{
    /**
     * @var TimeGenerator
     */
    protected $timeGenerator;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var OrderDishRepository
     */
    protected $orderDishRepository;

    /**
     * @var CartDataTransformer
     */
    protected $cartDataTransformer;

    /**
     * @var DeliveryTypeRepository
     */
    protected $deliveryTypeRepository;

    /**
     * CartData constructor.
     * @param TimeGenerator $timeGenerator
     * @param OrderRepository $orderRepository
     * @param OrderDishRepository $orderDishRepository
     * @param CartDataTransformer $cartDataTransformer
     * @param DeliveryTypeRepository $deliveryTypeRepository
     */
    public function __construct(
        TimeGenerator $timeGenerator,
        OrderRepository $orderRepository,
        OrderDishRepository $orderDishRepository,
        CartDataTransformer $cartDataTransformer,
        DeliveryTypeRepository $deliveryTypeRepository
    ) {
        $this->timeGenerator = $timeGenerator;
        $this->orderRepository = $orderRepository;
        $this->orderDishRepository = $orderDishRepository;
        $this->cartDataTransformer = $cartDataTransformer;
        $this->deliveryTypeRepository = $deliveryTypeRepository;
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    public function gatCartData(string $sessionId)
    {
        $order = $this->getOrder($sessionId);
        $data['orderData'] = $order
            ? $this->getOrderDishesData($order)
            : $this->getDefaultOrderDishesData();
        $data['deliveryTypes'] = $this->deliveryTypeRepository->all();

        return $data;
    }

    /**
     * @param string $sessionId
     * @param string $orderDishId
     * @return mixed
     */
    public function getUpdatedPrices(string $sessionId, string $orderDishId)
    {
        $order = $this->getOrder($sessionId);
        $orderDish = $this->orderDishRepository->findById($orderDishId);
        $data = $this->cartDataTransformer->getUpdatedPrices($order, $orderDish);

        return $data;
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    protected function getOrder(string $sessionId)
    {
        return $this->orderRepository->firstUncompletedBySessionId($sessionId);
    }

    /**
     * @param Order $order
     * @return \Illuminate\Support\Collection
     */
    protected function getOrderDishesData(Order $order)
    {
        $this->loadOrderRelations($order);
        $data = $this->collectOrderDishesData($order);
        $orderData = $this->fillOrderData($data);

        return $orderData;
    }

    /**
     * @param Order $order
     */
    protected function loadOrderRelations(Order $order)
    {
        $order->load('dishes', 'dishes.menuDish', 'dishes.menuDish.dish', 'dishes.menuDish.dish.discount');
    }

    /**
     * @param Order $order
     * @return array
     */
    protected function collectOrderDishesData(Order $order)
    {
        $data['orderTotalPrice']= 0;
        $data['orderDishes']= collect();
        $data['discounts'] = collect();
        $data['dishesToUpdate'] = collect();
        foreach ($order->dishes as $dish) {
            $data = $this->collectOrderDishData($dish, $data);
        }
        $data['orderId'] = $order->id;

        return $data;
    }

    /**
     * @param array $data
     * @return \Illuminate\Support\Collection
     */
    protected function fillOrderData(array $data)
    {
        $orderData = collect([
            'discounts' => $data['discounts'],
            'orderDishes' => $data['orderDishes'],
            'orderTotalPrice' => $data['orderTotalPrice'],
            'dishesToUpdate' => $data['dishesToUpdate'],
            'orderId' => $data['orderId'],
        ]);

        return $orderData;
    }

    /**
     * @param OrderDish $dish
     * @param array $data
     * @return array
     */
    protected function collectOrderDishData(OrderDish $dish, array $data)
    {
        $orderDishData = $this->cartDataTransformer->getOrderDish($dish);
        $discountData = $this->getDiscountData($orderDishData['id'], $dish->menuDish->dish->discount);
        $discountData ? $data['discounts']->push($discountData) : null;
        $data['orderTotalPrice'] += $orderDishData['total'];
        $data['orderDishes']->push($orderDishData);
        $data['dishesToUpdate']->push(['id' => $dish->id, 'totalPrice' => $orderDishData['total']]);

        return $data;
    }

    /**
     * @param string $id
     * @param Discount|null $discount
     * @return null
     */
    protected function getDiscountData(string $id, Discount $discount = null)
    {
        $now = $this->timeGenerator->now();
        if ($discount && $discount->from < $now && $discount->to > $now) {
            $data['id'] = $id;
            $data['endTime'] = $this->timeGenerator->createFromTimeString($discount->to, 'Europe/Kiev', 'Y-m-d H:i:s');

            return $data;
        }

        return null;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    protected function getDefaultOrderDishesData()
    {
        $data = collect();
        $data->put('discounts', []);
        $data->put('orderDishes', []);
        $data->put('orderTotalPrice', '');

        return $data;
    }
}
