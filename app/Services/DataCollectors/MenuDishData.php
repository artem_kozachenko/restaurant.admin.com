<?php

namespace App\Services\DataCollectors;

use App\Services\Repositories\MenuDishRepository;
use App\Traits\Pagination;

/**
 * Class MenuDishData
 * @package App\Services\DataGetters
 */
class MenuDishData
{
    use Pagination;

    /**
     * @var MenuDishRepository
     */
    protected $menuDishRepository;

    /**
     * MenuDishData constructor.
     * @param MenuDishRepository $menuDishRepository
     */
    public function __construct(MenuDishRepository $menuDishRepository)
    {
        $this->menuDishRepository = $menuDishRepository;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function dataRequiredForExport(array $params)
    {
        $data['dishHistory'] = $this->menuDishRepository
            ->getDishHistory($params['dish_id'], $params['date_from'], $params['date_to']);
        $data['dishHistory'] = $this->paginate(
            $data['dishHistory'],
            15,
            [
                'path' => 'history',
                'query' => [
                    'dish_id' => $params['dish_id'],
                    'date_from' => $params['date_from'],
                    'date_to' => $params['date_to'],
                ]
            ]
        );
        $data['dish_id'] = $params['dish_id'];
        $data['date_from'] = $params['date_from'];
        $data['date_to'] = $params['date_to'];

        return $data;
    }
}
