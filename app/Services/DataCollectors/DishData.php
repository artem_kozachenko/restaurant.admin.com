<?php

namespace App\Services\DataCollectors;

use App\Services\CacheFacade\Dish\Facade as DishCache;

/**
 * Class DishData
 * @package App\Services\DataCollectors
 */
class DishData
{
    /**
     * @var DishCache
     */
    protected $dishCache;

    /**
     * DishController constructor.
     * @param DishCache $cache
     */
    public function __construct(DishCache $cache)
    {
        $this->dishCache = $cache;
    }

    /**
     * @param string|null $search
     * @param string $page
     * @return mixed
     * @throws \Exception
     */
    public function search(string $search = null, string $page = null)
    {
        return $search
            ? $this->dishCache->whereNameLikePaginate(['search' => $search, 'quantity' => 12], $page)
            : $this->dishCache->paginate(['quantity' => 12], $page);
    }
}
