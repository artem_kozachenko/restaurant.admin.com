<?php

namespace App\Services\DataCollectors;

use App\Models\Menu;
use App\Models\Order;
use Illuminate\Support\Collection;
use App\Services\Repositories\DishRepository;
use App\Services\Repositories\MenuRepository;
use App\Services\Repositories\OrderRepository;
use App\Services\DataTransformers\MenuData as MenuDataTransformer;

/**
 * Class MenuData
 * @package App\Services\DataCollectors
 */
class MenuData
{
    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * @var MenuRepository
     */
    protected $menuRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var MenuDataTransformer
     */
    protected $menuDataTransformer;

    /**
     * MenuData constructor.
     * @param DishRepository $dishRepository
     * @param MenuRepository $menuRepository
     * @param OrderRepository $orderRepository
     * @param MenuDataTransformer $menuDataTransformer
     */
    public function __construct(
        DishRepository $dishRepository,
        MenuRepository $menuRepository,
        OrderRepository $orderRepository,
        MenuDataTransformer $menuDataTransformer
    ) {
        $this->dishRepository = $dishRepository;
        $this->menuRepository = $menuRepository;
        $this->orderRepository = $orderRepository;
        $this->menuDataTransformer = $menuDataTransformer;
    }

    /**
     * @return mixed
     */
    public function lastMenu()
    {
        $lastMenu = $this->menuRepository->lastBeforeNow();
        if ($lastMenu) {
            $lastMenuDishesIds = $lastMenu->dishes->pluck('id')->toArray();
            $data['lastMenuDishes'] = $this->dishRepository->findById($lastMenuDishesIds);
            $data['remainDishes'] = $this->dishRepository->whereNotIn($lastMenuDishesIds);
        } else {
            $data['lastMenuDishes'] = [];
            $data['remainDishes'] = $this->dishRepository->all();
        }

        return $data;
    }

    /**
     * @param string $sessionId
     * @return \Illuminate\Support\Collection
     */
    public function currentWeekMenuData(string $sessionId = null)
    {
        $menu = collect();
        $currentWeekMenu = $this->menuRepository->currentWeekMenu();
        $order = $this->getOrder($sessionId);
        if ($currentWeekMenu) {
            $menu = $this->collectMenuDishes($currentWeekMenu, $menu, $order);
        }

        return $menu;
    }

    /**
     * @param string $sessionId
     * @return mixed
     */
    protected function getOrder(string $sessionId = null)
    {
        $order = $this->orderRepository->firstUncompletedBySessionId($sessionId);

        return $order
            ? $this->transformOrder($order)
            : null;
    }

    /**
     * @param $order
     * @return mixed
     */
    protected function transformOrder($order)
    {
        $order = $order->dishes->mapWithKeys(function ($item) {
            return [$item['menu_dish_id'] => $item['quantity']];
        });

        return $order;
    }

    /**
     * @param Menu $currentWeekMenu
     * @param Collection $menu
     * @param Order|null $order
     * @return Collection
     */
    protected function collectMenuDishes(Menu $currentWeekMenu, Collection $menu, Collection $order = null)
    {
        foreach ($currentWeekMenu->dishes as $menuDish) {
            $dish = $this->menuDataTransformer->getCurrentWeekMenuDish($menuDish, $order);
            $menu->push($dish);
        }
        $menu = $this->groupByCategory($menu);

        return $menu;
    }

    /**
     * @param Collection $menu
     * @return Collection
     */
    protected function groupByCategory(Collection $menu)
    {
        $menu = $menu->groupBy(function ($item, $key) {
            return $item->getCategory();
        });

        return $menu;
    }
}
