<?php

namespace App\Services\DataCollectors;

use App\Services\DataTransformers\DashboardData as DashboardDataTransformer;
use App\Services\Generators\TimeGenerator;
use App\Services\Repositories\MenuDishRepository;
use App\Services\Repositories\OrderDishRepository;

/**
 * Class DashboardData
 * @package App\Services\DataCollectors
 */
class DashboardData
{
    /**
     * @var TimeGenerator
     */
    protected $timeGenerator;

    /**
     * @var MenuDishRepository
     */
    protected $menuDishRepository;

    /**
     * @var OrderDishRepository
     */
    protected $orderDishRepository;

    /**
     * @var DashboardDataTransformer
     */
    protected $dashboardDataTransformer;

    /**
     * DashboardData constructor.
     * @param TimeGenerator $timeGenerator
     * @param MenuDishRepository $menuDishRepository
     * @param OrderDishRepository $orderDishRepository
     * @param DashboardDataTransformer $dashboardDataTransformer
     */
    public function __construct(
        TimeGenerator $timeGenerator,
        MenuDishRepository $menuDishRepository,
        OrderDishRepository $orderDishRepository,
        DashboardDataTransformer $dashboardDataTransformer
    ) {
        $this->timeGenerator = $timeGenerator;
        $this->menuDishRepository = $menuDishRepository;
        $this->orderDishRepository = $orderDishRepository;
        $this->dashboardDataTransformer = $dashboardDataTransformer;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getChartData(array $params)
    {
        $dishesIds = $this->getDishesIds($params['dishes_ids']);
        $menuDishIds = $this->menuDishRepository->allByDishesIds($dishesIds)->pluck('id')->toArray();
        $orderDishes = $this->orderDishRepository
            ->allByDateAndMenuDishesIds($params['date_from'], $params['date_to'], $menuDishIds);
        $dates = $this->timeGenerator->createInterval($params['date_from'], $params['date_to'], 'P1D');
        $data = $this->dashboardDataTransformer->transformChartData($orderDishes, $dates);

        return $data;
    }

    /**
     * @param string $dishesIds
     * @return array|string
     */
    protected function getDishesIds(string $dishesIds)
    {
        if ($dishesIds) {
            $dishesIds = explode(',', $dishesIds);
        }

        return $dishesIds;
    }
}
