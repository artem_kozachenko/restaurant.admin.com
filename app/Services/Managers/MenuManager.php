<?php

namespace App\Services\Managers;

use App\Models\Menu;

/**
 * Class MenuManager
 * @package App\Services\Managers
 */
class MenuManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Menu::class;
    }
}
