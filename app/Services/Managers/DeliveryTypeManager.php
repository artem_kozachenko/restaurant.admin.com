<?php

namespace App\Services\Managers;

use App\Models\DeliveryType;

/**
 * Class DeliveryTypeManager
 * @package App\Services\Managers
 */
class DeliveryTypeManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return DeliveryType::class;
    }
}
