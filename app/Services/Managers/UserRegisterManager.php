<?php

namespace App\Services\Managers;

use App\Models\UserRegister;

/**
 * Class RegisterUserManager
 * @package App\Services\Managers
 */
class UserRegisterManager
{
    /**
     * @param array $params
     */
    public function create(array $params)
    {
        UserRegister::create($params);
    }

    /**
     * @param string $token
     */
    public function deleteByToken(string $token)
    {
        UserRegister::where('token', $token)->delete();
    }

    /**
     * @param string $time
     */
    public function deleteByTime(string $time)
    {
        UserRegister::where('created_at', '<', $time)->delete();
    }
}
