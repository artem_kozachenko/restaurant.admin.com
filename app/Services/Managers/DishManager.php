<?php

namespace App\Services\Managers;

use App\Models\Dish;

/**
 * Class DishManager
 * @package App\Services\Managers
 */
class DishManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Dish::class;
    }

    /**
     * @param Dish $dish
     * @param array $ingredients
     */
    public function attachIngredients(Dish $dish, array $ingredients)
    {
        $dish->ingredients()->attach($ingredients);
    }

    /**
     * @param Dish $dish
     * @param array $ingredients
     */
    public function detachIngredients(Dish $dish, array $ingredients)
    {
        $dish->ingredients()->detach($ingredients);
    }
}
