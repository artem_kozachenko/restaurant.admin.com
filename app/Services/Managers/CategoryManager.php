<?php

namespace App\Services\Managers;

use App\Models\Category;

/**
 * Class CategoryManager
 * @package App\Services\Managers
 */
class CategoryManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Category::class;
    }
}
