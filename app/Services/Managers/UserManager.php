<?php

namespace App\Services\Managers;

use App\Models\User;

/**
 * Class UserManager
 * @package App\Services\Managers
 */
class UserManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return User::class;
    }
}