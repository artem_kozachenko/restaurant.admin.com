<?php

namespace App\Services\Managers;

use App\Models\Order;

/**
 * Class OrderManager
 * @package App\Services\Managers
 */
class OrderManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Order::class;
    }

    /**
     * @param string $id
     * @param array $params
     */
    public function updateById(string $id, array $params)
    {
        Order::where('id', $id)->update($params);
    }
}
