<?php

namespace App\Services\Managers;

use App\Models\Photo;

/**
 * Class PhotoManager
 * @package App\Services\Managers
 */
class PhotoManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Photo::class;
    }
}
