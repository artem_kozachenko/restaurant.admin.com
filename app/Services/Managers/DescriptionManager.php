<?php

namespace App\Services\Managers;

use App\Models\Description;

/**
 * Class DescriptionManager
 * @package App\Services\Managers
 */
class DescriptionManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Description::class;
    }
}
