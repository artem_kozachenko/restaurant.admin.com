<?php

namespace App\Services\Managers;

use App\Models\OrderDish;

/**
 * Class OrderDishManager
 * @package App\Services\Managers
 */
class OrderDishManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return OrderDish::class;
    }

    /**
     * @param string $id
     * @param string $orderId
     */
    public function deleteByDishAndOrderID(string $id, string $orderId)
    {
        OrderDish::where([
            ['id', $id],
            ['order_id', $orderId]
        ])->delete();
    }

    /**
     * @param string $id
     * @param array $params
     */
    public function updateById(string $id, array $params)
    {
        OrderDish::where('id', $id)->update($params);
    }
}
