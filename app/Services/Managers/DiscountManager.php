<?php

namespace App\Services\Managers;

use App\Models\Discount;

/**
 * Class DiscountManager
 * @package App\Services\Managers
 */
class DiscountManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Discount::class;
    }
}
