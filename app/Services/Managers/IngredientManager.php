<?php

namespace App\Services\Managers;

use App\Models\Ingredient;

/**
 * Class IngredientManager
 * @package App\Services\Managers
 */
class IngredientManager extends BaseManager
{
    /**
     * @return mixed|string
     */
    protected function getClass()
    {
        return Ingredient::class;
    }
}
