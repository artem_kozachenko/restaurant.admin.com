<?php

namespace App\Services\Managers;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseManager
 * @package App\Services\Managers
 */
abstract class BaseManager
{
    /**
     * @var
     */
    protected $entity;

    /**
     * @return mixed
     */
    abstract protected function getClass();

    /**
     * BaseManager constructor.
     */
    public function __construct()
    {
        $this->entity = $this->getClass();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function create(array $params)
    {
        return $this->entity::create($params);
    }

    /**
     * @param Model $entity
     * @param array $params
     * @return Model
     */
    public function update(Model $entity, array $params)
    {
        $entity->update($params);

        return $entity;
    }

    /**
     * @param Model $entity
     * @throws \Exception
     */
    public function delete(Model $entity)
    {
        $entity->delete();
    }
}
