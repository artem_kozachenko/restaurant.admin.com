<?php

namespace App\Services\Managers;

use App\Models\MenuDish;

/**
 * Class MenuDishesManager
 * @package App\Services\Managers
 */
class MenuDishManager
{
    /**
     * @param array $params
     */
    public function create(array $params)
    {
        MenuDish::insert($params);
    }

    /**
     * @param array $dishesIds
     * @param array $prices
     */
    public function updateByIds(array $dishesIds, array $prices)
    {
        foreach ($dishesIds as $dishId) {
            MenuDish::where('id', $dishId)->update(['price' => $prices[$dishId]]);
        }
    }

    /**
     * @param array $params
     */
    public function deleteByIds(array $params)
    {
        MenuDish::whereIn('id', $params)->delete();
    }

    /**
     * @param string $id
     */
    public function deleteByDishId(string $id)
    {
        MenuDish::where('dish_id', $id)->delete();
    }
}
