<?php

namespace App\Http\Controllers\AdminSide;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExportDataRequest;
use App\Services\DataCollectors\MenuDishData;
use App\Services\Files\Export\Objects\MenuDish;
use App\Services\Repositories\DishRepository;

/**
 * Class ExportController
 * @package App\Http\Controllers\AdminSide
 */
class ExportController extends Controller
{
    /**
     * @var MenuDish
     */
    protected $menuDishExporter;

    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * @var MenuDishData
     */
    protected $menuDishData;

    /**
     * ExportController constructor.
     * @param MenuDish $menuDishExporter
     * @param MenuDishData $menuDishData
     * @param DishRepository $dishRepository
     */
    public function __construct(
        MenuDish $menuDishExporter,
        MenuDishData $menuDishData,
        DishRepository $dishRepository
    ) {
        $this->menuDishExporter = $menuDishExporter;
        $this->menuDishData = $menuDishData;
        $this->dishRepository = $dishRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showForm()
    {
        $dishes = $this->dishRepository->all();

        return view('admin-side.entities.dish.export', compact('dishes'));
    }

    /**
     * @param ExportDataRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showHistory(ExportDataRequest $request)
    {
        $data = $this->menuDishData->dataRequiredForExport($request->except('_token'));

        return view(
            'admin-side.entities.dish.history',
            [
                'data' => $data['dishHistory'],
                'dish_id' => $data['dish_id'],
                'date_from' => $data['date_from'],
                'date_to' => $data['date_to'],
            ]
        );
    }

    /**
     * @param ExportDataRequest $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public function export(ExportDataRequest $request)
    {
        $file = $this->menuDishExporter->export($request->except('_token'), 'csv');

        return response()->download($file)->deleteFileAfterSend(true);
    }
}
