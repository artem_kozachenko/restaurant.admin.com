<?php

namespace App\Http\Controllers\AdminSide;

use App\Http\Controllers\Controller;
use App\Services\DataCollectors\DashboardData;
use App\Services\Repositories\DishRepository;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\AdminSide
 */
class DashboardController extends Controller
{
    /**
     * @var DashboardData
     */
    protected $dashboardData;

    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * DashboardController constructor.
     * @param DashboardData $dashboardData
     * @param DishRepository $dishRepository
     */
    public function __construct(
        DashboardData $dashboardData,
        DishRepository $dishRepository
    ) {
        $this->dashboardData = $dashboardData;
        $this->dishRepository = $dishRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dishes = $this->dishRepository->all();

        return view('admin-side.dashboard.index', compact('dishes'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getChartData(Request $request)
    {
        $data = $this->dashboardData->getChartData($request->input());

        return response()->json($data);
    }
}
