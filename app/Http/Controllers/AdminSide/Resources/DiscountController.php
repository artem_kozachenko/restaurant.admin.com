<?php

namespace App\Http\Controllers\AdminSide\Resources;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDiscountRequest;
use App\Http\Requests\UpdateDiscountRequest;
use App\Models\Discount;
use App\Services\Managers\DiscountManager;
use App\Services\Repositories\DiscountRepository;

/**
 * Class DiscountController
 * @package App\Http\Controllers\AdminSide\Resources
 */
class DiscountController extends Controller
{
    /**
     * @var DiscountManager
     */
    protected $discountManager;

    /**
     * @var DiscountRepository
     */
    protected $discountRepository;

    /**
     * DiscountController constructor.
     * @param DiscountManager $discountManager
     * @param DiscountRepository $discountRepository
     */
    public function __construct(
        DiscountManager $discountManager,
        DiscountRepository $discountRepository
    ) {
        $this->discountManager = $discountManager;
        $this->discountRepository = $discountRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $discounts = $this->discountRepository->paginate(15);

        return view('admin-side.entities.discount.index', compact('discounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-side.entities.discount.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateDiscountRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateDiscountRequest $request)
    {
        $this->discountManager->create($request->except('_token'));

        return redirect()->route('discounts.index')
            ->with('success', 'New discount has been successfully created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Discount $discount
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Discount $discount)
    {
        return view('admin-side.entities.discount.edit', compact('discount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDiscountRequest $request
     * @param Discount $discount
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateDiscountRequest $request, Discount $discount)
    {
        $this->discountManager->update($discount, $request->except('_token', '_method'));

        return redirect()->back()
            ->with('success', 'Discount has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Discount $discount
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Discount $discount)
    {
        $this->discountManager->delete($discount);

        return redirect()->route('discounts.index')
            ->with('success', 'Discount has been successfully deleted.');
    }
}
