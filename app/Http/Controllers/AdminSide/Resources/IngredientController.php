<?php

namespace App\Http\Controllers\AdminSide\Resources;

use App\Http\Requests\CreateIngredientRequest;
use App\Http\Requests\UpdateIngredientRequest;
use App\Models\Ingredient;
use App\Services\IntermediateManagers\IngredientIntermediateManager;
use App\Services\Managers\IngredientManager;
use App\Services\Repositories\IngredientRepository;
use App\Http\Controllers\Controller;

/**
 * Class IngredientController
 * @package App\Http\Controllers\AdminSide\Resources
 */
class IngredientController extends Controller
{
    /**
     * @var IngredientRepository
     */
    protected $ingredientRepository;

    /**
     * @var IngredientIntermediateManager
     */
    protected $ingredientIntermediateManager;

    /**
     * IngredientController constructor.
     * @param IngredientRepository $ingredientRepository
     * @param IngredientIntermediateManager $ingredientIntermediateManager
     */
    public function __construct(
        IngredientRepository $ingredientRepository,
        IngredientIntermediateManager $ingredientIntermediateManager
    ) {
        $this->ingredientRepository = $ingredientRepository;
        $this->ingredientIntermediateManager = $ingredientIntermediateManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $ingredients = $this->ingredientRepository->paginate(10);
        $ingredients->load('photo');

        return view('admin-side.entities.ingredient.index', compact('ingredients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin-side.entities.ingredient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateIngredientRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateIngredientRequest $request)
    {
        $this->ingredientIntermediateManager->create($request->except('_token'));

        return redirect()->route('ingredients.index')
            ->with('success', 'New ingredient has been successfully created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Ingredient $ingredient
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Ingredient $ingredient)
    {
        return view('admin-side.entities.ingredient.edit', compact('ingredient'));
    }

    /**
     * @param UpdateIngredientRequest $request
     * @param Ingredient $ingredient
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(UpdateIngredientRequest $request, Ingredient $ingredient)
    {
        $this->ingredientIntermediateManager->update($ingredient, $request->except(['_token', '_method']));

        return redirect()->route('ingredients.index', $ingredient)
            ->with('success', 'Ingredient has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param IngredientManager $ingredientManager
     * @param Ingredient $ingredient
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(IngredientManager $ingredientManager, Ingredient $ingredient)
    {
        $ingredientManager->delete($ingredient);

        return redirect()->route('ingredients.index')
            ->with('success', 'Ingredient has been successfully deleted.');
    }
}
