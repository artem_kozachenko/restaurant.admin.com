<?php

namespace App\Http\Controllers\AdminSide\Resources;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use App\Models\Menu;
use App\Services\DataCollectors\MenuData;
use App\Services\IntermediateManagers\MenuIntermediateManager;
use App\Services\Managers\MenuManager;
use App\Services\Repositories\DishRepository;
use App\Services\Repositories\MenuRepository;
use App\Traits\Pagination;

/**
 * Class MenuController
 * @package App\Http\Controllers\AdminSide\Resources
 */
class MenuController extends Controller
{
    use Pagination;

    /**
     * @var MenuData
     */
    protected $menuData;

    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * @var MenuRepository
     */
    protected $menuRepository;

    /**
     * @var MenuIntermediateManager
     */
    protected $menuIntermediateManager;

    /**
     * MenuController constructor.
     * @param MenuData $menuData
     * @param DishRepository $dishRepository
     * @param MenuRepository $menuRepository
     * @param MenuIntermediateManager $menuIntermediateManager
     */
    public function __construct(
        MenuData $menuData,
        DishRepository $dishRepository,
        MenuRepository $menuRepository,
        MenuIntermediateManager $menuIntermediateManager
    ) {
        $this->menuData = $menuData;
        $this->dishRepository = $dishRepository;
        $this->menuRepository = $menuRepository;
        $this->menuIntermediateManager = $menuIntermediateManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $menus = $this->menuRepository->all()->sortByDesc('date');
        $menus->load('dishes');
        $menus = $this->paginate($menus, 10, ['path' => 'menus']);

        return view('admin-side.entities.menu.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data = $this->menuData->lastMenu();

        return view(
            'admin-side.entities.menu.create',
            [
                'remainDishes' => $data['remainDishes'],
                'lastMenuDishes' => $data['lastMenuDishes']
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateMenuRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateMenuRequest $request)
    {
        $responseMessage = $this->menuIntermediateManager->create($request->except('_token'));

        return redirect()->route('menus.index')
            ->with($responseMessage->getStatus(), $responseMessage->getMessage());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Menu $menu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Menu $menu)
    {
        $usedDishes = $menu->dishes;
        $unusedDishes = $this->dishRepository
            ->whereNotIn($usedDishes->pluck('dish_id')->toArray());

        return view('admin-side.entities.menu.edit', compact(['menu', 'usedDishes', 'unusedDishes']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMenuRequest $request
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateMenuRequest $request, Menu $menu)
    {
        $responseMessage = $this->menuIntermediateManager->update($menu, $request->except('_token', '_method'));

        return redirect()->route('menus.index', $menu)
            ->with($responseMessage->getStatus(), $responseMessage->getMessage());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MenuManager $menuManager
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(MenuManager $menuManager, Menu $menu)
    {
        $menuManager->delete($menu);

        return redirect()->route('menus.index')
            ->with('success', 'Menu has been successfully deleted.');
    }
}
