<?php

namespace App\Http\Controllers\AdminSide\Resources;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDishRequest;
use App\Http\Requests\UpdateDishRequest;
use App\Models\Dish;
use App\Services\IntermediateManagers\DishIntermediateManager;
use App\Services\Managers\DishManager;
use App\Services\Repositories\CategoryRepository;
use App\Services\Repositories\DiscountRepository;
use App\Services\Repositories\DishRepository;
use App\Services\Repositories\IngredientRepository;

/**
 * Class DishController
 * @package App\Http\Controllers\AdminSide\Resources
 */
class DishController extends Controller
{
    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var DiscountRepository
     */
    protected $discountRepository;

    /**
     * @var IngredientRepository
     */
    protected $ingredientRepository;

    /**
     * @var DishIntermediateManager
     */
    protected $dishIntermediateManager;

    /**
     * DishController constructor.
     * @param DishRepository $dishRepository
     * @param CategoryRepository $categoryRepository
     * @param DiscountRepository $discountRepository
     * @param IngredientRepository $ingredientRepository
     * @param DishIntermediateManager $dishIntermediateManager
     */
    public function __construct(
        DishRepository $dishRepository,
        CategoryRepository $categoryRepository,
        DiscountRepository $discountRepository,
        IngredientRepository $ingredientRepository,
        DishIntermediateManager $dishIntermediateManager
    ) {
        $this->dishRepository = $dishRepository;
        $this->categoryRepository = $categoryRepository;
        $this->discountRepository = $discountRepository;
        $this->ingredientRepository = $ingredientRepository;
        $this->dishIntermediateManager = $dishIntermediateManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dishes = $this->dishRepository->paginate(10);
        $dishes->load('photo');

        return view('admin-side.entities.dish.index', compact('dishes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $ingredients = $this->ingredientRepository->all();
        $categories = $this->categoryRepository->all();
        $discounts = $this->discountRepository->all();

        return view('admin-side.entities.dish.create', compact(['ingredients', 'categories', 'discounts']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateDishRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateDishRequest $request)
    {
        $responseMessage = $this->dishIntermediateManager->create($request->except('_token'));

        return redirect()->route('dishes.index')
            ->with($responseMessage->getStatus(), $responseMessage->getMessage());
    }

    /**
     * Display the specified resource.
     *
     * @param Dish $dish
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Dish $dish)
    {
        $ingredients = $dish->ingredients->toArray();

        return view('admin-side.entities.dish.show', compact(['dish', 'ingredients']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Dish $dish
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Dish $dish)
    {
        $usedIngredients = $dish->ingredients;
        $unusedIngredients = $this->ingredientRepository
            ->whereNotIn($usedIngredients->pluck('id')->toArray());
        $categories = $this->categoryRepository->allExceptCurrent($dish->category_id)->toArray();
        $discounts = $dish->discount_id
            ? $this->discountRepository->allExceptCurrent($dish->discount_id)
            : $this->discountRepository->all();

        return view('admin-side.entities.dish.edit', compact([
            'dish',
            'usedIngredients',
            'unusedIngredients',
            'categories',
            'discounts'
        ]));
    }

    /**
     * @param UpdateDishRequest $request
     * @param Dish $dish
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(UpdateDishRequest $request, Dish $dish)
    {
        $responseMessage = $this->dishIntermediateManager->update($dish, $request->except(['_token', '_method']));

        return redirect()->route('dishes.show', $dish)
            ->with($responseMessage->getStatus(), $responseMessage->getMessage());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DishManager $dishManager
     * @param Dish $dish
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(DishManager $dishManager, Dish $dish)
    {
        $dishManager->delete($dish);

        return redirect()->route('dishes.index')
            ->with('success', 'Dish has been successfully deleted.');
    }
}
