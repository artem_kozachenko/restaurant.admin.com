<?php

namespace App\Http\Controllers\AdminSide\Resources;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use App\Services\Managers\UserManager;
use App\Services\Repositories\RoleRepository;
use App\Services\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController
 * @package App\Http\Controllers\AdminSide\Resources
 */
class UserController extends Controller
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * UserController constructor.
     * @param UserManager $userManager
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     */
    public function __construct(
        UserManager $userManager,
        UserRepository $userRepository,
        RoleRepository $roleRepository
    ) {
        $this->userManager = $userManager;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Auth::user()->role->name == Role::SUPER_ADMIN
            ? $this->userRepository->adminsAndEditorsPaginate(20)
            : $this->userRepository->editorsPaginate(20);

        return view('admin-side.entities.user.index', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $roles = $this->roleRepository->allExceptCurrent($user->role_id);

        return view('admin-side.entities.user.edit', compact(['user', 'roles']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->userManager->update($user, $request->except('_token', '_method'));

        return redirect()->route('users.index')
            ->with('success', 'User has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $this->userManager->delete($user);

        return redirect()->route('users.index')
            ->with('success', 'User has been successfully deleted.');
    }
}
