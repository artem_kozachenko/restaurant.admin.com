<?php

namespace App\Http\Controllers\AdminSide\Resources;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDeliveryTypeRequest;
use App\Http\Requests\UpdateDeliveryTypeRequest;
use App\Models\DeliveryType;
use App\Services\Managers\DeliveryTypeManager;
use App\Services\Repositories\DeliveryTypeRepository;

/**
 * Class DeliveryTypeController
 * @package App\Http\Controllers\AdminSide\Resources
 */
class DeliveryTypeController extends Controller
{
    /**
     * @var DeliveryTypeManager
     */
    protected $deliveryTypeManager;

    /**
     * @var DeliveryTypeRepository
     */
    protected $deliveryTypeRepository;

    /**
     * DeliveryTypeController constructor.
     * @param DeliveryTypeManager $deliveryTypeManager
     * @param DeliveryTypeRepository $deliveryTypeRepository
     */
    public function __construct(
        DeliveryTypeManager $deliveryTypeManager,
        DeliveryTypeRepository $deliveryTypeRepository
    ) {
        $this->deliveryTypeManager = $deliveryTypeManager;
        $this->deliveryTypeRepository = $deliveryTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $deliveryTypes = $this->deliveryTypeRepository->paginate(15);

        return view('admin-side.entities.delivery.index', compact('deliveryTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin-side.entities.delivery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateDeliveryTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateDeliveryTypeRequest $request)
    {
        $this->deliveryTypeManager->create($request->input());

        return redirect()->route('delivery_types.index')
            ->with('success', 'New delivery type has been successfully added.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param DeliveryType $deliveryType
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(DeliveryType $deliveryType)
    {
        return view('admin-side.entities.delivery.edit', compact('deliveryType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDeliveryTypeRequest $request
     * @param DeliveryType $deliveryType
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateDeliveryTypeRequest $request, DeliveryType $deliveryType)
    {
        $this->deliveryTypeManager->update($deliveryType, $request->input());

        return redirect()->route('delivery_types.index')
            ->with('success', 'Delivery type has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeliveryType $deliveryType
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(DeliveryType $deliveryType)
    {
        $this->deliveryTypeManager->delete($deliveryType);

        return redirect()->route('delivery_types.index')
            ->with('success', 'Delivery type has been successfully deleted.');
    }
}
