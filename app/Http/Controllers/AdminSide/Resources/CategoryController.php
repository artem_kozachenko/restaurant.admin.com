<?php

namespace App\Http\Controllers\AdminSide\Resources;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use App\Services\Generators\NameGenerator;
use App\Services\Managers\CategoryManager;
use App\Services\Repositories\CategoryRepository;

/**
 * Class CategoryController
 * @package App\Http\Controllers\AdminSide\Resources
 */
class CategoryController extends Controller
{
    /**
     * @var NameGenerator
     */
    protected $nameGenerator;

    /**
     * @var CategoryManager
     */
    protected $categoryManager;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * CategoryController constructor.
     * @param NameGenerator $nameGenerator
     * @param CategoryManager $categoryManager
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(
        NameGenerator $nameGenerator,
        CategoryManager $categoryManager,
        CategoryRepository $categoryRepository
    ) {
        $this->nameGenerator = $nameGenerator;
        $this->categoryManager = $categoryManager;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = $this->categoryRepository->paginate(15);

        return view('admin-side.entities.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->categoryRepository->all();

        return view('admin-side.entities.category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateCategoryRequest $request)
    {
        $params = $request->except('_token');
        $params['url_name'] = $this->nameGenerator->urlName($request->input('name'));
        $this->categoryManager->create($params);

        return redirect()->route('categories.index')
            ->with('success', 'New category has been successfully created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        $categories = $this
                ->categoryRepository
                ->allExceptCurrentAndParent($category->id, $category->parent_category_id) ?? null;

        return view('admin-side.entities.category.edit', compact(['category', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategoryRequest $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $params = $request->except('_token', '_method');
        $params['url_name'] = $this->nameGenerator->urlName($request->input('name'));
        $this->categoryManager->update($category, $params);

        return redirect()->route('categories.show', $category)
            ->with('success', 'Category has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $this->categoryManager->delete($category);

        return redirect()->route('categories.index')
            ->with('success', 'Category has been successfully deleted.');
    }
}
