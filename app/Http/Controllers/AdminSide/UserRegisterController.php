<?php

namespace App\Http\Controllers\AdminSide;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostCreateUserRequest;
use App\Http\Requests\PreCreateUserRequest;
use App\Models\Role;
use App\Services\IntermediateManagers\UserRegisterIntermediateManager;
use App\Services\Repositories\RoleRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserRegisterController
 * @package App\Http\Controllers\AdminSide
 */
class UserRegisterController extends Controller
{
    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * @var UserRegisterIntermediateManager
     */
    protected $userRegisterIntermediateManager;

    /**
     * UserRegisterController constructor.
     * @param RoleRepository $roleRepository
     * @param UserRegisterIntermediateManager $userRegisterIntermediateManager
     */
    public function __construct(
        RoleRepository $roleRepository,
        UserRegisterIntermediateManager $userRegisterIntermediateManager
    ) {
        $this->roleRepository = $roleRepository;
        $this->userRegisterIntermediateManager = $userRegisterIntermediateManager;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPreCreateForm()
    {
        $roles = Auth::user()->role->name == Role::SUPER_ADMIN
            ? $this->roleRepository->all()
            : $this->roleRepository->notAll();

        return view('admin-side.user-register.pre-create', compact('roles'));
    }

    /**
     * @param PreCreateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function preCreateUser(PreCreateUserRequest $request)
    {
        $this->userRegisterIntermediateManager->preCreate($request->except('_token'));

        return redirect()->route('users.index')
            ->with('success', 'User registration request has been successfully created.');
    }

    /**
     * @param string $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPostCreateForm(string $token)
    {
        return view('admin-side.user-register.post-create', compact('token'));
    }

    /**
     * @param PostCreateUserRequest $request
     * @param string $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreateUser(PostCreateUserRequest $request, string $token)
    {
        $this->userRegisterIntermediateManager->postCreate($request->except('_token'), $token);

        return redirect()->route('login')->with('success', 'Your account has been successfully created.');
    }
}
