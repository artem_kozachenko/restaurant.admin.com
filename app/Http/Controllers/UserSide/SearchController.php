<?php

namespace App\Http\Controllers\UserSide;

use App\Http\Controllers\Controller;
use App\Services\Searchers\DishSearcher;
use Illuminate\Http\Request;

/**
 * Class SearchController
 * @package App\Http\Controllers\UserSide
 */
class SearchController extends Controller
{
    /**
     * @var DishSearcher
     */
    protected $dishSearcher;

    /**
     * SearchController constructor.
     * @param DishSearcher $dishSearcher
     */
    public function __construct(DishSearcher $dishSearcher)
    {
        $this->dishSearcher = $dishSearcher;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function findDishesAutocomplete(Request $request)
    {
        $dishes = $this->dishSearcher->search($request->input('search'));

        return response()->json($dishes, 200);
    }
}
