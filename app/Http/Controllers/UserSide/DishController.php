<?php

namespace App\Http\Controllers\UserSide;

use App\Models\Dish;
use App\Services\DataCollectors\DishData;
use Illuminate\Http\Request;

/**
 * Class DishController
 * @package App\Http\Controllers\UserSide
 */
class DishController
{
    /**
     * @var DishData
     */
    protected $dishData;

    /**
     * DishController constructor.
     * @param DishData $dishData
     */
    public function __construct(DishData $dishData)
    {
        $this->dishData = $dishData;
    }

    /**
     * @param Dish $dish
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDish(Dish $dish)
    {
        return view('user-side.dish', compact('dish'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showDishes(Request $request)
    {
        $dishes = $this->dishData->search($request->input('search'), $request->input('page'));

        return view('user-side.dishes', compact('dishes'));
    }
}
