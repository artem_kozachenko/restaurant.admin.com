<?php

namespace App\Http\Controllers\UserSide;

use App\Services\DataCollectors\MenuData;

/**
 * Class MenuController
 * @package App\Http\Controllers\UserSide
 */
class MenuController
{
    /**
     * @var MenuData
     */
    protected $menuData;

    /**
     * MenuController constructor.
     * @param MenuData $menuData
     */
    public function __construct(MenuData $menuData)
    {
        $this->menuData = $menuData;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMenu()
    {
        $menu = $this->menuData->currentWeekMenuData();

        return view('user-side.menu', compact('menu'));
    }
}
