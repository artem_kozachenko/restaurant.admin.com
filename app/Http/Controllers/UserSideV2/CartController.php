<?php

namespace App\Http\Controllers\UserSideV2;

use App\Http\Controllers\Controller;
use App\Http\Requests\RecalculateTotalPriceRequest;
use App\Services\DataCollectors\CartData;
use App\Services\Order\PriceUpdater;
use Illuminate\Http\Request;

/**
 * Class CartController
 * @package App\Http\Controllers\UserSideV2
 */
class CartController extends Controller
{
    /**
     * @var CartData
     */
    protected $cartData;

    /**
     * @var PriceUpdater
     */
    protected $priceUpdater;

    /**
     * CartController constructor.
     * @param CartData $cartData
     * @param PriceUpdater $priceUpdater
     */
    public function __construct(
        CartData $cartData,
        PriceUpdater $priceUpdater
    ) {
        $this->cartData = $cartData;
        $this->priceUpdater = $priceUpdater;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCart(Request $request)
    {
        $data = $this->cartData->gatCartData($request->session()->getId());
        if (isset($data['orderData']['orderId'])) {
            $this->priceUpdater
                ->orderTotalPriceUpdate($data['orderData']['orderId'], $data['orderData']['orderTotalPrice']);
            $this->priceUpdater
                ->orderDishesTotalPriceUpdate($data['orderData']['dishesToUpdate']);
        }

        return view('userSideV2.cart', [
            'orderDishes' => $data['orderData'],
            'deliveryTypes' => $data['deliveryTypes'],
            'discounts' => $data['orderData']['discounts'],
        ]);
    }

    /**
     * @param RecalculateTotalPriceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recalculateTotalPrice(RecalculateTotalPriceRequest $request)
    {
        $data = $this->cartData->getUpdatedPrices($request->session()->getId(), $request->input('id'));
        $this->priceUpdater
            ->orderTotalPriceUpdate($data['orderId'], $data['orderTotalPrice']);
        $this->priceUpdater
            ->orderDishTotalPriceUpdate($data['orderDishId'], $data['orderTotalPrice']);

        return response()->json($data);
    }
}
