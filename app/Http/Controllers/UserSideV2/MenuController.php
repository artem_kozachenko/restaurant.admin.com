<?php

namespace App\Http\Controllers\UserSideV2;

use App\Services\DataCollectors\MenuData;
use Illuminate\Http\Request;

/**
 * Class MenuController
 * @package App\Http\Controllers\UserSideV2
 */
class MenuController
{
    /**
     * @var MenuData
     */
    protected $menuData;

    /**
     * MenuController constructor.
     * @param MenuData $menuData
     */
    public function __construct(MenuData $menuData)
    {
        $this->menuData = $menuData;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $menu = $this->menuData->currentWeekMenuData($request->session()->getId());

        return view('userSideV2.menu', compact('menu'));
    }
}
