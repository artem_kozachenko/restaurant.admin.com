<?php

namespace App\Http\Controllers\UserSideV2;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderDishRequest;
use App\Services\Order\OrderDishRemover;
use App\Services\Order\OrderConfirmation;
use App\Http\Requests\ConfirmOrderRequest;
use App\Services\Order\OrderDishCounterChanger;

/**
 * Class OrderController
 * @package App\Http\Controllers\UserSideV2
 */
class OrderController extends Controller
{
    /**
     * @var OrderDishRemover
     */
    protected $orderDishRemover;

    /**
     * @var OrderConfirmation
     */
    protected $orderConfirmation;

    /**
     * @var OrderDishCounterChanger
     */
    protected $orderDishCounterChanger;

    /**
     * OrderController constructor.
     * @param OrderDishRemover $orderDishRemover
     * @param OrderConfirmation $orderConfirmation
     * @param OrderDishCounterChanger $orderDishCounterChanger
     */
    public function __construct(
        OrderDishRemover $orderDishRemover,
        OrderConfirmation $orderConfirmation,
        OrderDishCounterChanger $orderDishCounterChanger
    ) {
        $this->orderDishRemover = $orderDishRemover;
        $this->orderConfirmation = $orderConfirmation;
        $this->orderDishCounterChanger = $orderDishCounterChanger;
    }

    /**
     * @param ConfirmOrderRequest $request
     */
    public function confirmOrder(ConfirmOrderRequest $request)
    {
        $this->orderConfirmation->confirmOrder($request->input(), $request->cookie());
    }

    /**
     * @param OrderDishRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function increaseOrderCounter(OrderDishRequest $request)
    {
        $quantity = $this->orderDishCounterChanger->increaseOrderDishCounter($request->input(), $request->cookie());

        return response()->json($quantity);
    }

    /**
     * @param OrderDishRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function decreaseOrderCount(OrderDishRequest $request)
    {
        $quantity = $this->orderDishCounterChanger->decreaseOrderDishCount($request->input(), $request->cookie());

        return response()->json($quantity);
    }

    /**
     * @param OrderDishRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeOrderItem(OrderDishRequest $request)
    {
        $totalPrice = $this->orderDishRemover->removeOrderDish($request->input(), $request->cookie());

        return response()->json($totalPrice);
    }
}
