<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Class DishAutocompleteCollection
 * @package App\Http\Resources
 */
class DishAutocompleteCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'photo' => $this->photo ? $this->photo->name : 'default.png',
            'category' => $this->category->name
        ];
    }
}
