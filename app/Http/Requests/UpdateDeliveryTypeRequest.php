<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class UpdateDeliveryTypeRequest
 * @package App\Http\Requests
 */
class UpdateDeliveryTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool) Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:delivery_types,id,' . $this->id,
            'price' => 'required|numeric'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The delivery name field is required.',
            'name.unique' => 'Such delivery type already exists.',
            'price.required' => 'The price field is required.',
            'price.numeric' => 'The price field must be a number.',
        ];
    }
}
