<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class UpdateDishRequest
 * @package App\Http\Requests
 */
class UpdateDishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool) Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:dishes,id,' . $this->id,
            'price' => 'required|numeric',
            'category_id' => 'required',
            'photo' => 'mimes:jpeg,jpg,png',
            'ingredients_ids_checked' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The dish name field is required.',
            'name.unique' => 'Such dish already exists.',
            'price.required' => 'The dish price field is required.',
            'price.numeric' => 'The dish price field must be a number.',
            'category_id.required' => 'You have to select category.',
        ];
    }
}
