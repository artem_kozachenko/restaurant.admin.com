<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class CreateDiscountRequest
 * @package App\Http\Requests
 */
class CreateDiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool) Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:discounts',
            'percent' => 'required|numeric',
            'from' => 'required|date_format:H:i',
            'to' => 'required|date_format:H:i',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The discount name field is required.',
            'name.unique' => 'Such discount already exists.',
            'percent.required' => 'The percent field is required.',
            'percent.numeric' => 'The percent field must be a number.',
            'from.required' => 'You have to select time.',
            'from.date_format' => 'Wrong time format.',
            'to.required' => 'You have to select time.',
            'to.date_format' => 'Wrong time format.',
        ];
    }
}
