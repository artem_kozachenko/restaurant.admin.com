<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class ExportDataRequest
 * @package App\Http\Requests
 */
class ExportDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool) Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dish_id' => 'required|integer',
            'date_from' => 'required|date',
            'date_to' => 'required|date',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'dish_id.required' => 'You have to select dish.',
            'date_from.required' => 'You have to select date.',
            'date_to.required' => 'You have to select date.',
        ];
    }
}
