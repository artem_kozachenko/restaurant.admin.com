<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class MinAccessLevel
 * @package App\Http\Middleware
 */
class MinAccessLevel
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @param string $minAccessLevel
     * @return bool|mixed
     */
    public function handle($request, Closure $next, string $minAccessLevel)
    {
        return $request->user()->role->access_level >= $minAccessLevel
            ? $next($request)
            : abort(404);
    }
}
