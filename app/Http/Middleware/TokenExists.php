<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

/**
 * Class TokenExists
 * @package App\Http\Middleware
 */
class TokenExists
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @param string $table
     * @return mixed
     */
    public function handle($request, Closure $next, string $table)
    {
        return DB::table($table)->where('token', $request->token)->exists()
            ? $next($request)
            : abort(404);
    }
}
