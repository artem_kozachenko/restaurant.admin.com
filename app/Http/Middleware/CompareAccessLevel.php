<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class CompareAccessLevel
 * @package App\Http\Middleware
 */
class CompareAccessLevel
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @return bool|mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUser = $request->user();
        $accessibleUser = $request->user;

        return $currentUser->role->access_level > $accessibleUser->role->access_level
            ? $next($request)
            : abort(404);
    }
}
