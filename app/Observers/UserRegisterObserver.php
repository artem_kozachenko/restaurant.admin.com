<?php

namespace App\Observers;

use App\Jobs\DeleteAfter24Hour;
use App\Jobs\UserRegisterMessage;
use App\Models\UserRegister;

/**
 * Class UserRegisterObserver
 * @package App\Observers
 */
class UserRegisterObserver
{
    /**
     * Handle to the user register "created" event.
     *
     * @param  \App\Models\UserRegister  $userRegister
     * @return void
     */
    public function created(UserRegister $userRegister)
    {
        UserRegisterMessage::dispatch($userRegister->token, $userRegister->email);
        DeleteAfter24Hour::dispatch($userRegister->token)->delay(now()->addHours(24));
    }
}
