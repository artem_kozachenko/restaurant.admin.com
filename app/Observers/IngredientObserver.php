<?php

namespace App\Observers;

use App\Models\Ingredient;

/**
 * Class IngredientObserver
 * @package App\Observers
 */
class IngredientObserver
{
    /**
     * Handle the ingredient "deleted" event.
     *
     * @param  \App\Models\Ingredient  $ingredient
     * @return void
     */
    public function deleted(Ingredient $ingredient)
    {
        $photo = $ingredient->photo;
        if ($photo) {
            $photo->delete();
        }
    }
}
