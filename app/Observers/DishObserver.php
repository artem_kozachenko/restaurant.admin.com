<?php

namespace App\Observers;

use App\Models\Dish;
use App\Services\CacheMagic\Cache;
use App\Services\Managers\MenuDishManager;

/**
 * Class DishObserver
 * @package App\Observers
 */
class DishObserver
{
    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @var MenuDishManager
     */
    protected $menuDishManager;

    /**
     * DishObserver constructor.
     * @param Cache $cache
     * @param MenuDishManager $menuDishManager
     */
    public function __construct(
        Cache $cache,
        MenuDishManager $menuDishManager
    ) {
        $this->cache = $cache;
        $this->menuDishManager = $menuDishManager;
    }

    /**
     * Handle the dish "created" event.
     *
     * @param  \App\Models\Dish  $dish
     * @return void
     * @throws \Exception
     */
    public function created(Dish $dish)
    {
        $this->forgetDishesCache();
    }

    /**
     * Handle the dish "updated" event.
     *
     * @param  \App\Models\Dish  $dish
     * @return void
     * @throws \Exception
     */
    public function updated(Dish $dish)
    {
        $this->forgetDishesCache();
    }

    /**
     * Handle the dish "deleted" event.
     *
     * @param  \App\Models\Dish  $dish
     * @return void
     * @throws \Exception
     */
    public function deleted(Dish $dish)
    {
        $oldPhoto = $dish->photo;
        if ($oldPhoto) {
            $oldPhoto->delete();
        }
        $oldDescription = $dish->description;
        if ($oldDescription) {
            $oldDescription->delete();
        }
        $this->menuDishManager->deleteByDishId($dish->id);
        $this->forgetDishesCache();
    }

    /**
     * @throws \Exception
     */
    protected function forgetDishesCache()
    {
        $this->cache->flushByTags(['dishes']);
    }
}
