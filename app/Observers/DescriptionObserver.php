<?php

namespace App\Observers;

use App\Models\Description;
use App\Services\Repositories\DescriptionRepository;

/**
 * Class DescriptionObserver
 * @package App\Observers
 */
class DescriptionObserver
{
    /**
     * @var DescriptionRepository
     */
    protected $descriptionRepository;

    /**
     * DescriptionObserver constructor.
     * @param DescriptionRepository $descriptionRepository
     */
    public function __construct(DescriptionRepository $descriptionRepository)
    {
        $this->descriptionRepository = $descriptionRepository;
    }

    /**
     * Handle the description "creating" event.
     *
     * @param  \App\Models\Description  $description
     * @return void
     */
    public function creating(Description $description)
    {
        $oldDescription = $this
            ->descriptionRepository
            ->oldDescription(
                $description->descriptionable_id,
                $description->descriptionable_type
            );
        if ($oldDescription) {
            $oldDescription->delete();
        }
    }
}
