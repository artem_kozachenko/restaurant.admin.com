<?php

namespace App\Observers;

use App\Models\Photo;
use App\Services\Repositories\PhotoRepository;
use Illuminate\Support\Facades\Storage;

/**
 * Class PhotoObserver
 * @package App\Observers
 */
class PhotoObserver
{
    /**
     * @var PhotoRepository
     */
    protected $photoRepository;

    /**
     * PhotoObserver constructor.
     * @param PhotoRepository $photoRepository
     */
    public function __construct(PhotoRepository $photoRepository)
    {
        $this->photoRepository = $photoRepository;
    }

    /**
     * Handle the photo "creating" event.
     *
     * @param  \App\Models\Photo  $photo
     * @return void
     */
    public function creating(Photo $photo)
    {
        $oldPhoto = $this
            ->photoRepository
            ->oldPhoto(
                $photo->photoable_id,
                $photo->photoable_type
            );
        if ($oldPhoto) {
            $oldPhoto->delete();
        }
    }

    /**
     * Handle the photo "deleted" event.
     *
     * @param  \App\Models\Photo  $photo
     * @return void
     */
    public function deleted(Photo $photo)
    {
        Storage::delete($photo->path);
    }
}
