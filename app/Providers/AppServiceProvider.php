<?php

namespace App\Providers;

use App\Models\Description;
use App\Models\Dish;
use App\Models\Ingredient;
use App\Models\Photo;
use App\Models\UserRegister;
use App\Observers\DescriptionObserver;
use App\Observers\DishObserver;
use App\Observers\IngredientObserver;
use App\Observers\PhotoObserver;
use App\Observers\UserRegisterObserver;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        UserRegister::observe(UserRegisterObserver::class);
        Dish::observe(DishObserver::class);
        Photo::observe(PhotoObserver::class);
        Ingredient::observe(IngredientObserver::class);
        Description::observe(DescriptionObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
