$(document).ready(function () {
    var doNotHide;
    $('#search')
        .autocomplete({
            minLength: 2,
            source: function () {
                $.ajax({
                    url: "http://restaurant.admin.com/search/dishes/autocomplete",
                    data: {'search': $('#search').val()},
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        $('#start').children().remove();
                        if (response.length > 0) {
                            response.forEach(function (item) {
                                $('#start').append(
                                    '<a data-id="' + item.id + '" href="http://restaurant.admin.com/dish/' + item.id + '">' +
                                        '<div class="search-item">' +
                                            '<div class="search-item-image">' +
                                                '<img class="rounded" width="100%" height="100%" src="http://restaurant.admin.com/storage/dishes/' + item.photo + '" alt="' + item.name + '">' +
                                            '</div>' +
                                            '<div class="search-item-text">' +
                                                '<p>Name: ' + item.name + '</p>' +
                                                '<p>Price: ' + item.price + '</p>' +
                                                '<p>Category: ' + item.category + '</p>' +
                                            '</div>' +
                                        '</div>' +
                                    '</a>'
                                );
                            });
                            $('#search-results').show();
                        } else {
                            $('#start').children().remove();
                            $('#search-results').hide();
                        }
                    },
                });
            }
        })
        .click(function () {
            if ($('#start').children().length > 0) {
                $('#search-results').show();
            }
            doNotHide = true;
        });
    $('#wrapper').click(function () {
        if (doNotHide == false) {
            $('#search-results').hide();
        }
        doNotHide = false;
    });
});
