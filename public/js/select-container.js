$(document).ready(function() {
    // Drag and drop functionality
    $("ul.droptrue").sortable({
        connectWith: "ul"
    });
    // Selected items check
    $("ul.selected-items").sortable({
        update: function (e, i) {
            // Toggle input availability
            item = i.item;
            input = $(item).find('input');
            if ($(input).prop('disabled')) {
                $(input).prop('disabled', false);
            } else {
                $(input).prop('disabled', true);
            }
            items = $(this).children('li');
            if (items.length > 0) {
                $(this).children('input').first().prop('checked', true);
            } else {
                $(this).children('input').first().prop('checked', false);
            }
        }
    });
    // Search functionality
    $('input[data-role="search"]').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        var dataType = $(this).data('ul-type');
        $('ul[data-type="' + dataType + '"]').children('li').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
// If last menu dishes isset => set checked true
var selectedItemsContainer = $('ul.selected-items');
if (selectedItemsContainer.children('li').length > 0) {
    selectedItemsContainer.children('input').first().prop('checked', true);
}
// Prepare selected items ids
function collectItems(type) {
    var items = $("ul[data-type='selected-" + type + "']").children('li');
    var itemIds = [];
    for(var i = 0; i < items.length; i++) {
        var id = items[i].dataset.id;
        itemIds.push(id);
    }
    $('input[name="' + type + '"]').val(itemIds);
}
// Don't show error message for drag and drop
function removeError(error, element, name) {
    var i = 0;
    name.forEach(function (item) {
        if (element[0].name == item) {
            i = 1
        }
    });
    if (i == 1) {

        return false;
    }
    $(element).after(error);
}
