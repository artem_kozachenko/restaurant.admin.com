var str = window.location.href;
var pos = 0;
var i = 0;
var slashes = [];
// Find all slashes
while (true) {
    var foundPos = str.indexOf('/', pos);
    i++;
    if (foundPos == -1) {
        break;
    }
    slashes[i] = foundPos;
    pos = foundPos + 1;
}
// If four slashes
if (slashes.length == 5) {
    var questionMarkPos = str.indexOf('?', pos);
    // If no question mark
    if (questionMarkPos == -1) {
        link = str.substring(slashes[4] + 1);
    // If question mark after fourth slash
    } else {
        link = str.substring(slashes[4] + 1, questionMarkPos);
    }
// If more then four slashes
} else {
    link = str.substring(slashes[4] + 1, slashes[5]);
}
// Set active link
$('#main-links a[name="' + link + '"]').addClass('active');

if (! localStorage.getItem('visible')) {
    localStorage.setItem('visible', 'true');
}
var visible = localStorage.getItem('visible');
// Hide nav if it was hidden.
if (visible == 'false') {
    $('#header').css('width', '100%');
    $('#content').css('width', '100%');
    $('#nav').css('width', '0px');
}
$('button[name="collapser"]').click(function () {
    // alert(visible);
    if (visible == 'true') {
        $('#nav').animate({width: "0%"}, 300);
        $('#header').animate({width: "100%"}, 300);
        $('#content').animate({width: "100%"}, 300);
        visible = 'false';
        localStorage.setItem('visible', visible)
    } else {
        $('#header').animate({width: "80%"}, 300);
        $('#content').animate({width: "80%"}, 300);
        $('#nav').animate({width: "20%"}, 300);
        visible = 'true';
        localStorage.setItem('visible', visible)
    }
});
