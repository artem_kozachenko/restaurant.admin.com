var str = window.location.href;
var pos = 0;
var i = 0;
var slashes = [];
// Find all slashes
while (true) {
    var foundPos = str.indexOf('/', pos);
    i++;
    if (foundPos == -1) {
        break;
    }
    slashes[i] = foundPos;
    pos = foundPos + 1;
}
// If three slashes
if (slashes.length == 4) {
    var questionMarkPos = str.indexOf('?', pos);
    // If no question mark
    if (questionMarkPos == -1) {
        link = str.substring(slashes[3] + 1);
    // If question mark after third slash
    } else {
        link = str.substring(slashes[3] + 1, questionMarkPos);
    }
// If more then three slashes
} else {
    link = str.substring(slashes[3] + 1, slashes[4]);
}
if (link == '') {
    $('li[name="menu"]').addClass('active');
}
// Set active link
$('li[name="' + link + '"]').addClass('active');
