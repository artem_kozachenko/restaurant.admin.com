$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
});
$.validator.addMethod("checked", function(value, element, arg){
    if (! $(element).prop('checked')) {
        $('div[data-for="' + arg + '"]').addClass('error-drop')
        return false;
    }
    else {
        $('div[data-for="' + arg + '"]').removeClass('error-drop')
        return true;
    }
});
$.validator.addMethod("time", function(value, element) {
    if (!/^\d{2}:\d{2}$/.test(value)) return false;
    var parts = value.split(':');
    if (parts[0] > 23 || parts[1] > 59 || parts[2] > 59) return false;
    return true;
}, "Invalid time format.");