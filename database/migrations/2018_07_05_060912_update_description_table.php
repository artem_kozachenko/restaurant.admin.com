<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class UpdateDescriptionTable
 */
class UpdateDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('descriptions', function (Blueprint $table) {
            $table->dropColumn('dish_id');
            $table->integer('descriptionable_id')->after('content');
            $table->string('descriptionable_type')->after('descriptionable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('descriptions', function (Blueprint $table) {
            $table->dropColumn(['descriptionable_id', 'descriptionable_type']);
            $table->integer('dish_id')->unsigned()->after('content');
        });
    }
}
