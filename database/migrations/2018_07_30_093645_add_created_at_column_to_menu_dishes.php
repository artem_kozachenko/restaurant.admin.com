<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddCreatedAtColumnToMenuDishes
 */
class AddCreatedAtColumnToMenuDishes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_dishes', function (Blueprint $table) {
            $table->timestamp('created_at')->after('dish_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_dishes', function (Blueprint $table) {
            $table->dropColumn('created_at');
        });
    }
}
