<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class UpdateMenuDishesTable
 */
class UpdateMenuDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_dishes', function (Blueprint $table) {
            $table->dropColumn(['name', 'category', 'category_url_name', 'description_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_dishes', function (Blueprint $table) {
            $table->string('name')->after('menu_id');
            $table->string('category')->after('price');
            $table->string('category_url_name')->after('category');
            $table->integer('description_id')->nullable()->after('category_url_name');
        });
    }
}
