<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddCategoryUrlNameColumnToMenuDishesTable
 */
class AddCategoryUrlNameColumnToMenuDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_dishes', function (Blueprint $table) {
            $table->string('category_url_name')->after('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_dishes', function (Blueprint $table) {
            $table->dropColumn('category_url_name');
        });
    }
}
