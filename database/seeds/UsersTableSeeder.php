<?php

use Illuminate\Database\Seeder;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 50)->create();
        factory(App\Models\User::class)->create([
            'name' => 'superadmin1',
            'email' => 'superadmin1@gmail.com',
            'password' => bcrypt('qweqweqwe'),
            'role_id' => 1
        ]);
        factory(App\Models\User::class)->create([
            'name' => 'admin1',
            'email' => 'admin1@gmail.com',
            'password' => bcrypt('qweqweqwe'),
            'role_id' => 2
        ]);
        factory(App\Models\User::class)->create([
            'name' => 'editor1',
            'email' => 'editor1@gmail.com',
            'password' => bcrypt('qweqweqwe'),
            'role_id' => 3
        ]);
        factory(App\Models\User::class)->create([
            'name' => 'superadmin2',
            'email' => 'superadmin2@gmail.com',
            'password' => bcrypt('qweqweqwe'),
            'role_id' => 1
        ]);
        factory(App\Models\User::class)->create([
            'name' => 'admin2',
            'email' => 'admin2@gmail.com',
            'password' => bcrypt('qweqweqwe'),
            'role_id' => 2
        ]);
        factory(App\Models\User::class)->create([
            'name' => 'editor2',
            'email' => 'editor2@gmail.com',
            'password' => bcrypt('qweqweqwe'),
            'role_id' => 3
        ]);
    }
}
