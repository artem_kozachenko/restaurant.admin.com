<?php

use Illuminate\Database\Seeder;

/**
 * Class RoleTableSeeder
 */
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Role::class)->create([
            'name' => 'Super admin',
            'access_level' => '3'
        ]);
        factory(App\Models\Role::class)->create([
            'name' => 'Admin',
            'access_level' => '2'
        ]);
        factory(App\Models\Role::class)->create([
            'name' => 'Editor',
            'access_level' => '1'
        ]);
    }
}
