<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $parentCategories = factory(App\Models\Category::class, 10)->create();
        factory(App\Models\Category::class, 10)->create([
            'parent_category_id' => function () use ($parentCategories) {
                return $parentCategories->random()->id;
            }
        ]);
        $ingredients = factory(App\Models\Ingredient::class, 100)->create();
        $dishes = factory(App\Models\Dish::class, 100)->create([
            'category_id' => function () use ($parentCategories) {
                return $parentCategories->random()->id;
            }
        ])
            ->each(function ($dish) use ($ingredients) {
                $dish->description()->create([
                    'content' => file_get_contents('http://loripsum.net/api/1/short/plaintext'),
                    'descriptionable_id' => $dish->id,
                    'descriptionable_type' => get_class($dish)
                ]);
                $dish->ingredients()->attach($ingredients->random(10)->pluck('id')->toArray());
            });
        factory(App\Models\Menu::class, 10)->create()
            ->each(function ($menu) use ($dishes) {
                $dish = $dishes->random();
                $menu->dishes()->create([
                    'menu_id' => $menu->id,
                    'price' => $dish->price,
                    'dish_id' => $dish->id
                ]);
            });
    }
}
