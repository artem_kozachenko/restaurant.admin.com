<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Models\Menu::class, function (Faker $faker) {
    $date = $faker->unique()->date();
    $date = new Carbon($date);

    return [
        'date' => $date->year . '-' . $date->weekOfYear
    ];
});
