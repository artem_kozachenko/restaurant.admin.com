<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    $name = $faker->unique()->lexify();
    return [
        'name' => $name,
        'url_name' => $name,
        'parent_category_id' => null
    ];
});
