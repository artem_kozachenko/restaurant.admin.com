<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Description::class, function (Faker $faker) {
    return [
        'content' => file_get_contents('http://loripsum.net/api/1/short/plaintext'),
        'descriptionable_type' => 'App\Models\Dish'
    ];
});
